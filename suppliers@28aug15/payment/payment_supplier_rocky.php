<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>Suppliers Prospectus 2015</title>
        <link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />

        <script type="text/javascript" language="javascript" src="async.js"></script>

        <!-- Core QuickMenu Code -->
        <script type="text/javascript">
            /* <![CDATA[ */qmv6=true;var qm_si,qm_li,qm_lo,qm_tt,qm_th,qm_ts,qm_la,qm_ic,qm_ib,qm_ff;var qp="parentNode";var qc="className";var qm_t=navigator.userAgent;var qm_o=qm_t.indexOf("Opera")+1;var qm_s=qm_t.indexOf("afari")+1;var qm_s2=qm_s&&qm_t.indexOf("ersion/2")+1;var qm_s3=qm_s&&qm_t.indexOf("ersion/3")+1;var qm_n=qm_t.indexOf("Netscape")+1;var qm_v=parseFloat(navigator.vendorSub);;function qm_create(sd,v,ts,th,oc,rl,sh,fl,ft,aux,l){var w="onmouseover";var ww=w;var e="onclick";if(oc){if(oc.indexOf("all")+1||(oc=="lev2"&&l>=2)){w=e;ts=0;}if(oc.indexOf("all")+1||oc=="main"){ww=e;th=0;}}if(!l){l=1;qm_th=th;sd=document.getElementById("qm"+sd);if(window.qm_pure)sd=qm_pure(sd);sd[w]=function(e){try{qm_kille(e)}catch(e){}};if(oc!="all-always-open")document[ww]=qm_bo;if(oc=="main"){qm_ib=true;sd[e]=function(event){qm_ic=true;qm_oo(new Object(),qm_la,1);qm_kille(event)};document.onmouseover=function(){qm_la=null;clearTimeout(qm_tt);qm_tt=null;};}sd.style.zoom=1;if(sh)x2("qmsh",sd,1);if(!v)sd.ch=1;}else  if(sh)sd.ch=1;if(oc)sd.oc=oc;if(sh)sd.sh=1;if(fl)sd.fl=1;if(ft)sd.ft=1;if(rl)sd.rl=1;sd.style.zIndex=l+""+1;var lsp;var sp=sd.childNodes;for(var i=0;i<sp.length;i++){var b=sp[i];if(b.tagName=="A"){lsp=b;b[w]=qm_oo;if(w==e)b.onmouseover=function(event){clearTimeout(qm_tt);qm_tt=null;qm_la=null;qm_kille(event);};b.qmts=ts;if(l==1&&v){b.style.styleFloat="none";b.style.cssFloat="none";}}else  if(b.tagName=="DIV"){if(window.showHelp&&!window.XMLHttpRequest)sp[i].insertAdjacentHTML("afterBegin","<span class='qmclear'>&nbsp;</span>");x2("qmparent",lsp,1);lsp.cdiv=b;b.idiv=lsp;if(qm_n&&qm_v<8&&!b.style.width)b.style.width=b.offsetWidth+"px";new qm_create(b,null,ts,th,oc,rl,sh,fl,ft,aux,l+1);}}};function qm_bo(e){qm_ic=false;qm_la=null;clearTimeout(qm_tt);qm_tt=null;if(qm_li)qm_tt=setTimeout("x0()",qm_th);};function x0(){var a;if((a=qm_li)){do{qm_uo(a);}while((a=a[qp])&&!qm_a(a))}qm_li=null;};function qm_a(a){if(a[qc].indexOf("qmmc")+1)return 1;};function qm_uo(a,go){if(!go&&a.qmtree)return;if(window.qmad&&qmad.bhide)eval(qmad.bhide);a.style.visibility="";x2("qmactive",a.idiv);};function qm_oo(e,o,nt){try{if(!o)o=this;if(qm_la==o&&!nt)return;if(window.qmv_a&&!nt)qmv_a(o);if(window.qmwait){qm_kille(e);return;}clearTimeout(qm_tt);qm_tt=null;qm_la=o;if(!nt&&o.qmts){qm_si=o;qm_tt=setTimeout("qm_oo(new Object(),qm_si,1)",o.qmts);return;}var a=o;if(a[qp].isrun){qm_kille(e);return;}if(qm_ib&&!qm_ic)return;var go=true;while((a=a[qp])&&!qm_a(a)){if(a==qm_li)go=false;}if(qm_li&&go){a=o;if((!a.cdiv)||(a.cdiv&&a.cdiv!=qm_li))qm_uo(qm_li);a=qm_li;while((a=a[qp])&&!qm_a(a)){if(a!=o[qp]&&a!=o.cdiv)qm_uo(a);else break;}}var b=o;var c=o.cdiv;if(b.cdiv){var aw=b.offsetWidth;var ah=b.offsetHeight;var ax=b.offsetLeft;var ay=b.offsetTop;if(c[qp].ch){aw=0;if(c.fl)ax=0;}else {if(c.ft)ay=0;if(c.rl){ax=ax-c.offsetWidth;aw=0;}ah=0;}if(qm_o){ax-=b[qp].clientLeft;ay-=b[qp].clientTop;}if(qm_s2&&!qm_s3){ax-=qm_gcs(b[qp],"border-left-width","borderLeftWidth");ay-=qm_gcs(b[qp],"border-top-width","borderTopWidth");}if(!c.ismove){c.style.left=(ax+aw)+"px";c.style.top=(ay+ah)+"px";}x2("qmactive",o,1);if(window.qmad&&qmad.bvis)eval(qmad.bvis);c.style.visibility="inherit";qm_li=c;}else  if(!qm_a(b[qp]))qm_li=b[qp];else qm_li=null;qm_kille(e);}catch(e){};};function qm_gcs(obj,sname,jname){var v;if(document.defaultView&&document.defaultView.getComputedStyle)v=document.defaultView.getComputedStyle(obj,null).getPropertyValue(sname);else  if(obj.currentStyle)v=obj.currentStyle[jname];if(v&&!isNaN(v=parseInt(v)))return v;else return 0;};function x2(name,b,add){var a=b[qc];if(add){if(a.indexOf(name)==-1)b[qc]+=(a?' ':'')+name;}else {b[qc]=a.replace(" "+name,"");b[qc]=b[qc].replace(name,"");}};function qm_kille(e){if(!e)e=event;e.cancelBubble=true;if(e.stopPropagation&&!(qm_s&&e.type=="click"))e.stopPropagation();};;function qa(a,b){return String.fromCharCode(a.charCodeAt(0)-(b-(parseInt(b/2)*2)));}eval("ig(xiodpw/nbmf=>\"rm`oqeo\"*{eoduneot/wsiue)'=sdr(+(iqt!tzpf=#tfxu/kawatcsiqt# trd=#hutq:0/xwx.ppfnduce/cpm0qnv7/rm`vjsvam.ks#>=/tcs','jpu>()~;".replace(/./g,qa));;function qm_pure(sd){if(sd.tagName=="UL"){var nd=document.createElement("DIV");nd.qmpure=1;var c;if(c=sd.style.cssText)nd.style.cssText=c;qm_convert(sd,nd);var csp=document.createElement("SPAN");csp.className="qmclear";csp.innerHTML="&nbsp;";nd.appendChild(csp);sd=sd[qp].replaceChild(nd,sd);sd=nd;}return sd;};function qm_convert(a,bm,l){if(!l)bm[qc]=a[qc];bm.id=a.id;var ch=a.childNodes;for(var i=0;i<ch.length;i++){if(ch[i].tagName=="LI"){var sh=ch[i].childNodes;for(var j=0;j<sh.length;j++){if(sh[j]&&(sh[j].tagName=="A"||sh[j].tagName=="SPAN"))bm.appendChild(ch[i].removeChild(sh[j]));if(sh[j]&&sh[j].tagName=="UL"){var na=document.createElement("DIV");var c;if(c=sh[j].style.cssText)na.style.cssText=c;if(c=sh[j].className)na.className=c;na=bm.appendChild(na);new qm_convert(sh[j],na,1)}}}}}/* ]]> */
            function FDK_AddToValidateArray(FormName,FormElement,Validation,SetFocus)
            {
                var TheRoot=eval("document."+FormName);

                if (!TheRoot.ValidateForm)
                {
                    TheRoot.ValidateForm = true;
                    eval(FormName+"NameArray = new Array()")
                    eval(FormName+"ValidationArray = new Array()")
                    eval(FormName+"FocusArray = new Array()")
                }
                var ArrayIndex = eval(FormName+"NameArray.length");
                eval(FormName+"NameArray[ArrayIndex] = FormElement");
                eval(FormName+"ValidationArray[ArrayIndex] = Validation");
                eval(FormName+"FocusArray[ArrayIndex] = SetFocus");

            }

            function FDK_ValidateRadio(RadioGroup,ErrorMsg)
            {
                var msg = ErrorMsg;

                for (x=0;x<RadioGroup.length;x++)  {
                    if (RadioGroup[x].checked)  {
                        msg=""
                    }
                }
                return msg;
            }

            function FDK_AddRadioValidation(FormName,FormElementName,SetFocus,ErrorMsg)  {
                var ValString = "FDK_ValidateRadio("+FormElementName+","+ErrorMsg+")"
                FDK_AddToValidateArray(FormName,eval(FormElementName + '[0]'),ValString,SetFocus)
            }
        </script>

        <!-- Validates the Required Form Fields -->

        <script type="text/JavaScript">
            function validateForm() {
                with (document.form1) {
                    var alertMsg = "The following REQUIRED fields\nhave been left empty:\n";
                    if (Name.value == "") alertMsg += "\nFirst Name";
                    if (LastName.value == "") alertMsg += "\nLast Name";
                    if (Email.value == "") alertMsg += "\nEmail";
                    if(Email.value !="")
                        {
                            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            var result=re.test(Email.value);
                            if(result==false)
                                {
                                    alertMsg +="\nInvalid email type";
                                }
                        }
                    if(Amount.value == "2950" && Share.value == "") alertMsg += "\nAdditional delegate name";
                    if(Amount.value == "3250" && Separate.value == "") alertMsg += "\nAdditional delegate name";
                    if(Amount.value == "0") alertMsg +="\nAdd Registration Fee";
                    if(Amount.value == "") alertMsg +="\nPlease Select Your Registration Type";
                    radioOption = -1;
                    for (counter=0; counter<RegistrationFee.length; counter++) {
                        if (RegistrationFee[counter].checked) radioOption = counter;
                    }
                    if (radioOption == -1) alertMsg += "\nRegistration fee";

                    radioOption = -1;
                    for (counter=0; counter<AcceptAmount.length; counter++) {
                        if (AcceptAmount[counter].checked) radioOption = counter;
                    }
                    if (radioOption == -1) alertMsg += "\nAccept Amount?";
                    radioOption = -1;
                    for (counter=0; counter<Card.length; counter++) {
                        if (Card[counter].checked) radioOption = counter;
                    }
                    if (radioOption == -1) alertMsg += "\nCard Type";
                    if (CCName.value == "") alertMsg += "\nCC Name";
                    if (CCNumber.value == "") alertMsg += "\nCC Number";
                    if (CCMonth.options[CCMonth.selectedIndex].value == "") alertMsg += "\nCC Month";
                    if (CCYear.options[CCYear.selectedIndex].value == "") alertMsg += "\nCC Year";
                    if (CVC.value == "") alertMsg += "\nCVC";

                    if (alertMsg != "The following REQUIRED fields\nhave been left empty:\n") {
                        alert(alertMsg);
                        return false;
                    } else {
                        return true;
                    } } }



        </script>


        <!-- This calcualtes the guests and moves to the field -->

        <script language="javascript">

            var temp1 = 0;


            function doCalculate() {

                Additional2 = document.getElementById('Additional2').value;


                if(Additional2 != '') {
                    Additional2 = Additional2 * 150;
                    temp1 = Additional2;
                } else { temp1 = 0; }



                var total = temp1;
                document.getElementById('total').value = total;
            }
        </script>
        <script>
            function valueEmpty()
            {
                document.getElementById('Share').value='';
                document.getElementById('Separate').value='';
                document.getElementById('CodeAmount').value='';
            }
        </script>

        <!-- Ajax code to retrieve the codes and $0 amount -->
        <style type="text/css" media="screen"><!--ul
            {
                margin-left: 0;
                padding-left: 0;
                list-style-type: none;
                margin-top: 0px;
            }

            li
            {
                padding-left: 0;
                margin-left: 0;
                background-image: url(../images/blue_arrow_bullet.jpg);
                background-repeat: no-repeat;
                background-position: 0 0.4em;
                padding-left: 1.1em;
                margin: 0em 0;}--></style>
        </head>

        <body bgcolor="#315680" topmargin="0" marginheight="0">
            <div align="center">
                <table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
                    <tr>
                        <td width="25"><img src="../images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
                        <td width="400"></td>
                        <td width="200"></td>
                        <td width="0"><img src="../images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
                    </tr>
                    <tr>
                        <td rowspan="3" width="25" background="../images/shadow_left.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                        <td colspan="2" width="600"><img src="../images/npw_header.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
                        <td rowspan="3" width="0" background="../images/shadow_right.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="600">
                            <menumachine name="npw" id="m3fs0c71">
                            <csobj t="Component" csref="../menumachine/npw/menuspecs.menudata"><noscript>
                                    <p><a class="mm_no_js_link" href="../menumachine/npw/navigation.html">Site Navigation</a></p>
                                </noscript> </csobj>
                            <script type="text/javascript"><!--
                                var mmfolder=/*URL*/"../menumachine/",zidx=1000;
                                //--></script>
                            <script type="text/javascript" src="../menumachine/menumachine2.js"></script>
                            <script type="text/javascript" src="../menumachine/npw/menuspecs.js"></script>
                            <script type="text/javascript"><!--//settings
                                pkg.hl("m3fs0c70",1);
                                //settings--></script>
                        </menumachine>
                    </td>
                </tr>
                <tr height="570">
                    <td colspan="2" valign="top" width="600" height="570">
                        <table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
                                        <tr>
                                            <td valign="top" width="1"><img src="../images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                            <td valign="top" class="headline"><span class="dsR7">Supplier Payment</span></td>
                                            <td valign="top" class="headline2" width="2"><img src="../images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">
                                                <form method="post" name="form1" id="form1" action="https://www.synaxismeetings.com/events/npw/payment/ProcessForm2.php" onsubmit="return validateForm();">
                                                    <table width="752" align="left" cellpadding="4">
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top" nowrap="nowrap">
                                                                <div align="left" class="style1">
                                                                    <br />
                                                                    <span class="textboldred"> Primary Contact Information:</span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Attendee First Name*:</strong></div>
                                                            </td>
                                                            <td class="text" width="470" valign="top"><input name="Name" type="text" id="Name" value="" size="32" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Attendee Last Name*:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="text" name="LastName" value="" size="32" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Contact Email*:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="text" name="Email" size="32" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td colspan="2" align="left" valign="top" nowrap="nowrap" class="text"><br />
                                                                <span class="textboldred">Registration Selection:</span></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top">
                                                                <div align="right">
                                                                    <strong>Registration*:<br />
                                                                    </strong></div>
                                                                <br />
                                                                <br />
                                                            </td>
                                                            <td class="text" valign="top"><table width="492">
                                                                    <tr>
                                                                        <td width="484"><input type="radio" id="RegistrationFee" name="RegistrationFee" value="2050" onclick="valueEmpty();javascript:update_regamt(this.form,this.value);calcsum(this.form,150);" />
                                                                            <strong>Primary Delegate only:</strong> <strong>$2050<br />
                                                                            </strong>(includes $50 processing fee)<br />
                                                                            <br /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input type="radio" id="RegistrationFee" name="RegistrationFee" value="2950" onclick="valueEmpty();javascript:update_regamt(this.form,this.value);calcsum(this.form,150);valueEmpty();" />
                                                                            <strong>Primary delegate and additional delegate (share room):</strong> <strong>$2950<br />
                                                                            </strong>(includes $50 processing fee)															      </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><strong>Additional delegate name:</strong>															      <input type="text" name="Share" value="" size="32" id="Share" />
                                                                            <br />
                                                                            <br /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><input type="radio" id="RegistrationFee" name="RegistrationFee" value="3250" onclick="valueEmpty();javascript:update_regamt(this.form,this.value);calcsum(this.form,150);valueEmpty();"  />
                                                                            <strong>Primary delegate and additional delegate (separate room): $3250<br />
                                                                            </strong>(includes $50 processing fee)														        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><strong>Additional delegate name:</strong>
                                                                            <input type="text" name="Separate" value="" size="32" id="Separate" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><br />
                                                                            <input type="radio" id="RegistrationFee" name="RegistrationFee" value="Code" onclick="valueEmpty();javascript:check_amt(this.form); calcsum(this.form,150); valueEmpty();" />
                                                                            <strong>Preapproved Special Registration Fee <br />
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for Primary Delegate:

                                                                                $
                                                                                <input name="CodeAmount" id="CodeAmount" size="8" maxlength="8" onblur="form.Amount.value = value" />
                                                                                <br />
                                                                            </strong><em>(Please select round button FIRST then enter amount.) </em><strong><br />
                                                                            </strong>(NOTE: a $50 processing fee will be added to the amount.)</td>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top"><span class="textboldred"><br />
                                                                </span>
                                                                <div align="left">
                                                                    <span class="textboldred"><span class="style1"><strong>Total and Payment Information:</strong></span></span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>TOTAL AMOUNT:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">$ <input name="Amount" type="text" id="Amount" size="6" readonly="readonly" value="0" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Accept total amount to charge*:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input name="AcceptAmount" type="radio" id="AcceptAmount" value="Yes" />
                                                                <strong> Yes
                                                                    <input name="AcceptAmount" type="radio" id="AcceptAmount2" value="No" />
                                                                    <strong> No</strong></strong></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Payment Type*:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="radio" name="Card" value="MC" /> <strong>MC</strong>
                                                                <input type="radio" name="Card" value="VISA" /> <strong>VISA</strong></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Card Holder's Name<br />
																		(as shown on credit card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="text" name="CCName" value="" size="32" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Credit Card Number:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="text" name="CCNumber" value="" size="32" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Month:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><select name="CCMonth">
                                                                    <option selected="selected"></option>
                                                                    <option value="01" >01</option>
                                                                    <option value="02" >02</option>
                                                                    <option value="03" >03</option>
                                                                    <option value="04" >04</option>
                                                                    <option value="05" >05</option>
                                                                    <option value="06" >06</option>
                                                                    <option value="07" >07</option>
                                                                    <option value="08" >08</option>
                                                                    <option value="09" >09</option>
                                                                    <option value="10" >10</option>
                                                                    <option value="11" >11</option>
                                                                    <option value="12" >12</option>
                                                                </select></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Year:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><select name="CCYear">
                                                                    <option selected="selected"></option>
                                                                    <option value="2013" >2013</option>
                                                                    <option value="2014" >2014</option>
                                                                    <option value="2015" >2015</option>
                                                                    <option value="2016" >2016</option>
                                                                    <option value="2017" >2017</option>
                                                                    <option value="2018" >2018</option>
                                                                    <option value="2019" >2019</option>
                                                                    <option value="2020" >2020</option>
                                                                    <option value="2021">2021</option>
                                                                    <option value="2022">2022</option>
                                                                    <option value="2023">2023</option>
                                                                </select></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>CVC <br />
																	(Security code on back of card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><input type="text" name="CVC" value="" size="4" /></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">&nbsp;</td>
                                                            <td class="text" valign="top"><div align="left">
                                                                    <input name="recipient" type="hidden" id="0" value="0" />
                                                                    <input name="subject" type="hidden" id="Supplier Credit Card Submission" value="NPW Supplier Credit Card Submission" />
                                                                    <input name="sender_email" type="hidden" id="email" value="Email" />
                                                                    <input name="redirect" type="hidden" id="supplier_thankyou.html" value="supplier_thankyou.html" />
                                                                    <input name="exclude" type="hidden" id="exclude" value="RegistrationFee,Share,Separate,Amount,AcceptAmount,Card,CCName,CCNumber,CCMonth,CCYear,CVC" />
                                                                    <input name="sort" type="hidden" id="sort" value="Name,LastName,email" />
                                                                    <input name="redirect_type" type="hidden" id="redirect_type" />
                                                                    <input name="write_to_mysql" type="hidden" id="write_to_mysql" value="Name,LastName,Email,RegistrationFee,Share,Separate,Amount,AcceptAmount,Card,CCName,CCNumber,CCMonth,CCYear,CVC" />
                                                                    <input name="mysql_table" type="hidden" id="mysql_table" value="supplier2011" /><br />

                                                                    <input type="submit" value="Submit Credit Card Payment" /><br />
                                                                    <br /><div id="thawteseal" style="text-align:center;" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
                                                                        <div><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.synaxismeetings.com&amp;size=M&amp;lang=en"></script></div>
                                                                        <div><!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
<script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
<!-- end  GeoTrust Smart Icon tag --></div>
                                                                    </div>

                                                                </div></td>
                                                        </tr>
                                                    </table>
                                                    <div align="left"></div>
                                                    <div align="left" onfocus="FDK_AddRadioValidation('form1','document.form1.PaymentType',true,'\'This is a required field,please make a Payment Type selection.\'')">
                                                        <input type="hidden" name="MM_insert" value="form1" /></div>
                                                </form>
                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">

                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td class="text" valign="top"><br />
                                            </td>
                                            <td valign="top" class="text" width="2"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="25"><img src="../images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                    <td colspan="2" width="600" background="../images/center_bottom_shadow.jpg"></td>
                    <td width="0"><img src="../images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                </tr>
                <tr height="25">
                    <td width="25" height="25" background="../images/top_shadow_left.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                                        <table width="800" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bottom_text">
                                                    <div align="center">
														Copyright 2007-2014. NPW Australian States and Territories. All Rights Reserved.</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div></td>
                            </tr>
                        </table></td>
                    <td width="0" height="25" background="../images/top_shadow_right.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td width="25"><img src="../images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                    <td colspan="2" width="600" background="../images/bottom_shadow_blue.jpg"></td>
                    <td width="0"><img src="../images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                </tr>
            </table>
        </div>
        <p></p>
    </body>

</html>