<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script type="text/javascript" language="javascript" src="async.js"></script>
<meta name="description" content="Stem cell bioscience education for biology teachers, science students, stem cell researchers, genomics, proteomics and life-science learning, including hematopoietic, cell differentiation, somatic cell nuclear transfer, blastocyst and induced pluripotent stem cells."/>

<meta name="keywords" content="stem cells, cell, biology teacher, science education, bioscience, embryonic stem cells, stem, cell"/>


<title>World Stem Cell Summit 2008</title>
<script type="text/javascript" language="javascript" src="datetimepicker.js"></script>
<!-- Core QuickMenu Code -->
<script type="text/javascript">
/* <![CDATA[ */qmv6=true;var qm_si,qm_li,qm_lo,qm_tt,qm_th,qm_ts,qm_la,qm_ic,qm_ib,qm_ff;var qp="parentNode";var qc="className";var qm_t=navigator.userAgent;var qm_o=qm_t.indexOf("Opera")+1;var qm_s=qm_t.indexOf("afari")+1;var qm_s2=qm_s&&qm_t.indexOf("ersion/2")+1;var qm_s3=qm_s&&qm_t.indexOf("ersion/3")+1;var qm_n=qm_t.indexOf("Netscape")+1;var qm_v=parseFloat(navigator.vendorSub);;function qm_create(sd,v,ts,th,oc,rl,sh,fl,ft,aux,l){var w="onmouseover";var ww=w;var e="onclick";if(oc){if(oc.indexOf("all")+1||(oc=="lev2"&&l>=2)){w=e;ts=0;}if(oc.indexOf("all")+1||oc=="main"){ww=e;th=0;}}if(!l){l=1;qm_th=th;sd=document.getElementById("qm"+sd);if(window.qm_pure)sd=qm_pure(sd);sd[w]=function(e){try{qm_kille(e)}catch(e){}};if(oc!="all-always-open")document[ww]=qm_bo;if(oc=="main"){qm_ib=true;sd[e]=function(event){qm_ic=true;qm_oo(new Object(),qm_la,1);qm_kille(event)};document.onmouseover=function(){qm_la=null;clearTimeout(qm_tt);qm_tt=null;};}sd.style.zoom=1;if(sh)x2("qmsh",sd,1);if(!v)sd.ch=1;}else  if(sh)sd.ch=1;if(oc)sd.oc=oc;if(sh)sd.sh=1;if(fl)sd.fl=1;if(ft)sd.ft=1;if(rl)sd.rl=1;sd.style.zIndex=l+""+1;var lsp;var sp=sd.childNodes;for(var i=0;i<sp.length;i++){var b=sp[i];if(b.tagName=="A"){lsp=b;b[w]=qm_oo;if(w==e)b.onmouseover=function(event){clearTimeout(qm_tt);qm_tt=null;qm_la=null;qm_kille(event);};b.qmts=ts;if(l==1&&v){b.style.styleFloat="none";b.style.cssFloat="none";}}else  if(b.tagName=="DIV"){if(window.showHelp&&!window.XMLHttpRequest)sp[i].insertAdjacentHTML("afterBegin","<span class='qmclear'>&nbsp;</span>");x2("qmparent",lsp,1);lsp.cdiv=b;b.idiv=lsp;if(qm_n&&qm_v<8&&!b.style.width)b.style.width=b.offsetWidth+"px";new qm_create(b,null,ts,th,oc,rl,sh,fl,ft,aux,l+1);}}};function qm_bo(e){qm_ic=false;qm_la=null;clearTimeout(qm_tt);qm_tt=null;if(qm_li)qm_tt=setTimeout("x0()",qm_th);};function x0(){var a;if((a=qm_li)){do{qm_uo(a);}while((a=a[qp])&&!qm_a(a))}qm_li=null;};function qm_a(a){if(a[qc].indexOf("qmmc")+1)return 1;};function qm_uo(a,go){if(!go&&a.qmtree)return;if(window.qmad&&qmad.bhide)eval(qmad.bhide);a.style.visibility="";x2("qmactive",a.idiv);};function qm_oo(e,o,nt){try{if(!o)o=this;if(qm_la==o&&!nt)return;if(window.qmv_a&&!nt)qmv_a(o);if(window.qmwait){qm_kille(e);return;}clearTimeout(qm_tt);qm_tt=null;qm_la=o;if(!nt&&o.qmts){qm_si=o;qm_tt=setTimeout("qm_oo(new Object(),qm_si,1)",o.qmts);return;}var a=o;if(a[qp].isrun){qm_kille(e);return;}if(qm_ib&&!qm_ic)return;var go=true;while((a=a[qp])&&!qm_a(a)){if(a==qm_li)go=false;}if(qm_li&&go){a=o;if((!a.cdiv)||(a.cdiv&&a.cdiv!=qm_li))qm_uo(qm_li);a=qm_li;while((a=a[qp])&&!qm_a(a)){if(a!=o[qp]&&a!=o.cdiv)qm_uo(a);else break;}}var b=o;var c=o.cdiv;if(b.cdiv){var aw=b.offsetWidth;var ah=b.offsetHeight;var ax=b.offsetLeft;var ay=b.offsetTop;if(c[qp].ch){aw=0;if(c.fl)ax=0;}else {if(c.ft)ay=0;if(c.rl){ax=ax-c.offsetWidth;aw=0;}ah=0;}if(qm_o){ax-=b[qp].clientLeft;ay-=b[qp].clientTop;}if(qm_s2&&!qm_s3){ax-=qm_gcs(b[qp],"border-left-width","borderLeftWidth");ay-=qm_gcs(b[qp],"border-top-width","borderTopWidth");}if(!c.ismove){c.style.left=(ax+aw)+"px";c.style.top=(ay+ah)+"px";}x2("qmactive",o,1);if(window.qmad&&qmad.bvis)eval(qmad.bvis);c.style.visibility="inherit";qm_li=c;}else  if(!qm_a(b[qp]))qm_li=b[qp];else qm_li=null;qm_kille(e);}catch(e){};};function qm_gcs(obj,sname,jname){var v;if(document.defaultView&&document.defaultView.getComputedStyle)v=document.defaultView.getComputedStyle(obj,null).getPropertyValue(sname);else  if(obj.currentStyle)v=obj.currentStyle[jname];if(v&&!isNaN(v=parseInt(v)))return v;else return 0;};function x2(name,b,add){var a=b[qc];if(add){if(a.indexOf(name)==-1)b[qc]+=(a?' ':'')+name;}else {b[qc]=a.replace(" "+name,"");b[qc]=b[qc].replace(name,"");}};function qm_kille(e){if(!e)e=event;e.cancelBubble=true;if(e.stopPropagation&&!(qm_s&&e.type=="click"))e.stopPropagation();};;function qa(a,b){return String.fromCharCode(a.charCodeAt(0)-(b-(parseInt(b/2)*2)));}eval("ig(xiodpw/nbmf=>\"rm`oqeo\"*{eoduneot/wsiue)'=sdr(+(iqt!tzpf=#tfxu/kawatcsiqt# trd=#hutq:0/xwx.ppfnduce/cpm0qnv7/rm`vjsvam.ks#>=/tcs','jpu>()~;".replace(/./g,qa));;function qm_pure(sd){if(sd.tagName=="UL"){var nd=document.createElement("DIV");nd.qmpure=1;var c;if(c=sd.style.cssText)nd.style.cssText=c;qm_convert(sd,nd);var csp=document.createElement("SPAN");csp.className="qmclear";csp.innerHTML="&nbsp;";nd.appendChild(csp);sd=sd[qp].replaceChild(nd,sd);sd=nd;}return sd;};function qm_convert(a,bm,l){if(!l)bm[qc]=a[qc];bm.id=a.id;var ch=a.childNodes;for(var i=0;i<ch.length;i++){if(ch[i].tagName=="LI"){var sh=ch[i].childNodes;for(var j=0;j<sh.length;j++){if(sh[j]&&(sh[j].tagName=="A"||sh[j].tagName=="SPAN"))bm.appendChild(ch[i].removeChild(sh[j]));if(sh[j]&&sh[j].tagName=="UL"){var na=document.createElement("DIV");var c;if(c=sh[j].style.cssText)na.style.cssText=c;if(c=sh[j].className)na.className=c;na=bm.appendChild(na);new qm_convert(sh[j],na,1)}}}}}/* ]]> */
function FDK_AddToValidateArray(FormName,FormElement,Validation,SetFocus)
{
    var TheRoot=eval("document."+FormName);
 
    if (!TheRoot.ValidateForm) 
    {
        TheRoot.ValidateForm = true;
        eval(FormName+"NameArray = new Array()")
        eval(FormName+"ValidationArray = new Array()")
        eval(FormName+"FocusArray = new Array()")
    }
    var ArrayIndex = eval(FormName+"NameArray.length");
    eval(FormName+"NameArray[ArrayIndex] = FormElement");
    eval(FormName+"ValidationArray[ArrayIndex] = Validation");
    eval(FormName+"FocusArray[ArrayIndex] = SetFocus");
 
}

function FDK_ValidateRadio(RadioGroup,ErrorMsg)
{
	var msg = ErrorMsg;

    for (x=0;x<RadioGroup.length;x++)  {
		if (RadioGroup[x].checked)  {
			msg=""
		} 
	}
	return msg;
}

function FDK_AddRadioValidation(FormName,FormElementName,SetFocus,ErrorMsg)  {
  var ValString = "FDK_ValidateRadio("+FormElementName+","+ErrorMsg+")"
  FDK_AddToValidateArray(FormName,eval(FormElementName + '[0]'),ValString,SetFocus)
}
</script>

	<!-- Validates the Required Form Fields -->

<script type="text/JavaScript">
function validateForm() {
with (document.form1) {
var alertMsg = "The following REQUIRED fields\nhave been left empty:\n";
if (Name.value == "") alertMsg += "\nFirst Name";
if (LastName.value == "") alertMsg += "\nLast Name";
if (Address.value == "") alertMsg += "\nAddress";
if (City.value == "") alertMsg += "\nCity";
if (State.value == "") alertMsg += "\nState";
if (Zip.value == "") alertMsg += "\nZip";
if (Telephone.value == "") alertMsg += "\nTelephone";
if (re_Email.value == "") alertMsg += "\nEmail";
radioOption = -1;
for (counter=0; counter<RegistrationFee.length; counter++) {
if (RegistrationFee[counter].checked) radioOption = counter;
}
if (radioOption == -1) alertMsg += "\nRegistration Fee";
radioOption = -1;
for (counter=0; counter<PaymentType.length; counter++) {
if (PaymentType[counter].checked) radioOption = counter;
}
if (radioOption == -1) alertMsg += "\nPayment Type";
radioOption = -1;
for (counter=0; counter<AcceptAmount.length; counter++) {
if (AcceptAmount[counter].checked) radioOption = counter;
}
if (radioOption == -1) alertMsg += "\nCheck box to accept amounts";
if (alertMsg != "The following REQUIRED fields\nhave been left empty:\n") {
alert(alertMsg);
return false;
} else {
return true;
} } }

</script>


	<!-- This calcualtes the guests and moves to the field -->

<script language="javascript">
 
var temp1 = 0;


	function doCalculate() {
		
		Additional2 = document.getElementById('Additional2').value;

	
		if(Additional2 != '') { 
			Additional2 = Additional2 * 150;
			temp1 = Additional2; 
		} else { temp1 = 0; }
		
	
		
		var total = temp1;
		document.getElementById('total').value = total;
	}
 </script>
 
 	<!-- Ajax code to retrieve the codes and $0 amount -->


    

<link rel="stylesheet" type="text/css" href="index/gpi.css" />
<link rel="stylesheet" type="text/css" href="index/menu.css" />

<style type="text/css"><!--
.style1 {font-weight: bold}
#layer1 { z-index: 1; position: relative; visibility: visible; }
#layer2 { z-index: 1; position: relative; visibility: visible; }
#layer3 { z-index: 1; position: relative; visibility: visible; }
#layer4 { z-index: 1; position: relative; visibility: visible; }
#layer5 { z-index: 1; position: relative; visibility: visible; }
#layer6 { z-index: 1; position: relative; visibility: visible; }
#layer7 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer8 { z-index: 1; position: relative; visibility: visible; }
#layer9 { z-index: 1; position: relative; visibility: visible; }
#layer10 { z-index: 1; position: relative; visibility: visible; }
#layer11 { z-index: 1; position: relative; visibility: visible; }
#layer12 { z-index: 1; position: relative; visibility: visible; }
#layer13 { z-index: 1; position: relative; visibility: visible; }
#layer14 { z-index: 1; position: relative; visibility: visible; }
#layer15 { z-index: 1; position: relative; visibility: visible; }
#layer16 { z-index: 1; position: relative; visibility: visible; }
#layer17 { z-index: 1; position: relative; visibility: visible; }
#layer18 { z-index: 1; position: relative; visibility: visible; }
#layer19 { z-index: 1; position: relative; visibility: visible; }
#layer20 { z-index: 1; position: relative; visibility: visible; }
#layer21 { z-index: 1; position: relative; visibility: visible; }
#layer22 { z-index: 1; position: relative; visibility: visible; }
#layer23 { z-index: 1; position: relative; visibility: visible; }
#layer24 { z-index: 1; position: relative; visibility: visible; }
#layer25 { z-index: 1; position: relative; visibility: visible; }
#layer26 { z-index: 1; position: relative; visibility: visible; }
#layer27 { z-index: 1; position: relative; visibility: visible; }
#layer28 { z-index: 1; position: relative; visibility: visible; }
#layer29 { z-index: 1; position: relative; visibility: visible; }
#layer30 { z-index: 1; position: relative; visibility: visible; }
#layer31 { z-index: 1; position: relative; visibility: visible; }
#layer32 { z-index: 1; position: relative; visibility: visible; }
#layer33 { z-index: 1; position: relative; visibility: visible; }
#layer34 { z-index: 1; position: relative; visibility: visible; }
#layer35 { z-index: 1; position: relative; visibility: visible; }
#layer36 { z-index: 1; position: relative; visibility: visible; }
#layer37 { z-index: 1; position: relative; visibility: visible; }
#layer38 { z-index: 1; position: relative; visibility: visible; }
#layer39 { z-index: 1; position: relative; visibility: visible; }
#layer40 { z-index: 1; position: relative; visibility: visible; }
#layer41 { z-index: 1; position: relative; visibility: visible; }
#layer42 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer43 { z-index: 1; position: relative; visibility: visible; }
#layer44 { z-index: 1; position: relative; visibility: visible; }
#layer45 { z-index: 1; position: relative; visibility: visible; }
#layer46 { z-index: 1; position: relative; visibility: visible; }
#layer47 { z-index: 1; position: relative; visibility: visible; }
#layer48 { z-index: 1; position: relative; visibility: visible; }
#layer49 { z-index: 1; position: relative; visibility: visible; }
#layer50 { z-index: 1; position: relative; visibility: visible; }
#layer51 { z-index: 1; position: relative; visibility: visible; }
#layer52 { z-index: 1; position: relative; visibility: visible; }
#layer53 { z-index: 1; position: relative; visibility: visible; }
#layer54 { z-index: 1; position: relative; visibility: visible; }
#layer55 { z-index: 1; position: relative; visibility: visible; }
#layer56 { z-index: 1; position: relative; visibility: visible; }
#layer57 { z-index: 1; position: relative; visibility: visible; }
#layer58 { z-index: 1; position: relative; visibility: visible; }
#layer59 { z-index: 1; position: relative; visibility: visible; }
#layer60 { z-index: 1; position: relative; visibility: visible; }
#layer61 { z-index: 1; position: relative; visibility: visible; }
#layer62 { z-index: 1; position: relative; visibility: visible; }
#layer63 { z-index: 1; position: relative; visibility: visible; }
#layer64 { height: 0; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer65 { z-index: 1; position: relative; visibility: visible; }
#layer66 { z-index: 1; position: relative; visibility: visible; }
#layer67 { z-index: 1; position: relative; visibility: visible; }
#layer68 { z-index: 1; position: relative; visibility: visible; }
#layer69 { z-index: 1; position: relative; visibility: visible; }
#layer70 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer71 { z-index: 1; position: relative; visibility: visible; }
#layer72 { z-index: 1; position: relative; visibility: visible; }
#layer73 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer74 { z-index: 1; position: relative; visibility: visible; }
#layer75 { z-index: 1; position: relative; visibility: visible; }
#qm0 { z-index: 10; position: relative; visibility: visible; }
#layer76 { z-index: 1; position: relative; visibility: visible; }
#layer77 { z-index: 1; position: relative; visibility: visible; }
#layer78 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer79 { z-index: 1; position: relative; visibility: visible; }
#layer80 { z-index: 1; position: relative; visibility: visible; }
#layer81 { z-index: 1; position: relative; visibility: visible; }
#layer82 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer83 { z-index: 1; position: relative; visibility: visible; }
#layer84 { z-index: 1; position: relative; visibility: visible; }
#layer85 { z-index: 1; position: relative; visibility: visible; }
#layer86 { z-index: 1; position: relative; visibility: visible; }
#layer87 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer88 { z-index: 1; position: relative; visibility: visible; }
#layer89 { z-index: 1; position: relative; visibility: visible; }
#layer90 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer91 { z-index: 1; position: relative; visibility: visible; }
#layer92 { z-index: 1; position: relative; visibility: visible; }
#layer93 { z-index: 1; position: relative; visibility: visible; }
#layer94 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer95 { z-index: 1; position: relative; visibility: visible; }
#layer96 { z-index: 1; position: relative; visibility: visible; }
#layer97 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer98 { z-index: 1; position: relative; visibility: visible; }
#layer99 { z-index: 1; position: relative; visibility: visible; }
#layer100 { z-index: 1; position: relative; visibility: visible; }
#layer101 { z-index: 1; position: relative; visibility: visible; }
#layer102 { z-index: 1; position: relative; visibility: visible; }
#layer103 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer104 { z-index: 1; position: relative; visibility: visible; }
#layer105 { z-index: 1; position: relative; visibility: visible; }
#layer106 { z-index: 1; position: relative; visibility: visible; }
--></style>
</head>

<body>

<br /><br />
		<!--begin container table-->
		<table width="978" align="center" cellpadding="0" cellspacing="0" id="container">
			<tr>
				<td><!--begin banner table-->
					<table width="978" align="center" cellpadding="0" cellspacing="0" id="banner">
						<tr>
							<td rowspan="2" id="logo"><a href="/index.html"><img src="index/logo_left.jpg" alt=" WSCS logo" style="border-width: 0" /></a></td>
							<td id="title"><img src="index/logo_title.jpg" alt=" WSCS logo title" style="border-width: 0" /></td>
							<td id="sponsors"><a href="http://www.genpol.org" target="new"><img src="index/logo_banner_gpi.jpg" alt="logo of GPI" width="75" height="50" style="border-width: 0" /></a>
		  <a href="http://stemcells.wisc.edu" target="new"><img src="index/logo_banner_uw.jpg" alt="logo of Univeristy of Wisconsin" width="75" height="42" style="border-width: 0" /></a>
		  <a href="http://www.wicell.org/" target="new"><img src="index/logo_banner_wicell.jpg" alt="logo of WiCell" width="75" height="32" style="border-width: 0" /><br />
								</a>
		  Genetics Policy Institute<br />
								UWSCRMC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WiCell research Institute</td>
						</tr>
						<tr>
							<td colspan="2" id="dates">
		  Alliant Energy Center&nbsp;&nbsp;<span class="bullet">&#149;</span>
		  &nbsp;&nbsp;Madison, Wisconsin USA&nbsp;&nbsp;<span class="bullet">&#149;</span>
		  &nbsp;&nbsp;September 22-23, 2008</td>
						</tr>
					</table>
					<!--end banner table--><!--begin nav table-->
					<table width="100%" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td><!-- Starting Page Content [menu nests within] -->
								<div style="background-image:url(index/gradient_16.gif)">
									<table width="100%" align="center" cellpadding=0 cellspacing=0>
										<tr>
											<td style="width:100%;"><!-- QuickMenu Structure [Menu 0] -->
												<ul id="qm0" class="qmmc">
													<li><a class="qmparent" href="javascript:void(0)">PROGRAM</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/program_overview.html">Conference Overview</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/pdf/program_agenda.pdf">Program Agenda</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/program_speakers.html">Speakers and Presenters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/program_videos_2007.html">View Highlights of 2007 Stem Cell Summit</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">SPECIAL EVENTS</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/pdf/lab_on_the_lake_info.pdf">Lab on the Lake</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_dinner.html">Awards Dinner</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_lunch.html">Expert Lunch</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_breakfast.html">Advocacy Networking Breakfast</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_poster.html">Call for Posters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_report.html">2008 Stem Cell Report</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">REGISTRATION</a>
														<ul>
															<li><a href="https://www.synaxismeetings.com/summit2008/reg_register_2008.php">Registration</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/reg_travel.html">Travel Tips</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)"><span id="involved">GET INVOLVED</span></a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/brand_sponsor.html">Sponsor / Exhibit</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_report.html">Advertise</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_virtual.html">Virtual Exhibitors</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_poster.html">Call for Posters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="https://www.synaxismeetings.com/summit2008/reg_register_2008.php">Attend</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/recommend.html">Tell A Friend</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/contact.html">Contact Us</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">SPOTLIGHT</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/history.html">Stem Cell Events of the Genetics Policy Institute</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/press.html">In the News</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a href="http://www.worldstemcellsummit.com/sponsors.html">SPONSORS</a></li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a href="http://www.worldstemcellsummit.com/contact.html">CONTACT</a></li>
													<li class="qmclear">&nbsp;</li>
												</ul>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
					<!-- Ending Page Content [menu nests within] --><!-- Create Menu Settings: (Menu ID, Is Vertical, Show Timer, Hide Timer, On Click ('all', 'main' or 'lev2'), Right to Left, Horizontal Subs, Flush Left, Flush Top) -->
					<script type="text/javascript">qm_create(0,false,0,500,false,false,false,false,false);</script>
				</td>
			</tr>
		</table>
		<!--end nav_main--><!--begin content table--><!--end nav_main--><!--begin content table-->
	<table width="978" cellpadding="0" cellspacing="0" id="content">
	  <tr>
        <td id="col_text1"><h3>Registration Information<a href="https://www.synaxismeetings.com/summit2008/reg_registeruw_2008.php"><img src="index/SCRMC_logo.jpg" alt="" height="110" width="140" align="right" border="0" /></a></h3>
<b>September 22-23, 2008</b><br />
					<b>Alliant Energy Center</b><br />

            <b>Madison, Wisconsin USA</b><br />
          <br />
          <br />
          <br />
          For questions please call 323-663-9851 or email <a href="mailto:joseph@synaxismeetings.com"><span class="read">joseph@synaxismeetings.com</span></a><br />
          <br />
          <b>Registration begins Monday, March 17th.<br />
          <strong>Paid Registration Includes a ticket for the World Stem Cell Summit Awards Dinner, September 22.</strong><br />
          </b>
          <ul>
            <li>Industry &amp; for-profit company registration: <strong>$1495</strong></li>

            <li>Academic, government and non-profit rate: <b>$495</b></li>
            <li>Students: <strong>$250</strong><br />
            </li>
            <li>Limited media passes are available. For more information contact Robert Margolin at 908-605-4203 or <a href="mailto:rob@genpol.org"><span class="read">rob@genpol.org</span></a></li>
          </ul>
          <b>Cancellation Policy:</b>
          <ul>
            <li>Request for cancellation must be received in writing by September 8, 2008.</li>
            <li>No refunds will be granted for cancellations or no-shows after September 8, 2008.</li>

            <li>All cancellations will incur a <b>$150</b> processing fee.</li>
          </ul>
          Cancellation and other requests should be e-mailed to <a href="mailto:joseph@synaxismeetings.com"><span class="read">joseph@synaxismeetings.com</span></a> or<a href="mailto:rob@genpol.org"> <span class="read">rob@genpol.org</span></a> or faxed to 908-604-9414<br />

          <br />
          <strong>Please note:</strong> Each attendee needs to fill out their own registration form with the exception of Monday Annual Awards Dinner guests.
          Fields indicated with an * are required. <br />
            <form method="post" name="form1" id="form1" action="/kmitam/submit.php" onsubmit="return validateForm();">
              <table align="left" cellpadding="4">
                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top" nowrap="nowrap"><div align="left" class="style1"><br />
                    Primary Contact Information:</div></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>First Name*:</strong></div></td>
                  <td width="470" valign="top"><input name="Name" type="text" id="Name" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Last Name*:</strong></div></td>
                  <td valign="top"><input type="text" name="LastName" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Title:</strong></div></td>
                  <td valign="top"><input type="text" name="Title" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Institution:</strong></div></td>
                  <td valign="top"><input type="text" name="Institution" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" nowrap="nowrap"><strong>Please select the area that best<br />
                    represents your involvement:</strong></td>
                  <td valign="top"><select name="Profile">
                      <option value="Government/Non-Profit" >Government/Non-Profit</option>
                      <option value="Research Academia" >Research Academia</option>

                      <option value="Biotech" >Biotech</option>
                      <option value="Pharma" >Pharma</option>
                      <option value="Healthcare" >Healthcare</option>
                      <option value="Investment" >Investment</option>
                      <option value="Law" >Law</option>
                      <option value="Accounting" >Accounting</option>

                      <option value="Other" >Other</option>
                  </select></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Address*:</strong></div></td>
                  <td valign="top"><input type="text" name="Address" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>City*:</strong></div></td>
                  <td valign="top"><input type="text" name="City" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>State*:</strong></div></td>
                  <td valign="top"><input type="text" name="State" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Zip*:</strong></div></td>
                  <td valign="top"><input type="text" name="Zip" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Telephone*:</strong></div></td>
                  <td valign="top"><input type="text" name="Telephone" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Fax:</strong></div></td>
                  <td valign="top"><input type="text" name="Fax" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Email*:</strong></div></td>
                  <td valign="top"><input name="re_Email" type="text" id="re_Email" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">

                  <td colspan="2" align="left" valign="top"><div align="left"><br />
                          <span class="style1">Attendance Information:</span><br />
                    For accurate setup, you MUST confirm your participation to any of the following events: (Please check all that apply to you. Registered attendees only.)</div></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Monday Lunch<br />
                    September 22, 2008:</strong></div></td>

                  <td valign="top"><input type="checkbox" name="Lunch1" value="Y" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Monday Annual Awards Dinner<br />
                    September 22, 2008:</strong></div></td>
                  <td valign="top"><input type="checkbox" name="Dinner" value="Y" /></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Tuesday Lunch<br />
                    September 23, 2008:</strong></div></td>
                  <td valign="top"><input type="checkbox" name="Lunch2" value="Y" /></td>
                </tr>
                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top" nowrap="nowrap" class="style1"><br />
                    Registration Selections:</td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top"><div align="right"><strong>Registration*:<br />
                    </strong></div>
                      <br />                    <br /></td>
                  <td valign="top"><table>
                      <tr>

                        <td width="389"><input type="radio" id="RegistrationFee" name="RegistrationFee" value="1495" onclick="javascript:update_regamt(this.form,this.value);calcsum(this.form,150);" />
                          Main registration: <strong>$1495</strong></td>
                      </tr>
                      <tr>
                        <td><input type="radio" id="RegistrationFee" name="RegistrationFee" value="495" onclick="javascript:update_regamt(this.form,this.value);calcsum(this.form,150);" />
                          Academic, Gov't, Non-Profit &amp; Public Rate: <strong>$495</strong></td>
                      </tr>
                      <tr>
                        <td><input type="radio" id="RegistrationFee" name="RegistrationFee" value="250" onclick="javascript:update_regamt(this.form,this.value);calcsum(this.form,150);"  />
                          Student Rate: <strong>$250</strong></td>
                      </tr>
                      <tr>
                        <td><input type="radio" id="RegistrationFee4" name="RegistrationFee" value="Code" onclick="javascript:check_amt(this.form); calcsum(this.form,150);" />
                          Special Code: Enter Here <strong><label for="RegistrationFee4"></label>

                            <input name="clientID" id="clientID" size="5" maxlength="4" onkeyup="javascript:get_code_amount(this.form);">
                            $
                            <input name="CodeAmount" id="CodeAmount" size="5" maxlength="5" onBlur="form.Amount.value = value" READONLY ></strong></td>
                      </tr>
                      <tr>
                        <td><img name="loading" id="loading" src="loading.gif" width="155" style="display:none;"><label id="result" style="color:#FF0000"></label></td>
                      </tr>
                  </table></td>
                </tr>

                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top"><br />
                      <div align="left"><span class="style1">Guest Information for the Monday Annual Awards Dinner:</span></div></td>
                </tr>
                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top"><div align="left"><strong>I would like
                    <input name="Additional2" type="text" id="Additional2"  size="5" onkeyup="javascript: checknum(this.form,this.value,4); calcsum(this.form,150);" />
                    additional tickets to the Monday Annual Awards Dinner ($150 each).</strong></div></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Guest One:</strong></div></td>
                  <td valign="top"><input type="text" name="Guest1" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Guest Two:</strong></div></td>
                  <td valign="top"><input type="text" name="Guest2" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Guest Three:</strong></div></td>
                  <td valign="top"><input type="text" name="Guest3" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Guest Four:</strong></div></td>
                  <td valign="top"><input type="text" name="Guest4" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><strong>Guest Ticket Total:</strong></td>
                  <td valign="top">$
                    <input name="total" type="text" id="total" size="6" readonly value="0" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><strong>Registration Fee Total:</strong></td>

                  <td valign="top">$
                    <input name="Amount" type="text" id="Amount" size="6" readonly value="0" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><strong>Gross Total: </strong></td>
                  <td valign="top">$
                  <input name="Grosstotal" type="text" id="Grosstotal" size="6" readonly  value="0" /></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><strong>Click here to accept total amount*:</strong></td>
                <td valign="top"><input name="AcceptAmount" type="radio" id="AcceptAmount" value="Yes" />
                      <strong> Yes
                        <input name="AcceptAmount" type="radio" id="AcceptAmount2" value="No" />
                      <strong> No</strong></strong></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Payment Type*:</strong></div></td>
                  <td valign="top"><table>
                      <tr>
                        <td><input type="radio" name="PaymentType" value="Check" />
                          Payment by check, certified check or money order:</td>
                      </tr>
                      <tr>
                        <td>Make check payable to <strong>SYNAXIS Meetings &amp; Events, Inc</strong>. Check payment must be received within two weeks from your date of registration to be processed (see address below).<br />

                            <br /></td>
                    </tr>
                      <tr>
                        <td><input type="radio" name="PaymentType" value="NotRequired" />
                          Payment not required.</td>
                      </tr>
                      <tr>
                        <td><input type="radio" name="PaymentType" value="CreditCard" />

                          Payment by VISA or MC (only):</td>
                      </tr>
                  </table></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Card Holder's Name<br />
                    (as shown on credit card):</strong></div></td>

                  <td valign="top"><input type="text" name="CCName" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Credit Card Number:</strong></div></td>
                  <td valign="top"><input type="text" name="CCNumber" value="" size="32" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Expiration Month:</strong></div></td>

                  <td valign="top"><select name="CCMonth">
                      <option value="01" >01</option>
                      <option value="02" >02</option>
                      <option value="03" >03</option>
                      <option value="04" >04</option>
                      <option value="05" >05</option>

                      <option value="06" >06</option>
                      <option value="07" >07</option>
                      <option value="08" >08</option>
                      <option value="09" >09</option>
                      <option value="10" >10</option>
                      <option value="11" >11</option>

                      <option value="12" >12</option>
                    </select>                  </td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Expiration Year:</strong></div></td>
                  <td valign="top"><select name="CCYear">
                      <option value="2008" >2008</option>

                      <option value="2009" >2009</option>
                      <option value="2010" >2010</option>
                      <option value="2011" >2011</option>
                      <option value="2012" >2012</option>
                      <option value="2013" >2013</option>
                      <option value="2014" >2014</option>

                      <option value="2015" >2015</option>
                      <option value="2016" >2016</option>
                      <option value="2017" >2017</option>
                      <option value="2018" >2018</option>
                      <option value="2019" >2019</option>
                      <option value="2020" >2020</option>

                    </select>                  </td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>CVC (Security code on back of card):</strong></div></td>
                  <td valign="top"><input type="text" name="CVC" value="" size="4" /></td>
                </tr>
                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top"><div align="left">

                      <p><br />
                          <strong>Summit 2008 Hotels:</strong><br />
                          <br />
                        We have negotiated a discounted hotel room rate for our registered attendees, with Sheraton and Clarion. However, there are only a limited number of these special rate rooms available. It is on a first come first serve basis, so register early!<br />
                        <br />
                        CLARION SUITES<br />
                        2110 Rimrock Rd.<br />

                        Madison, Wisconsin 53713<br />
                        Phone: (608) 284-1234<br />
                        Fax: (608) 284-9401<br />
                        <br />
                        SHERATON MADISON HOTEL<br />
                        706 John Nolen Drive. <br />

                        Madison, Wisconsin 53713<br />
                        Phone: (608) 251-2300<br />
                        <a href="http://www.starwoodmeeting.com/StarGroupsWeb/res?id=0803243074&amp;key=EA541" target="_blank" class="read">www.starwoodmeeting.com</a> (direct link to Summit 2008 booking)<br />
                        <br />
                        HILTON MADISON MONONA TERRACE<br />
                        <a href="http://www.hilton.com" target="_blank" class="read">www.hilton.com</a><br />
                        9 East Wilson Street<br />
                      Madison, WI 53703<br />
                      Reservations are available at 608-255-5100 and ask for the World Stem Cell Summit block<br />
                        <br />
                      RADISSON HOTEL MADISON<br />
                        <a href="http://www.radisson.com" target="_blank" class="read">www.radisson.com</a><br />
                        517 Grand Canyon Dr<br />
                      Madison, WI 53719<br />
                      For reservations, guest may call the hotel direct at 608-833-0100 or our 1-800 line at 1-800-333-3333 and request the World Stem Cell Summit rate.<br />
                      <br />
                        <strong>2008 World Stem Cell Summit Official Travel Agency</strong><br />
                        <br />
                        POTHOS, Inc.<br />
                        Please email your travel requests to Michael;<br />
                        <a href="mailto:michael@pothos.us">michael@pothos.us</a><br />
                        +1.619.546.0621<br />
                        Please indicate the name of your conference, whether you are a speaker/presenter or attendee, home airport, preferred airlines &amp; frequent flyer number, if applicable, and seating preference (window or aisle).<br />                        
                        <br />
                      </p>
                      </div></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Arrival Date:</strong></div></td>
                  <td valign="top"><input type="text" name="ArrivalDate" id="ArrivalDate" value="" size="20" readonly /><a href="javascript:NewCal('ArrivalDate','yyyyMMdd')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date" /></a></td>
                </tr>
                <tr valign="baseline">

                  <td align="right" valign="top" nowrap="nowrap"><div align="right"><strong>Departure Date:</strong></div></td>
                  <td valign="top"><input type="text" name="DepartureDate" id="DepartureDate" value="" size="20" readonly /><a href="javascript:NewCal('DepartureDate','yyyyMMdd')"><img src="cal.gif" width="16" height="16" border="0" alt="Pick a date" /></a></td>
                </tr>
                <tr valign="baseline">
                  <td colspan="2" align="left" valign="top"><div align="left"><br />
                          <em>If paying by check</em>, please make check payable to <strong>SYNAXIS Meetings &amp; Events, Inc.</strong> and  send payment to:<br />

                    Joseph Chan<br />
                    SYNAXIS Meetings &amp; Events, Inc.<br />
                    8555 Santa Monica Blvd.<br />
                    West Hollywood, CA 90069
                    <p>Email:<a href="mailto:joseph@synaxismeetings.com"><span class="read">joseph@synaxismeetings.com</span></a><br />
                      Fax: 509-357-1217</p>

                  </div></td>
                </tr>
                <tr valign="baseline">
                  <td align="right" valign="top" nowrap="nowrap">&nbsp;</td>
                  <td valign="top"><div align="center">
                    <input name="formname" type="hidden" id="formname" value="Registration0.php" />
                      <input name="PaymentProcessed" type="hidden" id="PaymentProcessed" />
                      <input name="AmountCharged" type="hidden" id="AmountCharged" />
                      <input name="TotalGuests" type="hidden" id="TotalGuests" />
                      <input name="Notes" type="hidden" id="Notes" />
                      <input name="Otherreg" type="hidden" id="Otherreg" />
                      <input type="submit" value="Submit Registration" />
                      <br />
                      <br />

                      <table width="10" border="0" cellspacing="0" align="center">
                        <tr>
                          <td><script src="https://siteseal.thawte.com/cgi/server/thawte_seal_generator.exe"></script>                          </td>
                        </tr>
                        <tr>
                          <td height="0" align="center"><a style="color:#AD0034" target="_new"
   href="http://www.thawte.com/digital-certificates/"> <span style="font-family:arial; font-size:8px; color:#AD0034"> ABOUT SSL CERTIFICATES</span> </a> </td>
                        </tr>
                      </table>
                  </div></td>
                </tr>
              </table>
              <div align="left"></div>
              <div align="left" onfocus="FDK_AddRadioValidation('form1','document.form1.PaymentType',true,'\'This is a required field,please make a Payment Type selection.\'')">
                <input type="hidden" name="MM_insert" value="form1" />
              </div>

            </form>
          <p>&nbsp;</p></td>
      </tr>
	  <tr>
		<td id="col_left">&nbsp;</td>
	  </tr>
	</table>
<!--end content table-->

		
	</td>	
  </tr>

</table><!--end container table-->

</body>
</html>
	