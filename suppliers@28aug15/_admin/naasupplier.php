<?php require_once('../Connections/npw.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_naaasupplier = "-1";
if (isset($_GET['ID'])) {
  $colname_naaasupplier = $_GET['ID'];
}
mysql_select_db($database_npw, $npw);
$query_naaasupplier = "SELECT ID,Name,LastName,Email,RegistrationFee,Share,Separate,Amount,AcceptAmount,Card,CCName,CCNumber,CCMonth,CCYear,CVC FROM naasupplier2011 ORDER BY ID ASC";
$naaasupplier = mysql_query($query_naaasupplier, $npw) or die(mysql_error());
$row_naaasupplier = mysql_fetch_assoc($naaasupplier);
$totalRows_naaasupplier = mysql_num_rows($naaasupplier);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Supplier 2011 Credit Card Payments Received</title>
<link href="../css/basic.css" rel="stylesheet" type="text/css" />
</head>

<body>
<span class="headline">NAA Supplier 2011 Credit Card Payments Received</span>
<br />
<br />
<table border="1" cellpadding="4">
  <tr>
    <td><span class="text">ID</span></td>
    <td><span class="text">Name</span></td>
    <td><span class="text">LastName</span></td>
    <td><span class="text">Email</span></td>
    <td><span class="text">RegistrationFee</span></td>
    <td><span class="text">Share</span></td>
    <td><span class="text">Separate</span></td>
    <td><span class="text">Amount</span></td>
    <td><span class="text">AcceptAmount</span></td>
    <td><span class="text">Card</span></td>
    <td><span class="text">CCName</span></td>
    <td><span class="text">CCNumber</span></td>
    <td><span class="text">CCMonth</span></td>
    <td><span class="text">CCYear</span></td>
    <td><span class="text">CVC</span></td>
  </tr>
  <?php do { ?>
    <tr>
      <td><span class="text"><?php echo $row_naaasupplier['ID']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Name']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['LastName']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Email']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['RegistrationFee']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Share']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Separate']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Amount']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['AcceptAmount']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['Card']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['CCName']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['CCNumber']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['CCMonth']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['CCYear']; ?></span></td>
      <td><span class="text"><?php echo $row_naaasupplier['CVC']; ?></span></td>
    </tr>
    <?php } while ($row_naaasupplier = mysql_fetch_assoc($naaasupplier)); ?>
</table>
<br />
<br />
<a href="index.php"><span class="text">Return to admin home page</span></a><span class="text"></span><br />
</body>
</html>
<?php
mysql_free_result($naaasupplier);
?>
