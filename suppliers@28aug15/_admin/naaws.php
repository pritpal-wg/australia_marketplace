<?php require_once('../Connections/npw.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_naaws = "-1";
if (isset($_GET['ID'])) {
  $colname_naaws = $_GET['ID'];
}
mysql_select_db($database_npw, $npw);
$query_naaws = "SELECT ID,wsauthorize,CCName,CC_Type,CC_Number,CCMonth,CCYear,CVC,Company,delegate,Email FROM naaws2011 ORDER BY ID ASC";
$naaws = mysql_query($query_naaws, $npw) or die(mysql_error());
$row_naaws = mysql_fetch_assoc($naaws);
$totalRows_naaws = mysql_num_rows($naaws);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Wholesaler 2011 Credit Card Payments Received</title>
<link href="../css/basic.css" rel="stylesheet" type="text/css" />
</head>

<body>
<span class="headline">NAW Wholesaler 2011 Credit Card Payments Received</span>
<br />
<br />
<br />
<table border="1" cellpadding="4">
  <tr>
    <td><span class="text">ID</span></td>
    <td><span class="text">wsauthorize</span></td>
    <td><span class="text">CCName</span></td>
    <td><span class="text">CC_Type</span></td>
    <td><span class="text">CC_Number</span></td>
    <td><span class="text">CCMonth</span></td>
    <td><span class="text">CCYear</span></td>
    <td><span class="text">CVC</span></td>
    <td><span class="text">Company</span></td>
    <td><span class="text">delegate</span></td>
    <td><span class="text">Email</span></td>
  </tr>
  <?php do { ?>
    <tr>
      <td><span class="text"><?php echo $row_naaws['ID']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['wsauthorize']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CCName']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CC_Type']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CC_Number']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CCMonth']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CCYear']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['CVC']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['Company']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['delegate']; ?></span></td>
      <td><span class="text"><?php echo $row_naaws['Email']; ?></span></td>
    </tr>
    <?php } while ($row_naaws = mysql_fetch_assoc($naaws)); ?>
</table>
<br />
<br />
<a href="index.php" class="text">Return to admin home page</a>
</body>
</html>
<?php
mysql_free_result($naaws);
?>
