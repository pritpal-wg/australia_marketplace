<?php require_once('../Connections/npw.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_wholesaler = "-1";
if (isset($_GET['ID'])) {
  $colname_wholesaler = $_GET['ID'];
}
mysql_select_db($database_npw, $npw);
$query_wholesaler = "SELECT ID,wsauthorize,CCName,CC_Type,CC_Number,CCMonth,CCYear,CVC,Company,delegate,Email FROM ws2011 ORDER BY ID ASC";
$wholesaler = mysql_query($query_wholesaler, $npw) or die(mysql_error());
$row_wholesaler = mysql_fetch_assoc($wholesaler);
$totalRows_wholesaler = mysql_num_rows($wholesaler);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Wholesaler 2011 Credit Card Payments Received</title>
<link href="../css/basic.css" rel="stylesheet" type="text/css" />
</head>

<body>
<span class="headline">NPW Wholesaler 2011 Credit Card Payments Received</span><br />
<br />
<span class="text"><br />
<table border="1" cellpadding="4">
  <tr>
    <td>ID</td>
    <td>Authorize</td>
    <td>CC Name</td>
    <td>CC Type</td>
    <td>CC Number</td>
    <td>CC Month</td>
    <td>CC Year</td>
    <td>CVC</td>
    <td>Company</td>
    <td>Delegate</td>
    <td>Email</td>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_wholesaler['ID']; ?></td>
      <td><?php echo $row_wholesaler['wsauthorize']; ?></td>
      <td><?php echo $row_wholesaler['CCName']; ?></td>
      <td><?php echo $row_wholesaler['CC_Type']; ?></td>
      <td><?php echo $row_wholesaler['CC_Number']; ?></td>
      <td><?php echo $row_wholesaler['CCMonth']; ?></td>
      <td><?php echo $row_wholesaler['CCYear']; ?></td>
      <td><?php echo $row_wholesaler['CVC']; ?></td>
      <td><?php echo $row_wholesaler['Company']; ?></td>
      <td><?php echo $row_wholesaler['delegate']; ?></td>
      <td><?php echo $row_wholesaler['Email']; ?></td>
    </tr>
    <?php } while ($row_wholesaler = mysql_fetch_assoc($wholesaler)); ?>
</table>
<br />
<br />
<a href="index.php">Return to admin home page</a></span>
</body>
</html>
<?php
mysql_free_result($wholesaler);
?>
