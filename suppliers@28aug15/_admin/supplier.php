<?php require_once('../Connections/npw.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_supplier = "-1";
if (isset($_GET['ID'])) {
  $colname_supplier = $_GET['ID'];
}
mysql_select_db($database_npw, $npw);
$query_supplier = "SELECT ID,Name,LastName,Email,RegistrationFee,Share,Separate,Amount,AcceptAmount,Card,CCName,CCNumber,CCMonth,CCYear,CVC FROM supplier2011 ORDER BY ID ASC";
$supplier = mysql_query($query_supplier, $npw) or die(mysql_error());
$row_supplier = mysql_fetch_assoc($supplier);
$totalRows_supplier = mysql_num_rows($supplier);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Supplier 2011 Credit Card Payments Received</title>
<link href="../css/basic.css" rel="stylesheet" type="text/css" />
</head>

<body>
<span class="headline">NPW Supplier 2011 Credit Card Payments Received</span><br />
<br />
<table border="1" cellpadding="4">
  <tr>
    <td class="text">ID</td>
    <td class="text">First Name</td>
    <td class="text">Last Name</td>
    <td class="text">Email</td>
    <td class="text">Registration Fee</td>
    <td class="text">Share</td>
    <td class="text">Separate</td>
    <td class="text">Amount</td>
    <td class="text">Accept Amount</td>
    <td class="text">Card</td>
    <td class="text">CC Name</td>
    <td class="text">CC Number</td>
    <td class="text">CC Month</td>
    <td class="text">CC Year</td>
    <td class="text">CVC</td>
  </tr>
  <?php do { ?>
    <tr>
      <td class="text"><?php echo $row_supplier['ID']; ?></td>
      <td class="text"><?php echo $row_supplier['Name']; ?></td>
      <td class="text"><?php echo $row_supplier['LastName']; ?></td>
      <td class="text"><?php echo $row_supplier['Email']; ?></td>
      <td class="text"><?php echo $row_supplier['RegistrationFee']; ?></td>
      <td class="text"><?php echo $row_supplier['Share']; ?></td>
      <td class="text"><?php echo $row_supplier['Separate']; ?></td>
      <td class="text"><?php echo $row_supplier['Amount']; ?></td>
      <td class="text"><?php echo $row_supplier['AcceptAmount']; ?></td>
      <td class="text"><?php echo $row_supplier['Card']; ?></td>
      <td class="text"><?php echo $row_supplier['CCName']; ?></td>
      <td class="text"><?php echo $row_supplier['CCNumber']; ?></td>
      <td class="text"><?php echo $row_supplier['CCMonth']; ?></td>
      <td class="text"><?php echo $row_supplier['CCYear']; ?></td>
      <td class="text"><?php echo $row_supplier['CVC']; ?></td>
    </tr>
    <?php } while ($row_supplier = mysql_fetch_assoc($supplier)); ?>
</table>
<br />
<br />
<a href="index.php"><span class="text">Return to admin home page</span></a><span class="text"></span>
</body>
</html>
<?php
mysql_free_result($supplier);
?>
