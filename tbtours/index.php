<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tours Signup for Buyers and Sellers</title>
        <style>
            .formtext 
            { 
                color: #000000; 
                font-size: 12px; 
                font-family: Arial, Helvetica, Geneva, SunSans-Regular, sans-serif; 
                line-height: 18px; 
                text-align: left; 
            }

            .formheader 
            { 
                color: #000000; 
                font-size: 16px; 
                font-family: Arial, Helvetica, Geneva, SunSans-Regular, sans-serif; 
                line-height: 18px; 
                text-align: left; 
            }
        </style>
        <script>
        function fvalidation()
        {
            document.getElementById('error_fname').innerHTML="";
            document.getElementById('error_lname').innerHTML="";
            document.getElementById('error_buytype').innerHTML="";
            if(document.getElementById('fname').value=="")
            {
                document.getElementById('error_fname').innerHTML="This Field is Required.";
                document.getElementById('fname').focus();
                document.getElementById('error_fname').style.color="#F00";
                return false;
            }
            else if(document.getElementById('lname').value=="")
            {
                document.getElementById('error_lname').innerHTML="This Field is Required.";
                document.getElementById('lname').focus();
                document.getElementById('error_lname').style.color="#F00";
                return false;
            }
            else if(document.getElementById('buyer').checked == false && document.getElementById('supplier').checked == false)
            {
                document.getElementById('error_buytype').innerHTML="This Field is Required.";
                document.getElementById('buyer').focus();
                document.getElementById('error_buytype').style.color="#F00";
                return false;
            }
        }
        </script>
    
    </head>
    <body bgcolor="#000000">

    <div align="center" >
      <table width="1022" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th scope="col"><img src="images/tbheader.jpg" width="1022" height="253" alt="TB Header" /></th>
          </tr>
        <tr>
          <td align="center" bgcolor="#FFFFFF"><br>
                    <div style="float: left; margin-left: 30%;">
            <div>
                <h2 class="formheader">Tours</h2>
            </div>
            <form method="post" action="selecttour.php">
            <table class="formtext">
                <tr>
                    <td>
                        First Name:
                    </td>
                    <td>
                        <input type="text" name="fname" id="fname" placeholder="J.D."></input>
                        <span id="error_fname"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Last Name:
                    </td>
                    <td>
                        <input type="text" name="lname" id="lname" placeholder="Smith"></input>
                        <span id="error_lname"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Are you a buyer or supplier?
                    </td>
                    <td>
                        <input type="radio" name="buytype" value="Buyer" id="buyer">Buyer <input type="radio" name="buytype" value="Supplier" id="supplier">Supplier
<!--                        <select name="buytype" id="buytype">
                            <option value="">Select</option>
                            <option value="Buyer">Buyer</option>
                            <option value="Supplier">Supplier</option>
                        </select>-->
                        <span id="error_buytype"></span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" value="Submit" onclick="return fvalidation();"></input>
                    </td>
                </tr>
            </table>
            </form>
        </div>
                  <br>
                  <br>
<br></td>
        </tr>
        <tr>
          <td><img src="images/tbbase.jpg" alt="TB Footer" width="1022" height="335" usemap="#Map" /></td>
          </tr>
        </table>
      <map name="Map">
        <area shape="rect" coords="312,203,487,234" href="mailto:michael@torchbearers-info.com">
      </map>
    </div>
    </body>
</html>
