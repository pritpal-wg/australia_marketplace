<?php
include("config.php");

$rec=mysql_query("select * from buyer");
if(isset($_GET['edit']))
{
    
    $id=$_GET['edit'];
    $editid=mysql_fetch_array(mysql_query("select * from buyer where id='".$id."'"));
}

if(isset($_REQUEST['update']))
{
    $id=$_REQUEST['id'];
    $tourname=$_REQUEST['tourname'];
    $description=addslashes($_REQUEST['description']);
    $nooftours=$_REQUEST['nooftours'];
    $prvnooftours=$_REQUEST['prvnooftours'];
    $pendingtour=$_REQUEST['pendingtours'];    
    $status=$_REQUEST['status'];
    $addpendingtour=$nooftours-$prvnooftours;
    $pendtours=$pendingtour+$addpendingtour;
    if($pendtours>=0)
    {
    if($_FILES['tourimage']['name']!="")
            {
                //echo $_FILES['tourimage']['name'];die;
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $extension = end(explode(".", $_FILES["tourimage"]["name"]));
                if ((($_FILES["tourimage"]["type"] == "image/gif")
                || ($_FILES["tourimage"]["type"] == "image/jpeg")
                || ($_FILES["tourimage"]["type"] == "image/jpg")
                || ($_FILES["tourimage"]["type"] == "image/png"))                
                && in_array($extension, $allowedExts))
                {
                    $targetpath="../TourImages/";
                    $filename=md5(date("d m y h:i:s"))."_".$_FILES['tourimage']['name'];
                    $temppath=$_FILES['tourimage']['tmp_name'];
                    move_uploaded_file($temppath,$targetpath.$filename);
                    mysql_query("update buyer set tour_name='".$tourname."', image='".$filename."', description='".$description."', no_of_tours='".$nooftours."', pending_tours='".$pendtours."', status='".$status."' where id='".$id."'");
                    header("location:editBuyerTour.php?upd");
                }
                else
                {
                    header("location:editBuyerTour.php?ferror");
                }
            }
            else
            {
                mysql_query("update buyer set tour_name='".$tourname."', description='".$description."', no_of_tours='".$nooftours."', pending_tours='".$pendtours."', status='".$status."' where id='".$id."'");
                header("location:editBuyerTour.php?upd");
            } 
    }
    else
    {
        $msg="<h4 style='color:#F00;'>You can't updated no of tours value 
            because pending tours value can't be less then zero.</h4>";
        //header("location:editBuyerTour.php?error");
    }
}

if(isset($_GET['reset']) && $_GET['reset']!="")
{
    $id=$_GET['reset'];
    $rec12=mysql_query("select * from buyer where id='".$id."'");
    if(mysql_num_rows($rec12)!=0)
    {
        $rslt=mysql_fetch_array($rec12);
        $resetval=$rslt['no_of_tours'];
        mysql_query("update buyer set pending_tours='".$resetval."' where id='".$id."'");
        mysql_query("delete from tourbooking where booking_type='Buyer' and tour_id='".$id."'");
        header("location:editBuyerTour.php?succ");
    }
    else
    {
        header("location:editBuyerTour.php?error");
    }
}

if(isset($_GET['delete']) && $_GET['delete']!="")
{
    $id=$_GET['delete'];
    $rec12=mysql_query("select * from buyer where id='".$id."'");
    $rec123=mysql_query("select * from tourbooking where booking_type='Buyer' and tour_id='".$id."'");
  
        if(mysql_num_rows($rec12)!=0)
        {
            if(mysql_num_rows($rec123)==0)
            {                
                mysql_query("delete from buyer where id='".$id."'");
                
                header("location:editBuyerTour.php?delsucc");
            }
            else
            {
                header("location:editBuyerTour.php?errmsg");
            }
        }
        else
        {
            header("location:editBuyerTour.php?errdel");
        }
    
}
?>

<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script>
        function fvalidation()
        {
            document.getElementById('error_tourname').innerHTML="";
            document.getElementById('error_description').innerHTML="";
            document.getElementById('error_nooftours').innerHTML="";
            
            
            if(document.getElementById('tourname').value=="")
            {
                document.getElementById('error_tourname').innerHTML="This Field is Required.";
                document.getElementById('tourname').focus();
                document.getElementById('error_tourname').style.color="#F00";
                return false;
            }
            else if(document.getElementById('description').value=="")
            {
                document.getElementById('error_description').innerHTML="This Field is Required.";
                document.getElementById('description').focus();
                document.getElementById('error_description').style.color="#F00";
                return false;
            }
            else if(document.getElementById('nooftours').value=="")
            {
                document.getElementById('error_nooftours').innerHTML="This Field is Required.";
                document.getElementById('nooftours').focus();
                document.getElementById('error_nooftours').style.color="#F00";
                return false;
            }
            else if(isNaN(document.getElementById("nooftours").value)!=false)
                {
                    document.getElementById('error_nooftours').innerHTML="Please enter only number values.";
                document.getElementById('nooftours').focus();
                document.getElementById('error_nooftours').style.color="#F00";
                return false;
                }            
        }
        </script>
    </head>
    <body>
         <div style="float: left;">
            <div>
                <h2>
                    <?php
                        if(isset($_GET['edit']))
                        {
                            echo "Update Buyer Tour Program";
                        }
                        else
                        {
                            echo "Buyer Tour Program";
                        }
                    ?>                    
                </h2>
            </div>
             <?php
                if(isset($_GET['edit']))
                {
             ?>
            <div>
            <table>
                <form method="post" enctype="multipart/form-data">
                <tr>
                    <td>
                        Tour Name:
                        <input type="hidden" name="id" value="<?php if(isset($_GET['edit'])) echo $editid['id']?>"></input>
                    </td>
                    <td>
                        <input type="text" name="tourname" id="tourname" value="<?php if(isset($_GET['edit'])) echo $editid['tour_name']?>"></input>
                        <span id="error_tourname"></span>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                        <textarea name="description" id="description" rows="5" cols="30"><?php if(isset($_GET['edit'])) echo stripslashes ($editid['description']);?></textarea>
                        <span id="error_description"></span>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        No of Tours:
                    </td>
                    <td>
                        <input type="text" name="nooftours" id="nooftours" size="4" value="<?php if(isset($_GET['edit'])) echo $editid['no_of_tours']?>"></input>
                        <span id="error_nooftours"></span>
                        <input type="hidden" name="prvnooftours" value="<?php if(isset($_GET['edit'])) echo $editid['no_of_tours']?>"></input>
                        <input type="hidden" name="pendingtours" value="<?php if(isset($_GET['edit'])) echo $editid['pending_tours']?>"></input>
                    </td>                    
                </tr>                
                <tr>
                    <td>
                        Tour Image:
                    </td>
                    <td>
                        <input type="file" name="tourimage" id="tourimage"></input>                        
                    </td>                    
                </tr>
                <tr>
                    <td>
                        Status:
                    </td>
                    <td>
                        <select name="status">
                            <option value="1"<?php if(isset($_GET['edit'])) {if($editid['status']=="1") echo "selected"; } ?>>Active</option>
                            <option value="0"<?php if(isset($_GET['edit'])) {if($editid['status']=="0") echo "selected"; } ?>>Deactive</option>
                        </select>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        <?php 
                        if(isset($_GET['edit']) && $_GET['edit']!="")
                        {                            
                        ?>
                        <input type="submit" name="update" value="Update" onclick="return fvalidation();"></input>
                        <input type="button" name="Cancel" Value="Cancel" onclick="javascript:window.location='editBuyerTour.php'"></input>
                        <?php
                        }
                        ?>
                    </td>                    
                </tr>
                </form>
            </table>            
                    
            </div>
             <?php
                }
                ?>
             <div>
                 <?php                         
                        if(isset($msg)) 
                        { 
                            echo $msg;                            
                        }
                        if(isset($_GET['upd'])) 
                        { 
                            echo "<h4 style='color:#00cc66;'>Record updated successfully.</h4>";                            
                        }
                        if(isset($_GET['ferror']))
                        {
                            echo "<h4 style='color:#F00;'>Invalid File Type.</h4>";
                        }
                        if(isset($_GET['error']))
                        {
                            echo "<h4 style='color:#F00;'>Invalid tour reset request.</h4>";
                        }
                        if(isset($_GET['succ']))
                        {
                            echo "<h4 style='color:#00cc66;'>Tour reset successfully.</h4>";
                        }
                        if(isset($_GET['errdel']))
                        {
                            echo "<h4 style='color:#F00;'>Invalid tour delete request.</h4>";
                        }
                        if(isset($_GET['errmsg']))
                        {
                            echo "<h4 style='color:#F00;'>Your are not permitted to delete this tour because users buy this tour. Please reset your tour first after that your are able to delete this tour. </h4>";
                        }
                        if(isset($_GET['delsucc']))
                        {
                            echo "<h4 style='color:#00cc66;'>Tour deleted successfully.</h4>";
                        }
                    ?>
             </div>
             <div>
                 <table style="border: 1px solid #FFF">
                     <tr style="background-color: #0084C1; height: 35px; color: #FFF;">
                         <td>
                             S.No.
                         </td>
                         <td style="text-align: center;">
                             Image
                         </td>
                         <td>
                             Tour Name
                         </td>
                         <td style="width: 300px;">
                             Description
                         </td>
                         <td>
                             Date/Time
                         </td>
                         <td>
                             No of Tours
                         </td>
                         <td>
                             Pending Tours
                         </td>
                         <td style="width: 70px; text-align: center;">
                             Status
                         </td>
                         <td style="text-align: center;">
                             Action
                         </td>
                     </tr>
                     
                         <?php
                         $sr=1;
                         while($record=  mysql_fetch_array($rec))
                         {
                         ?>
                        <tr style="background-color: #F3F3F3;">
                         <td><?php echo $sr;?></td>
                         <td><a href="../TourImages/<?php echo $record['image']; ?>" target="_blank"><img src="../TourImages/<?php echo $record['image']; ?>" height="50" width="50"></img></a></td>
                         <td><?php echo $record['tour_name']; ?></td>
                         <td><?php echo stripslashes($record['description']); ?></td>
                         <td><?php echo date("F d, Y h:i:s",  strtotime($record['datentime'])); ?></td>
                         <td style="text-align: center;"><?php echo $record['no_of_tours']; ?></td>
                         <td style="text-align: center;"><?php echo $record['pending_tours']; ?></td>
                         <td style="text-align: center;"><?php if($record['status']==1) {echo "Active";} else { echo "Not Active"; } ?></td>
                         <td>
                             &nbsp;
                             <a href="editBuyerTour.php?edit=<?php echo $record['id']; ?>" style="text-decoration: none;">
                                 <img src="edit.png" height="20" width="20" title="Edit"></img>
                             </a>&nbsp;
                             <img src="xls.png" height="20" width="20" title="Download xls file" onclick='javascript:window.location="makecsvfile.php?dwload=<?php echo $record['id']; ?>&buyer"' style="cursor: pointer;"></img>
                             &nbsp;
                             <a onclick="javascript:r=confirm('Are you sure want to delete this event. If yes press &quot;OK&quot; or press &quot;Cancel&quot; to go back.'); if(r==true){ window.location='editBuyerTour.php?delete=<?php echo $record['id']; ?>'}" style="text-decoration: none; cursor: pointer;">
                                 <img src="delete.png" title="Delete"></img>
                             </a>&nbsp;
                             <img src="icon_reset.gif" height="20" width="20" title="Reset Tour" onclick="javascript:r=confirm('Resetting the tour will permanently delete all of its subscribers. However, you can download current list of subscribers by clicking\n&quot;Excel Sheet&quot; icon.  Press &quot;OK&quot; if you want to proceed with deleting or press &quot;Cancel&quot; to go back.'); if(r==true){ window.location='editBuyerTour.php?reset=<?php echo $record['id']; ?>'}" style="cursor: pointer;"></img>&nbsp;
                         </td>
                         </tr>
                         <?php 
                         $sr++;
                         }
                         ?>
                     
                 </table>
             </div>
        </div>
    </body>
</html>
