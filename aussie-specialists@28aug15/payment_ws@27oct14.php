<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>Australia Marketplace 2015 Premier Aussie Specialists Prospectus</title>
        <link href="css/basic.css" rel="stylesheet" type="text/css" media="all" />
	<style type="text/css">
	.red {color:red; }
    </style>

        <style type="text/css" media="screen">
        <!--ul
            {
                margin-left: 0;
                padding-left: 0;
                list-style-type: none;
                margin-top: 0px;
            }

            li
            {
                padding-left: 0;
                margin-left: 0;
                background-image: url(images/blue_arrow_bullet.jpg);
                background-repeat: no-repeat;
                background-position: 0 0.4em;
                padding-left: 1.1em;
                margin: 0em 0;}.red1 {color:red; }
.red1 {color:red; }
-->
        </style>
            <script type="text/javascript"><!--
                function validateForm() {
                    with (document.Payment) {
                        var alertMsg = "The following REQUIRED fields\nhave been left empty:\n";
                        radioOption = -1;
                        for (counter=0; counter<wsauthorize.length; counter++) {
                            if (wsauthorize[counter].checked) radioOption = counter;
                        }
                        if (radioOption == -1) alertMsg += "\nAuthorization";

                        if(wsauthorizeNo.checked){
                            alertMsg +="\nPlease select authorization value 'Yes'";
                        }
                        if (Company.value == "") alertMsg += "\nCompany";
                        if (delegate.value == "") alertMsg += "\nDelegate";
                        if (Email.value == "") alertMsg += "\nEmail";
                        if(Email.value !="")
                        {
                            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            var result=re.test(Email.value);
                            if(result==false)
                            {
                                alertMsg +="\nInvalid email type";
                            }
                        }
                        if (CCName.value == "") alertMsg += "\nCC Name";
                        if (CC_Type.value == "") alertMsg += "\nCC Type";
                        if (CC_Number.value == "") alertMsg += "\nCC Number";
                        if (CCMonth.options[CCMonth.selectedIndex].value == "") alertMsg += "\nCC Month";
                        if (CCYear.options[CCYear.selectedIndex].value == "") alertMsg += "\nCC Year";
                        if (CVC.value == "") alertMsg += "\nCVC";
                        if (alertMsg != "The following REQUIRED fields\nhave been left empty:\n") {
                            alert(alertMsg);
                            return false;
                        } else {
                            return true;
                        } } }

                //-->
            </script>
        </head>

        <body bgcolor="#315680" topmargin="0" marginheight="0">
            <div align="center">
                <table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
                    <tr>
                        <td width="25"><img src="images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
                        <td width="400"></td>
                        <td width="200"></td>
                        <td width="0"><img src="images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
                    </tr>
                    <tr>
                        <td rowspan="3" width="25" background="images/shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                        <td colspan="2" width="600"><img src="images/npw_header_ws.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
                        <td rowspan="3" width="0" background="images/shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="600">
                            <menumachine name="npw_ws" id="m8imulj">
                            <csobj t="Component" csref="menumachine/npw_ws/menuspecs.menudata"><noscript>
                                    <p><a class="mm_no_js_link" href="menumachine/npw_ws/navigation.html">Site Navigation</a></p>
                                </noscript> </csobj>
                            <script type="text/javascript"><!--
                                var mmfolder=/*URL*/"menumachine/",zidx=1000;
                                //--></script>
                            <script type="text/javascript" src="menumachine/menumachine2.js"></script>
                            <script type="text/javascript" src="menumachine/npw_ws/menuspecs.js"></script>
                        </menumachine>
                    </td>
                </tr>
                <tr height="570">
                    <td colspan="2" valign="top" width="600" height="570">
                        <table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
                                        <tr>
                                            <td valign="top" width="1"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                            <td valign="top" class="headline"><span class="dsR7">Wholesaler Payment</span></td>
                                            <td valign="top" class="headline2" width="2"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text"><strong class="textboldred">Please read before entering credit card information: Please do not pay your registration fee until you have received an official application acceptance confirmation email. All payment submissions here will be processed immediately.</strong><br />
                                              <br />
                                            Please complete and submit this form to pay by credit card. All fields are required.</td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">
                                              <form id="Payment" action="ProcessForm2.php" method="post" name="Payment" onsubmit="return validateForm()">
                                                    <table border="0" cellspacing="0" cellpadding="4">
                                                        <tr>
                                                            <td class="text" colspan="6"><span class="textboldred">I authorize the event organizers to charge my credit card $199. 
                                                                <input type="radio" name="wsauthorize" value="Yes" id="wsauthorizeYes" /> Yes <input type="radio" name="wsauthorize" value="No" id="wsauthorizeNo"/> No<br />
                                                                    <br />
																	Mastercard or VISA only accepted.</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text">Name on CC</td>
                                                            <td class="text">
                                                                <div align="left">
																		CC Type</div>
                                                            </td>
                                                            <td class="text">
                                                                <div align="left">
																		Credit Card #</div>
                                                            </td>
                                                            <td class="text">
                                                                <div align="left">
																		Expiration <br />
																  Month</div>
                                                            </td>
                                                            <td class="text">Expiration <br />
														    Year</td>
                                                            <td class="text">CVC Code</td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" name="CCName" size="24" /></td>
                                                            <td>
                                                                <div align="left">
                                                                    <select name="CC_Type" size="1">
                                                                        <option selected="selected"></option>
                                                                        <option value="VISA">VISA</option>
                                                                        <option value="MC">MC</option>
                                                                    </select></div>
                                                            </td>
                                                            <td>
                                                                <div align="left">
                                                                    <input type="text" name="CC_Number" size="17" /></div>
                                                            </td>
                                                            <td>
                                                                <div align="left">
                                                                    <select name="CCMonth">
                                                                        <option selected="selected"></option>
                                                                        <option value="01" >01</option>
                                                                        <option value="02" >02</option>
                                                                        <option value="03" >03</option>
                                                                        <option value="04" >04</option>
                                                                        <option value="05" >05</option>
                                                                        <option value="06" >06</option>
                                                                        <option value="07" >07</option>
                                                                        <option value="08" >08</option>
                                                                        <option value="09" >09</option>
                                                                        <option value="10" >10</option>
                                                                        <option value="11" >11</option>
                                                                        <option value="12" >12</option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td><select name="CCYear">
                                                                    <option selected="selected"></option>
                                                                    <option value="2014" >2014</option>
                                                                    <option value="2015" >2015</option>
                                                                    <option value="2016" >2016</option>
                                                                    <option value="2017" >2017</option>
                                                                    <option value="2018" >2018</option>
                                                                    <option value="2019" >2019</option>
                                                                    <option value="2020" >2020</option>
                                                                    <option value="2021">2021</option>
                                                                    <option value="2022">2022</option>
                                                                    <option value="2023">2023</option>
                                                                </select></td>
                                                            <td><input type="text" name="CVC" size="3" />														      <br />
														    (on back of card)</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6">Company you are paying registration fee for: <input type="text" name="Company" size="56" /><br />
                                                                <br />
																Delegate name: <input type="text" name="delegate" size="56" /><br />
                                                                <br />
																Email: <input type="text" name="Email" size="56" id="Email" /></td>
                                                        </tr>
                                                    </table>
                                                    <input name="recipient" type="hidden" id="0" value="0" />
                                                    <input name="subject" type="hidden" id="Wholesaler Credit Card Submission" value="Wholesaler Credit Card Submission" />
                                                    <input name="sender_email" type="hidden" id="email" value="Email" />
                                                    <input name="redirect" type="hidden" id="ws_thankyou.html" value="ws_thankyou.html" />
                                                    <input name="exclude" type="hidden" id="exclude" value="wsauthorize,CCName,CC_Type,CC_Number,CCMonth,CCYear,CVC" />
                                                    <input name="sort" type="hidden" id="sort" value="Company" />
                                                    <input name="redirect_type" type="hidden" id="redirect_type" />
                                                    <input name="write_to_mysql" type="hidden" id="write_to_mysql" value="wsauthorize,Company,delegate,Email,CCName,CC_Type,CC_Number,CCMonth,CCYear,CVC" />
                                                    <input name="mysql_table" type="hidden" id="mysql_table" value="ws2011" /><br />
                                                    <input type="submit" name="submitButtonName" />
                                                    <br />
                                                    <br />
                                              </form>
                                          
                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td class="text" valign="top"><!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
<a href="http://ssl.comodo.com">
<img src="images/comodo_secure_100x85_white.png" alt="SSL Certificate" width="100" height="85" style="border: 0px;"><br> <span style="font-weight:bold; font-size:7pt">SSL Certificate</span></a><br>
<!-- end  GeoTrust Smart Icon tag --><br />
                                            </td
                                            ><td valign="top" class="text" width="2"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="25"><img src="images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                    <td colspan="2" width="600" background="images/center_bottom_shadow.jpg"></td>
                    <td width="0"><img src="images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                </tr>
                <tr height="25">
                    <td width="25" height="25" background="images/top_shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                                        <table width="800" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bottom_text">
                                                    <div align="center">Copyright 2007-2015. Australian States and Territories and Tourism Australia. All Rights Reserved.</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div></td>
                            </tr>
                        </table></td>
                    <td width="0" height="25" background="images/top_shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td width="25"><img src="images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                    <td colspan="2" width="600" background="images/bottom_shadow_blue.jpg"></td>
                    <td width="0"><img src="images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                </tr>
            </table>
        </div>
        <p></p>
    </body>

</html>