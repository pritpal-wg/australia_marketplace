<?php
if (!isset($_POST) && empty($_POST) && $_POST['wsauthorize']=='No') {
    header('Location: payment_ws.php');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>NPW Prospectus 2014</title>
        <link href="css/basic.css" rel="stylesheet" type="text/css" media="all" />

        <script type="text/javascript" language="javascript" src="async.js"></script>

    </head>

    <body bgcolor="#315680" topmargin="0" marginheight="0">
        <div align="center">
            <table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
                <tr>
                    <td width="25"><img src="images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
                    <td width="400"></td>
                    <td width="200"></td>
                    <td width="0"><img src="images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
                </tr>
                <tr>
                    <td rowspan="3" width="25" background="images/shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td colspan="2" width="600"><img src="images/npw_header_ws.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
                    <td rowspan="3" width="0" background="images/shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td colspan="2" width="600">
                        <menumachine name="npw_ws" id="m8imulj">
                            <csobj t="Component" csref="menumachine/npw_ws/menuspecs.menudata"><noscript>
                                    <p><a class="mm_no_js_link" href="menumachine/npw_ws/navigation.html">Site Navigation</a></p>
                                </noscript> </csobj>
                            <script type="text/javascript"><!--
                                var mmfolder=/*URL*/"menumachine/",zidx=1000;
                                //--></script>
                            <script type="text/javascript" src="menumachine/menumachine2.js"></script>
                            <script type="text/javascript" src="menumachine/npw_ws/menuspecs.js"></script>
                        </menumachine>
                    </td>
                </tr>
                 <tr height="570">
                    <td colspan="2" valign="top" width="600" height="570">
                        <table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
                                        <tr>
                                            <td valign="top" width="1"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                            <td valign="top" class="headline"><span class="dsR7">Wholesaler Payment Preview</span></td>
                                            <td valign="top" class="headline2" width="2"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">
                                                <form method="post" name="form1" id="form1" action="authorize_net.php">
                                                    <table width="752" align="left" cellpadding="4">
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top" nowrap="nowrap">
                                                                <div align="left" class="style1">
                                                                    <br />
                                                                    <span class="textboldred"> Primary Contact Information:</span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Company you are paying registration fee for:</strong></div>
                                                            </td>
                                                            <td class="text" width="470" valign="top"><?php echo $_POST['Company'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Delegate name:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><?php echo $_POST['delegate'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Email:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><?php echo $_POST['Email'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top"><span class="textboldred"><br />
                                                                </span>
                                                                <div align="left">
                                                                    <span class="textboldred"><span class="style1"><strong>Total and Payment Information:</strong></span></span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>TOTAL AMOUNT:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                $199
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Payment Type:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CC_Type']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Card Holder's Name<br />
								(as shown on credit card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCName']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Credit Card Number:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CC_Number']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Month:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCMonth']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Year:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCYear']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>CVC <br />
							(Security code on back of card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CVC']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">&nbsp;</td>
                                                            <td class="text" valign="top"><div align="left">
                                                                    <input type="hidden" name="CompanyName" value="<?php echo $_POST['Company']; ?>" />
                                                                    <input type="hidden" name="DelegateName" value="<?php echo $_POST['delegate']; ?>" />
                                                                    <input type="hidden" name="Email" value="<?php echo $_POST['Email'];?>" />
                                                                    <input type="hidden" name="Amount" value="199" />
                                                                    <input type="hidden" name="Card" value="<?php echo $_POST['CC_Type'];?>" />
                                                                    <input type="hidden" name="CCName" value="<?php echo $_POST['CCName'];?>" />
                                                                    <input type="hidden" name="CCNumber" value="<?php echo $_POST['CC_Number'];?>" />
                                                                    <input type="hidden" name="CCMonth" value="<?php echo $_POST['CCMonth'];?>" />
                                                                    <input type="hidden" name="CCYear" value="<?php echo $_POST['CCYear'];?>" />
                                                                    <input type="hidden" name="CVC" value="<?php echo $_POST['CVC'];?>" />
                                                                    <input name="redirect" type="hidden" id="ws_thankyou.html" value="ws_thankyou.html" />

                                                                    <br />
                                                                    <input type="button" name="Back" onclick="javascript:history.back();" Value="Back" />
                                                                    <input type="submit" value="Make Payment" name="makepayment"/><br />
                                                                    <br /><div id="thawteseal" style="text-align:center;" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
                                                                        <div><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.synaxismeetings.com&amp;size=M&amp;lang=en"></script></div>
                                                                        <div><a href="http://www.thawte.com/ssl-certificates/" target="_blank" style="color:#000000; text-decoration:none; font:bold 10px arial,sans-serif; margin:0px; padding:0px;"><!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
<script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
<!-- end  GeoTrust Smart Icon tag --></a></div>
                                                                    </div>

                                                                </div></td>
                                                        </tr>
                                                    </table>
                                                    <div align="left"></div>
                                                    <div align="left" onfocus="FDK_AddRadioValidation('form1','document.form1.PaymentType',true,'\'This is a required field,please make a Payment Type selection.\'')">
                                                        <input type="hidden" name="MM_insert" value="form1" /></div>
                                                </form>
                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">

                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td class="text" valign="top"><br />
                                            </td>
                                            <td valign="top" class="text" width="2"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="25"><img src="images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                    <td colspan="2" width="600" background="images/center_bottom_shadow.jpg"></td>
                    <td width="0"><img src="images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                </tr>
                <tr height="25">
                    <td width="25" height="25" background="images/top_shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                                        <table width="800" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bottom_text">
                                                    <div align="center">
														Copyright 2007-2015. Australian States and Territories and Tourism Australia. All Rights Reserved.</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div></td>
                            </tr>
                        </table></td>
                    <td width="0" height="25" background="images/top_shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td width="25"><img src="images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                    <td colspan="2" width="600" background="images/bottom_shadow_blue.jpg"></td>
                    <td width="0"><img src="images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                </tr>
            </table>
        </div>
        <p></p>
    </body>

</html>