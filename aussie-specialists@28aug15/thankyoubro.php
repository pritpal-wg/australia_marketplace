<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>NPW Prospectus 2014</title>
		<link href="css/basic.css" rel="stylesheet" type="text/css" media="all" />
		<style type="text/css" media="screen"><!--ul
{
	margin-left: 0;
	padding-left: 0;
	list-style-type: none;
	margin-top: 0px;
}

li
{
padding-left: 0;
margin-left: 0;
background-image: url(images/blue_arrow_bullet.jpg);
background-repeat: no-repeat;
background-position: 0 0.4em;
padding-left: 1.1em;
margin: 0em 0;}--></style>
	</head>

	<body bgcolor="#315680" topmargin="0" marginheight="0">
		<div align="center">
			<table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
				<tr>
					<td width="25"><img src="images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
					<td width="400"></td>
					<td width="200"></td>
					<td width="0"><img src="images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
				</tr>
				<tr>
					<td rowspan="3" width="25" background="images/shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
					<td colspan="2" width="600"><img src="images/npw_header.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
					<td rowspan="3" width="0" background="images/shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
				</tr>
				<tr>
					<td colspan="2" width="600">
						<menumachine name="npw" id="m3fs0c71">
							<csobj t="Component" csref="menumachine/npw/menuspecs.menudata"><noscript>
									<p><a class="mm_no_js_link" href="menumachine/npw/navigation.html">Site Navigation</a></p>
								</noscript> </csobj> 
							<script type="text/javascript"><!--
var mmfolder=/*URL*/"menumachine/",zidx=1000;
//--></script>
							<script type="text/javascript" src="menumachine/menumachine2.js"></script>
							<script type="text/javascript" src="menumachine/npw/menuspecs.js"></script>
							<script type="text/javascript"><!--//settings
pkg.hl("m3fs0c70",1);
//settings--></script>
						</menumachine>
					</td>
				</tr>
				<tr height="570">
					<td colspan="2" valign="top" width="600" height="570">
						<table width="798" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
								  <tr>
											<td valign="top" width="1"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
											<td valign="top" class="headline"><span class="dsR7">Supplier Brochure Upload</span></td>
											<td valign="top" class="headline2" width="2"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td valign="top" class="text">We have received your files, thanks!</td>
											<td valign="top" class="headline2" width="2"></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td valign="top" class="text">
												
											</td>
											<td valign="top" class="headline2" width="2"></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td class="text" valign="top"><br />
											</td>
										  <td valign="top" class="text" width="2"></td>
										</tr>
									</table>
							  </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="25"><img src="images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
					<td colspan="2" width="600" background="images/center_bottom_shadow.jpg"></td>
					<td width="0"><img src="images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
				</tr>
				<tr height="25">
					<td width="25" height="25" background="images/top_shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
					<td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                              <table width="800" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td class="bottom_text">
													<div align="center">
														Copyright 2007-2015. Australian States and Territories and Tourism Australia. All Rights Reserved.</div>
												</td>
                                </tr>
                              </table>
                          </div></td>
                        </tr>
                    </table></td>
				  <td width="0" height="25" background="images/top_shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
			  </tr>
				<tr>
					<td width="25"><img src="images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
					<td colspan="2" width="600" background="images/bottom_shadow_blue.jpg"></td>
					<td width="0"><img src="images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
				</tr>
			</table>
		</div>
	<p></p>
	</body>

</html>