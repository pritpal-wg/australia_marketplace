<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

    <head>
        <meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
        <title>Payment Error</title>
        <style type="text/css" media="screen"><!--
            body, div, td, p { background-color: white }
            .MPinfo { color: black; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPFieldNames { color: black; font-weight: bold; font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPFieldValues  { color: #00327d; font-style: normal; font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPthankyou   { color: #00327d; font-size: 36px; font-family: "Times New Roman", Times, Georgia, serif }
            .MPerror { color: #c80019; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPerrorlist { color: #c80019; font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPsmall { color: black; font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPsubhead { color: #7d8287; font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif }
            .MPlink   { color: #00327d; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none }
            .MPlink a:link  { color: #00327d; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none }
            .MPlink a:visited  { color: #00327d; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none }
            .MPlink a:hover { color: #0093ff; font-size: 13px; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline }
            .MPcredit { color: #7d8287; font-size: 9px; font-family: Verdana, Arial, Helvetica, sans-serif }
            --></style>
    </head>

    <body>
        <div align="center">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>

                    <td align="left" valign="top">
                        <br>&nbsp;<br><span class="MPerror">Error on Payment Detail. Please fill the form again and provide correct payment detail.</span><br>&nbsp;<br>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="MPinfo">
					[ <span class="MPlink"><a href="http://www.synaxismeetings.com/events/npw/payment/payment_supplier.php">back to form</a></span> ]<br>
                    </td>
                </tr>
            </table>
        </div>
    </body>

</html>