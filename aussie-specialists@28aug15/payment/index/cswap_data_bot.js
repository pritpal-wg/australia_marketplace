

/**********************************************************************************************

                               Content Swap Items and Content

**********************************************************************************************/

/*  

        Note:  Each <li... tag below creates a new swap item, you may add as many as you wish.  Terminate each line with 
               a backslash '\'.

     Warning:  Double check that there are no extra white spaces after your terminating '\' backslashes, the swap will fail to
               load if there are extra spaces present.

*/


document.write("\
\
<ul id='cswap0' style='position:relative;display:none;z-index:0;'>\
\
	<li><img src='spon/logo_nabt.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_ssscr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_research.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_camr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_wtc.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_btci.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_thrive.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_fc.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_prevent.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_als.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_parkinsons.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_children.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_spinal.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_tamr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_mcscrc.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_aar.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_a4m.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_u2fp.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_brooke.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_wscn.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_womens.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_candidate.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cfc.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_act.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_assoz.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_r4c.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cures.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_scf.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_nahtf.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cal_hep.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_lupus.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_scan.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_heumann.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_your_congress.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_fsr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_nfr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cap.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_ben.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_regen.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_pharma.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_stemnet.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_ft.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cio.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cdn.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_nyamr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cvdn.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cns.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_dj_venture.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_drug.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_ctn.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_scp.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_sbc.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_pci.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_biolaw.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_kansas.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_reeves.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_missouri.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_missouri.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cscrt.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_sam.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_sam.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_bmc.jpg' style= 'padding: 10px 0 0 0'></li>\
</ul>\
\
</div>");





/**********************************************************************************************

                               Customizable Options and Styles

**********************************************************************************************/

node7 = true

function cswapdata0()
{


    /*---------------------------------------------
    Content Dimensions
    ---------------------------------------------*/

	this.container_width = 205
	this.container_height = 210



    /*---------------------------------------------
    Message Timing
    ---------------------------------------------*/

	this.initial_swap_delay = 1			//measured in seconds
	this.swap_delay = 3				//measured in seconds

	

    /*---------------------------------------------
    Container Styles and Padding
    ---------------------------------------------*/

	this.container_padding = "0,0,0,0"
	this.container_styles = "";



    /*---------------------------------------------
    Container Styles and Padding
    ---------------------------------------------*/
	
	this.item_styles = "color:#000000; text-decoration:none; font-family:Arial; font-size:13px; border-style:none;border-width:0px;";
	this.item_link_styles = "color:#0033cc; text-decoration:none; font-family:Arial; font-size:13px; border-style:none;border-width:0px;";
	this.item_link_hover_styles = "color:#0033cc; text-decoration:underline; font-family:Arial; font-size:13px; color:#ff0000;border-style:none;border-width:0px;";



    /*---------------------------------------------
    Animated Transitions (IE 5.5 & Up only)
    ---------------------------------------------*/

	/*--this.item_transitions = "filter:progid:DXImageTransform.Microsoft.Fade(duration=2);";--*/


}

