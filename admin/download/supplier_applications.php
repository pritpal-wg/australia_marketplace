<?php

//initial RowCount And Column
$rowCount = 1;
$column = 'A';

$all_data = $db->query('SELECT * FROM `supplier_application`')->fetchAll(PDO::FETCH_CLASS);

$cols = array(
    'S.No.',
    'First Name',
    'Last Name',
    'Job Title',
    'Company',
    'Address',
    'Tel',
    'Fax',
    'Email',
    'Website',
    'Emergency Name',
    'Emergency Relation',
    'Emergency Evening Phone',
    'Emergency Cell Phone',
    'Second Delegate?',
    'Second Delegate Name',
    'Second Delegate Job Title',
    'Second Delegate Company',
    'Second Delegate Address',
    'Second Delegate Telephone',
    'Second Delegate Email',
    'Second Delegate Website',
    'Second Delegate Emergency Name',
    'Second Delegate Emergency Relation',
    'Second Delegate Emergency Evening Phone',
    'Second Delegate Emergency Cell Phone',
    'Sales Point 1',
    'Sales Point 2',
    'Sales Point 3',
    'Sales Point 4',
    'Sales Point 5',
    'ATEC',
    'ATDW',
    'Description',
    'Individuals',
    'Adventure',
    'Incentive / Conventions',
    'Groups',
    'Luxury / Romance',
    'Gay / Lesbian',
    'Youth',
    'Eco Tourism',
    'Other',
    'Retail Road Show',
    'Date & Time',
);
foreach ($cols as $col) {
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, $col);
}
$rowCount++;
foreach ($all_data as $k => $data) {
    $column = 'A';
    $sales_points = unserialize($data->sales_points);
    $product_suitable_for = unserialize($data->product_suitable_for);
    $memberships = '';
    $memberships .= $data->is_atec ? 'ATEC, ' : '';
    $memberships .= $data->is_atdw ? 'ATDW, ' : '';
    if ($memberships) {
        $memberships = rtrim($memberships, ', ');
    }
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($k + 1));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->delegate_fname));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->delegate_lname));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->job_title));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->company_name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->address1 . ' ' . $data->address2));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->tel));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->fax));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->email));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->website));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_rel));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_evening_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_cell_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? 'Yes' : 'No'));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_delegate_fname . ' ' . $data->s_delegate_lname : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_job_title : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_company_name : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_address1 . ' ' . $data->s_address2 : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_tel : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_email : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_website : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_name : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_rel : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_evening_phone : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_cell_phone : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->point1));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->point2));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->point3));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->point4));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->point5));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->is_atec ? 'Yes' : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->is_atdw ? 'Yes' : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->description));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->individuals));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->adventure));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->incentive));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->groups));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->luxury));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->gay));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->youth));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->eco));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->other));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->a_retail_roadshow));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->date_add));
    $rowCount++;
}
// filename for download
$filename = "Supplier Applications(" . date('d-M-Y') . ").xls";

header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=\"$filename\"");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>