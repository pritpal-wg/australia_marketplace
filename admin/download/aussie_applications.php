<?php

//initial RowCount And Column
$rowCount = 1;
$column = 'A';

$all_data = $db->query('SELECT * FROM `aussie_application`')->fetchAll(PDO::FETCH_CLASS);

$cols = array(
    'S.No.',
    'First Name',
    'Last Name',
    'Job Title',
    'Company',
    'Product Contact',
    'Address',
    'Tel',
    'Fax',
    'Email',
    'Website',
    'Inbound Operator',
    'Company Profile',
    'Individuals',
    'Adventure',
    'Incentive / Conventions',
    'Groups',
    'Luxury / Romance',
    'Gay / Lesbian',
    'Youth',
    'Eco Tourism',
    'Other',
    'Room Type',
    'Arrival Date',
    'Departure Date',
    'Sharing With',
    'Subsidy',
    'Emergency Name',
    'Emergency Relation',
    'Emergency Evening Phone',
    'Emergency Cell Phone',
    'Date & Time',
);
foreach ($cols as $col) {
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, $col);
}
$rowCount++;
foreach ($all_data as $k => $data) {
    $column = 'A';
    $categories = unserialize($data->categories);
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($k + 1));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->name2));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->title));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->company));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->product_contact));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->address1 . ' ' . $data->address2));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->tel));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->fax));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->email));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->website));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->inbound_operator));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->company_profile));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->individuals));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->adventure));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->incentive));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->groups));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->luxury));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->gay));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->youth));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->eco));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->other));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->room_type));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->arrival_date));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->depart_date));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->room_type == 'Dbl' ? $data->sharing_with : '-'));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->subsidy));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_rel));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_evening_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergeny_cell_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->date_add));
    $rowCount++;
}
// filename for download
$filename = "Aussie Specialists Applications(" . date('d-M-Y') . ").xls";

header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=\"$filename\"");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>