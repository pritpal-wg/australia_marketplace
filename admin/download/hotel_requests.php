<?php

//initial RowCount And Column
$rowCount = 1;
$column = 'A';

$all_data = $db->query('SELECT * FROM `hotel_request`')->fetchAll(PDO::FETCH_CLASS);

$cols = array(
    'S.No.',
    'First Name',
    'Last Name',
    'Company',
    'Email',
    'Room Type',
    'Arrival Date',
    'Departure Date',
    'Sharing with',
    'Emergency Name',
    'Emergency Relation',
    'Emergency Evening Phone',
    'Emergency Cell Phone',
    'Second Delegate?',
    'Second Delegate Name',
    'Second Delegate Email',
    'Second Delegate Emergency Name',
    'Second Delegate Emergency Relation',
    'Second Delegate Emergency Evening Phone',
    'Second Delegate Emergency Cell Phone',
    'Second Delegate Separate Room?',
    'Second Delegate Room Type',
    'Second Delegate Arrival Date',
    'Second Delegate Departure Date',
    'Second Delegate Sharing with',
    'Date & Time',
);
foreach ($cols as $col) {
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, $col);
}
$rowCount++;
foreach ($all_data as $k => $data) {
    $column = 'A';
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($k + 1));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->delegate_fname));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->delegate_lname));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->company_name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->email));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->room_type));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->arrival_date));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->departure_date));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->room_type == 'Dbl' ? $data->sharing_with : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_name));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_rel));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_evening_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->emergency_cell_phone));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? 'Yes' : 'No'));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_delegate_fname . ' ' . $data->s_delegate_lname : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_email : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_name : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_rel : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_evening_phone : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_delegate ? $data->s_emergency_cell_phone : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_separate_room ? 'Yes' : 'No'));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_separate_room ? $data->s_room_type : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_separate_room ? $data->s_arrival_date : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_separate_room ? $data->s_departure_date : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->s_separate_room ? ($data->s_room_type == 'Dbl' ? $data->s_sharing_with : '') : ''));
    $objPHPExcel->getActiveSheet()->setCellValue(($column++) . $rowCount, ($data->date_add));
    $rowCount++;
}
// filename for download
$filename = "Hotel Requests(" . date('d-M-Y') . ").xls";

header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=\"$filename\"");
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>