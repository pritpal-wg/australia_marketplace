<?php $all_data = $db->query('SELECT * FROM `hotel_request`')->fetchAll(PDO::FETCH_CLASS) ?>
<style>
    #hotel_requests_table thead th {
        vertical-align: middle;
        text-align: center;
    }
</style>
<h2 class="page-header">
    <span class="pull-right">
        <a class="btn btn-primary" href="download.php?page=hotel_requests" style="color: #fff;text-decoration: none;"><i class="fa fa-download"></i> Download in Excel Format</a>
    </span>
    Hotel Requests
</h2>
<div class="table-responsive">
    <table class="table table-bordered table-hover" id="hotel_requests_table">
        <thead>
            <tr class="info">
                <th>S.No.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Company</th>
                <th>Email</th>
                <th>Room Type</th>
                <th>Arrival Date</th>
                <th>Departure Date</th>
                <th>Sharing with</th>
                <th>Emergency Name</th>
                <th>Emergency Relation</th>
                <th>Emergency Evening Phone</th>
                <th>Emergency Cell Phone</th>
                <th>Second Delegate?</th>
                <th>Second Delegate Name</th>
                <th>Second Delegate Email</th>
                <th>Second Delegate Emergency Name</th>
                <th>Second Delegate Emergency Relation</th>
                <th>Second Delegate Emergency Evening Phone</th>
                <th>Second Delegate Emergency Cell Phone</th>
                <th>Second Delegate Separate Room?</th>
                <th>Second Delegate Room Type</th>
                <th>Second Delegate Arrival Date</th>
                <th>Second Delegate Departure Date</th>
                <th>Second Delegate Sharing with</th>
                <th style="min-width: 150px">Date & Time</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($all_data as $k => $data) { ?>
                <tr>
                    <td><?php echo $k + 1 ?></td>
                    <td><?php echo $data->delegate_fname ?></td>
                    <td><?php echo $data->delegate_lname ?></td>
                    <td><?php echo $data->company_name ?></td>
                    <td><?php echo $data->email ?></td>
                    <td><?php echo $data->room_type ?></td>
                    <td><?php echo $data->arrival_date ?></td>
                    <td><?php echo $data->departure_date ?></td>
                    <td><?php echo $data->room_type == 'Dbl' ? $data->sharing_with : '<center><span class="label label-danger">None</span></center>' ?></td>
                    <td><?php echo $data->emergency_name ?></td>
                    <td><?php echo $data->emergency_rel ?></td>
                    <td><?php echo $data->emergency_evening_phone ?></td>
                    <td><?php echo $data->emergency_cell_phone ?></td>
                    <td style="vertical-align: top;text-align: center">
                        <?php echo $data->s_delegate ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' ?>
                    </td>
                    <td><?php echo $data->s_delegate ? $data->s_delegate_fname . ' ' . $data->s_delegate_lname : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_email : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_name : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_rel : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_evening_phone : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_cell_phone : '<center>-</center>' ?></td>
                    <td style="vertical-align: top;text-align: center">
                        <?php echo $data->s_separate_room ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' ?>
                    </td>
                    <td><?php echo $data->s_separate_room ? $data->s_room_type : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_separate_room ? $data->s_arrival_date : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_separate_room ? $data->s_departure_date : '<center>-</center>' ?></td>
                    <td>
                        <?php echo $data->s_separate_room ? ($data->s_room_type == 'Dbl' ? $data->s_sharing_with : '<center><span class="label label-danger">None</span></center>') : '<center>-</center>' ?>
                    </td>
                    <td><?php echo $data->date_add ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>