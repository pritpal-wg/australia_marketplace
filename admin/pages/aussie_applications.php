<?php $all_data = $db->query('SELECT * FROM `aussie_application`')->fetchAll(PDO::FETCH_CLASS) ?>
<style>
    #buyer_applications thead th {
        vertical-align: middle;
        text-align: center;
    }
</style>
<h2 class="page-header">
    <span class="pull-right">
        <a class="btn btn-primary" href="download.php?page=aussie_applications" style="color: #fff;text-decoration: none;"><i class="fa fa-download"></i> Download in Excel Format</a>
    </span>
    Aussie Specialists Applications
</h2>
<div class="table-responsive">
    <table class="table table-bordered table-hover" id="buyer_applications">
        <thead>
            <tr class="info">
                <th>S.No.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Job Title</th>
                <th>Company</th>
                <th>Product Contact</th>
                <th>Address</th>
                <th>Tel</th>
                <th>Fax</th>
                <th>Email</th>
                <th>Website</th>
                <th>Inbound Operator</th>
                <th>Company Profile</th>
                <th>Individuals</th>
                <th>Adventure</th>
                <th>Incentive / Conventions</th>
                <th>Groups</th>
                <th>Luxury / Romance</th>
                <th>Gay / Lesbian</th>
                <th>Youth</th>
                <th>Eco Tourism</th>
                <th>Other</th>
                <th>Room Type</th>
                <th>Arrival Date</th>
                <th>Departure Date</th>
                <th>Sharing With</th>
                <th>Subsidy</th>
                <th>Emergency Name</th>
                <th>Emergency Relation</th>
                <th>Emergency Evening Phone</th>
                <th>Emergency Cell Phone</th>
                <th style="min-width: 150px">Date & Time</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($all_data as $k => $data) { ?>
                <tr>
                    <td><?php echo $k + 1 ?></td>
                    <td><?php echo $data->name ?></td>
                    <td><?php echo $data->name2 ?></td>
                    <td>
                        <?php echo $data->title ?>
                    </td>
                    <td>
                        <?php echo $data->company ?>
                    </td>
                    <td>
                        <?php echo $data->product_contact ?>
                    </td>
                    <td>
                        <?php echo $data->address1 . ' ' . $data->address2 ?>
                    </td>
                    <td>
                        <?php echo $data->tel ?>
                    </td>
                    <td>
                        <?php echo $data->fax ?>
                    </td>
                    <td>
                        <?php echo $data->email ?>
                    </td>
                    <td>
                        <?php echo $data->website ?>
                    </td>
                    <td>
                        <?php echo $data->inbound_operator ?>
                    </td>
                    <td>
                        <?php echo $data->company_profile ?>
                    </td>
                    <td>
                        <?php echo $data->individuals ?>
                    </td>
                    <td>
                        <?php echo $data->adventure ?>
                    </td>
                    <td>
                        <?php echo $data->incentive ?>
                    </td>
                    <td>
                        <?php echo $data->groups ?>
                    </td>
                    <td>
                        <?php echo $data->luxury ?>
                    </td>
                    <td>
                        <?php echo $data->gay ?>
                    </td>
                    <td>
                        <?php echo $data->youth ?>
                    </td>
                    <td>
                        <?php echo $data->eco ?>
                    </td>
                    <td>
                        <?php echo $data->other ?>
                    </td>
                    <td>
                        <?php echo $data->room_type ?>
                    </td>
                    <td>
                        <?php echo $data->arrival_date ?>
                    </td>
                    <td>
                        <?php echo $data->depart_date ?>
                    </td>
                    <td>
                        <?php echo $data->room_type == 'Dbl' ? $data->sharing_with : '<center>-</center>' ?>
                    </td>
                    <td>
                        <?php echo $data->subsidy ?>
                    </td>
                    <td>
                        <?php echo $data->emergency_name ?>
                    </td>
                    <td>
                        <?php echo $data->emergency_rel ?>
                    </td>
                    <td>
                        <?php echo $data->emergency_evening_phone ?>
                    </td>
                    <td>
                        <?php echo $data->emergeny_cell_phone ?>
                    </td>
                    <td>
                        <?php echo $data->date_add ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>