<?php
$all_data = $db->query('SELECT * FROM `supplier_application`')->fetchAll(PDO::FETCH_CLASS);
?>
<style>
    #supplier_applications thead th {
        text-align: center;
        vertical-align: middle;
    }
</style>
<h2 class="page-header">
    <span class="pull-right">
        <a class="btn btn-primary" href="download.php?page=supplier_applications" style="color: #fff;text-decoration: none;"><i class="fa fa-download"></i> Download in Excel Format</a>
    </span>
    Supplier Applications
</h2>
<div class="table-responsive">
    <table class="table table-bordered table-hover" id="supplier_applications">
        <thead>
            <tr class="info">
                <th>S.No.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Job Title</th>
                <th>Company</th>
                <th>Address</th>
                <th>Tel</th>
                <th>Fax</th>
                <th>Email</th>
                <th>Website</th>
                <th>Emergency Name</th>
                <th>Emergency Relation</th>
                <th>Emergency Evening Phone</th>
                <th>Emergency Cell Phone</th>
                <th>Second Delegate?</th>
                <th>Second Delegate Name</th>
                <th>Second Delegate Job Title</th>
                <th>Second Delegate Company</th>
                <th>Second Delegate Address</th>
                <th>Second Delegate Telephone</th>
                <th>Second Delegate Website</th>
                <th>Second Delegate Email</th>
                <th>Second Delegate Emergency Name</th>
                <th>Second Delegate Emergency Relation</th>
                <th>Second Delegate Emergency Evening Phone</th>
                <th>Second Delegate Emergency Cell Phone</th>
                <th>Sales Point 1</th>
                <th>Sales Point 2</th>
                <th>Sales Point 3</th>
                <th>Sales Point 4</th>
                <th>Sales Point 5</th>
                <th>ATEC</th>
                <th>ATDW</th>
                <th>Description</th>
                <th>Individuals</th>
                <th>Adventure</th>
                <th>Incentive / Conventions</th>
                <th>Groups</th>
                <th>Luxury / Romance</th>
                <th>Gay / Lesbian</th>
                <th>Youth</th>
                <th>Eco Tourism</th>
                <th>Other</th>
                <th>Retail Road Show</th>
                <th style="min-width: 150px">Date & Time</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($all_data as $k => $data) { ?>
                <tr>
                    <td><?php echo $k + 1 ?></td>
                    <td><?php echo $data->delegate_fname ?></td>
                    <td><?php echo $data->delegate_lname ?></td>
                    <td><?php echo $data->job_title ?></td>
                    <td><?php echo $data->company_name ?></td>
                    <td><?php echo $data->address1 . ' ' . $data->address2 ?></td>
                    <td><?php echo $data->tel ?></td>
                    <td><?php echo $data->fax ?></td>
                    <td><?php echo $data->email ?></td>
                    <td><?php echo $data->website ?></td>
                    <td><?php echo $data->emergency_name ?></td>
                    <td><?php echo $data->emergency_rel ?></td>
                    <td><?php echo $data->emergency_evening_phone ?></td>
                    <td><?php echo $data->emergency_cell_phone ?></td>
                    <td style="vertical-align: top;text-align: center">
                        <?php echo $data->s_delegate ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' ?>
                    </td>
                    <td><?php echo $data->s_delegate ? $data->s_delegate_fname . ' ' . $data->s_delegate_lname : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_job_title : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_company_name : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_address1 . ' ' . $data->s_address2 : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_tel : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_website : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_email : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_name : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_rel : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_evening_phone : '<center>-</center>' ?></td>
                    <td><?php echo $data->s_delegate ? $data->s_emergency_cell_phone : '<center>-</center>' ?></td>
                    <td><?php echo $data->point1 ?></td>
                    <td><?php echo $data->point2 ?></td>
                    <td><?php echo $data->point3 ?></td>
                    <td><?php echo $data->point4 ?></td>
                    <td><?php echo $data->point5 ?></td>
                    <td><?php echo $data->is_atec ? 'Yes' : '-'; ?></td>
                    <td><?php echo $data->is_atdw ? 'Yes' : '-'; ?></td>
                    <td><?php echo $data->description ?></td>
                    <td><?php echo $data->individuals ?></td>
                    <td><?php echo $data->adventure ?></td>
                    <td><?php echo $data->incentive ?></td>
                    <td><?php echo $data->groups ?></td>
                    <td><?php echo $data->luxury ?></td>
                    <td><?php echo $data->gay ?></td>
                    <td><?php echo $data->youth ?></td>
                    <td><?php echo $data->eco ?></td>
                    <td><?php echo $data->other ?></td>
                    <td><?php echo $data->a_retail_roadshow ?></td>
                    <td><?php echo $data->date_add ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>