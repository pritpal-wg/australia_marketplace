<?php require_once '../Connections/config/config.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"  />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Credit</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../suppliers/css/basic.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="container">
            <div align="center">
                <p>
                    <span class="headline">Australia Marketplace 2016 Results</span><br />
                </p>
            </div>
            <div align="center">
                <p>
                    <span class="headline">Suppliers</span>
                    <span class="text">
                        <br />
                        <a href="index.php?page=supplier_applications">Supplier Applications & Hotel Requests</a>
                        <!--<br />-->
                        <!--<a href="index.php?page=hotel_requests">Hotel Requests</a>-->
                    </span>
                </p>
                <p>
                    <span class="headline">Buyers And Premier Aussie Specialists</span>
                    <span class="text">
                        <br />
                        <a href="index.php?page=buyer_applications">Buyer Applications</a>
                        <br />
                        <a href="index.php?page=aussie_applications">Premier Aussie Specialists Applications</a>
                    </span>
                </p>
            </div>
        </div>
        <?php if (isset($_GET['page']) && $_GET['page']) { ?>
            <hr />
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="overflow: auto !important;">
                        <?php if (file_exists(__DIR__ . '/pages/' . $_GET['page'] . '.php')) { ?>
                            <?php include __DIR__ . '/pages/' . $_GET['page'] . '.php' ?>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                <strong>Error:</strong>
                                File Not Found
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </body>
</html>