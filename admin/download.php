<?php

if (!isset($_GET['page'])) {
    header('Location: ../admin/');
    exit;
}
$page = $_GET['page'];
$allowed_pages = array(
    'supplier_applications',
    'hotel_requests',
    'buyer_applications',
    'aussie_applications',
);
if (!in_array($page, $allowed_pages)) {
    header('Location: ../admin/');
    exit;
}
require_once '../Connections/config/config.php';
require_once '../Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
include __DIR__ . '/download/' . $page . '.php';
?>