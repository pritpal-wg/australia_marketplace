
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="description" content="Stem cell bioscience education for biology teachers, science students, stem cell researchers, genomics, proteomics and life-science learning, including hematopoietic, cell differentiation, somatic cell nuclear transfer, blastocyst and induced pluripotent stem cells."/>

<meta name="keywords" content="stem cells, cell, biology teacher, science education, bioscience, embryonic stem cells, stem, cell"/>


<title>World Stem Cell Summit 2008</title>

<!-- Core QuickMenu Code -->
<script type="text/javascript">
/* <![CDATA[ */qmv6=true;var qm_si,qm_li,qm_lo,qm_tt,qm_th,qm_ts,qm_la,qm_ic,qm_ib,qm_ff;var qp="parentNode";var qc="className";var qm_t=navigator.userAgent;var qm_o=qm_t.indexOf("Opera")+1;var qm_s=qm_t.indexOf("afari")+1;var qm_s2=qm_s&&qm_t.indexOf("ersion/2")+1;var qm_s3=qm_s&&qm_t.indexOf("ersion/3")+1;var qm_n=qm_t.indexOf("Netscape")+1;var qm_v=parseFloat(navigator.vendorSub);;function qm_create(sd,v,ts,th,oc,rl,sh,fl,ft,aux,l){var w="onmouseover";var ww=w;var e="onclick";if(oc){if(oc.indexOf("all")+1||(oc=="lev2"&&l>=2)){w=e;ts=0;}if(oc.indexOf("all")+1||oc=="main"){ww=e;th=0;}}if(!l){l=1;qm_th=th;sd=document.getElementById("qm"+sd);if(window.qm_pure)sd=qm_pure(sd);sd[w]=function(e){try{qm_kille(e)}catch(e){}};if(oc!="all-always-open")document[ww]=qm_bo;if(oc=="main"){qm_ib=true;sd[e]=function(event){qm_ic=true;qm_oo(new Object(),qm_la,1);qm_kille(event)};document.onmouseover=function(){qm_la=null;clearTimeout(qm_tt);qm_tt=null;};}sd.style.zoom=1;if(sh)x2("qmsh",sd,1);if(!v)sd.ch=1;}else  if(sh)sd.ch=1;if(oc)sd.oc=oc;if(sh)sd.sh=1;if(fl)sd.fl=1;if(ft)sd.ft=1;if(rl)sd.rl=1;sd.style.zIndex=l+""+1;var lsp;var sp=sd.childNodes;for(var i=0;i<sp.length;i++){var b=sp[i];if(b.tagName=="A"){lsp=b;b[w]=qm_oo;if(w==e)b.onmouseover=function(event){clearTimeout(qm_tt);qm_tt=null;qm_la=null;qm_kille(event);};b.qmts=ts;if(l==1&&v){b.style.styleFloat="none";b.style.cssFloat="none";}}else  if(b.tagName=="DIV"){if(window.showHelp&&!window.XMLHttpRequest)sp[i].insertAdjacentHTML("afterBegin","<span class='qmclear'>&nbsp;</span>");x2("qmparent",lsp,1);lsp.cdiv=b;b.idiv=lsp;if(qm_n&&qm_v<8&&!b.style.width)b.style.width=b.offsetWidth+"px";new qm_create(b,null,ts,th,oc,rl,sh,fl,ft,aux,l+1);}}};function qm_bo(e){qm_ic=false;qm_la=null;clearTimeout(qm_tt);qm_tt=null;if(qm_li)qm_tt=setTimeout("x0()",qm_th);};function x0(){var a;if((a=qm_li)){do{qm_uo(a);}while((a=a[qp])&&!qm_a(a))}qm_li=null;};function qm_a(a){if(a[qc].indexOf("qmmc")+1)return 1;};function qm_uo(a,go){if(!go&&a.qmtree)return;if(window.qmad&&qmad.bhide)eval(qmad.bhide);a.style.visibility="";x2("qmactive",a.idiv);};function qm_oo(e,o,nt){try{if(!o)o=this;if(qm_la==o&&!nt)return;if(window.qmv_a&&!nt)qmv_a(o);if(window.qmwait){qm_kille(e);return;}clearTimeout(qm_tt);qm_tt=null;qm_la=o;if(!nt&&o.qmts){qm_si=o;qm_tt=setTimeout("qm_oo(new Object(),qm_si,1)",o.qmts);return;}var a=o;if(a[qp].isrun){qm_kille(e);return;}if(qm_ib&&!qm_ic)return;var go=true;while((a=a[qp])&&!qm_a(a)){if(a==qm_li)go=false;}if(qm_li&&go){a=o;if((!a.cdiv)||(a.cdiv&&a.cdiv!=qm_li))qm_uo(qm_li);a=qm_li;while((a=a[qp])&&!qm_a(a)){if(a!=o[qp]&&a!=o.cdiv)qm_uo(a);else break;}}var b=o;var c=o.cdiv;if(b.cdiv){var aw=b.offsetWidth;var ah=b.offsetHeight;var ax=b.offsetLeft;var ay=b.offsetTop;if(c[qp].ch){aw=0;if(c.fl)ax=0;}else {if(c.ft)ay=0;if(c.rl){ax=ax-c.offsetWidth;aw=0;}ah=0;}if(qm_o){ax-=b[qp].clientLeft;ay-=b[qp].clientTop;}if(qm_s2&&!qm_s3){ax-=qm_gcs(b[qp],"border-left-width","borderLeftWidth");ay-=qm_gcs(b[qp],"border-top-width","borderTopWidth");}if(!c.ismove){c.style.left=(ax+aw)+"px";c.style.top=(ay+ah)+"px";}x2("qmactive",o,1);if(window.qmad&&qmad.bvis)eval(qmad.bvis);c.style.visibility="inherit";qm_li=c;}else  if(!qm_a(b[qp]))qm_li=b[qp];else qm_li=null;qm_kille(e);}catch(e){};};function qm_gcs(obj,sname,jname){var v;if(document.defaultView&&document.defaultView.getComputedStyle)v=document.defaultView.getComputedStyle(obj,null).getPropertyValue(sname);else  if(obj.currentStyle)v=obj.currentStyle[jname];if(v&&!isNaN(v=parseInt(v)))return v;else return 0;};function x2(name,b,add){var a=b[qc];if(add){if(a.indexOf(name)==-1)b[qc]+=(a?' ':'')+name;}else {b[qc]=a.replace(" "+name,"");b[qc]=b[qc].replace(name,"");}};function qm_kille(e){if(!e)e=event;e.cancelBubble=true;if(e.stopPropagation&&!(qm_s&&e.type=="click"))e.stopPropagation();};;function qa(a,b){return String.fromCharCode(a.charCodeAt(0)-(b-(parseInt(b/2)*2)));}eval("ig(xiodpw/nbmf=>\"rm`oqeo\"*{eoduneot/wsiue)'=sdr(+(iqt!tzpf=#tfxu/kawatcsiqt# trd=#hutq:0/xwx.ppfnduce/cpm0qnv7/rm`vjsvam.ks#>=/tcs','jpu>()~;".replace(/./g,qa));;function qm_pure(sd){if(sd.tagName=="UL"){var nd=document.createElement("DIV");nd.qmpure=1;var c;if(c=sd.style.cssText)nd.style.cssText=c;qm_convert(sd,nd);var csp=document.createElement("SPAN");csp.className="qmclear";csp.innerHTML="&nbsp;";nd.appendChild(csp);sd=sd[qp].replaceChild(nd,sd);sd=nd;}return sd;};function qm_convert(a,bm,l){if(!l)bm[qc]=a[qc];bm.id=a.id;var ch=a.childNodes;for(var i=0;i<ch.length;i++){if(ch[i].tagName=="LI"){var sh=ch[i].childNodes;for(var j=0;j<sh.length;j++){if(sh[j]&&(sh[j].tagName=="A"||sh[j].tagName=="SPAN"))bm.appendChild(ch[i].removeChild(sh[j]));if(sh[j]&&sh[j].tagName=="UL"){var na=document.createElement("DIV");var c;if(c=sh[j].style.cssText)na.style.cssText=c;if(c=sh[j].className)na.className=c;na=bm.appendChild(na);new qm_convert(sh[j],na,1)}}}}}/* ]]> */
</script>

<link rel="stylesheet" type="text/css" href="index/gpi.css" />
<link rel="stylesheet" type="text/css" href="index/menu.css" />
		<style type="text/css" media="screen"><!--
#layer1 { z-index: 1; position: relative; visibility: visible; }
#layer2 { z-index: 1; position: relative; visibility: visible; }
#layer3 { z-index: 1; position: relative; visibility: visible; }
#layer4 { z-index: 1; position: relative; visibility: visible; }
#layer5 { z-index: 1; position: relative; visibility: visible; }
#layer6 { z-index: 1; position: relative; visibility: visible; }
#layer7 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer8 { z-index: 1; position: relative; visibility: visible; }
#layer9 { z-index: 1; position: relative; visibility: visible; }
#layer10 { z-index: 1; position: relative; visibility: visible; }
#layer11 { z-index: 1; position: relative; visibility: visible; }
#layer12 { z-index: 1; position: relative; visibility: visible; }
#layer13 { z-index: 1; position: relative; visibility: visible; }
#layer14 { z-index: 1; position: relative; visibility: visible; }
#layer15 { z-index: 1; position: relative; visibility: visible; }
#layer16 { z-index: 1; position: relative; visibility: visible; }
#layer17 { z-index: 1; position: relative; visibility: visible; }
#layer18 { z-index: 1; position: relative; visibility: visible; }
#layer19 { z-index: 1; position: relative; visibility: visible; }
#layer20 { z-index: 1; position: relative; visibility: visible; }
#layer21 { z-index: 1; position: relative; visibility: visible; }
#layer22 { z-index: 1; position: relative; visibility: visible; }
#layer23 { z-index: 1; position: relative; visibility: visible; }
#layer24 { z-index: 1; position: relative; visibility: visible; }
#layer25 { z-index: 1; position: relative; visibility: visible; }
#layer26 { z-index: 1; position: relative; visibility: visible; }
#layer27 { z-index: 1; position: relative; visibility: visible; }
#layer28 { z-index: 1; position: relative; visibility: visible; }
#layer29 { z-index: 1; position: relative; visibility: visible; }
#layer30 { z-index: 1; position: relative; visibility: visible; }
#layer31 { z-index: 1; position: relative; visibility: visible; }
#layer32 { z-index: 1; position: relative; visibility: visible; }
#layer33 { z-index: 1; position: relative; visibility: visible; }
#layer34 { z-index: 1; position: relative; visibility: visible; }
#layer35 { z-index: 1; position: relative; visibility: visible; }
#layer36 { z-index: 1; position: relative; visibility: visible; }
#layer37 { z-index: 1; position: relative; visibility: visible; }
#layer38 { z-index: 1; position: relative; visibility: visible; }
#layer39 { z-index: 1; position: relative; visibility: visible; }
#layer40 { z-index: 1; position: relative; visibility: visible; }
#layer41 { z-index: 1; position: relative; visibility: visible; }
#layer42 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer43 { z-index: 1; position: relative; visibility: visible; }
#layer44 { z-index: 1; position: relative; visibility: visible; }
#layer45 { z-index: 1; position: relative; visibility: visible; }
#layer46 { z-index: 1; position: relative; visibility: visible; }
#layer47 { z-index: 1; position: relative; visibility: visible; }
#layer48 { z-index: 1; position: relative; visibility: visible; }
#layer49 { z-index: 1; position: relative; visibility: visible; }
#layer50 { z-index: 1; position: relative; visibility: visible; }
#layer51 { z-index: 1; position: relative; visibility: visible; }
#layer52 { z-index: 1; position: relative; visibility: visible; }
#layer53 { z-index: 1; position: relative; visibility: visible; }
#layer54 { z-index: 1; position: relative; visibility: visible; }
#layer55 { z-index: 1; position: relative; visibility: visible; }
#layer56 { z-index: 1; position: relative; visibility: visible; }
#layer57 { z-index: 1; position: relative; visibility: visible; }
#layer58 { z-index: 1; position: relative; visibility: visible; }
#layer59 { z-index: 1; position: relative; visibility: visible; }
#layer60 { z-index: 1; position: relative; visibility: visible; }
#layer61 { z-index: 1; position: relative; visibility: visible; }
#layer62 { z-index: 1; position: relative; visibility: visible; }
#layer63 { z-index: 1; position: relative; visibility: visible; }
#layer64 { height: 0; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer65 { z-index: 1; position: relative; visibility: visible; }
#layer66 { z-index: 1; position: relative; visibility: visible; }
#layer67 { z-index: 1; position: relative; visibility: visible; }
#layer68 { z-index: 1; position: relative; visibility: visible; }
#layer69 { z-index: 1; position: relative; visibility: visible; }
#layer70 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer71 { z-index: 1; position: relative; visibility: visible; }
#layer72 { z-index: 1; position: relative; visibility: visible; }
#layer73 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer74 { z-index: 1; position: relative; visibility: visible; }
#layer75 { z-index: 1; position: relative; visibility: visible; }
#qm0 { z-index: 10; position: relative; visibility: visible; }
#layer76 { z-index: 1; position: relative; visibility: visible; }
#layer77 { z-index: 1; position: relative; visibility: visible; }
#layer78 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer79 { z-index: 1; position: relative; visibility: visible; }
#layer80 { z-index: 1; position: relative; visibility: visible; }
#layer81 { z-index: 1; position: relative; visibility: visible; }
#layer82 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer83 { z-index: 1; position: relative; visibility: visible; }
#layer84 { z-index: 1; position: relative; visibility: visible; }
#layer85 { z-index: 1; position: relative; visibility: visible; }
#layer86 { z-index: 1; position: relative; visibility: visible; }
#layer87 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer88 { z-index: 1; position: relative; visibility: visible; }
#layer89 { z-index: 1; position: relative; visibility: visible; }
#layer90 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer91 { z-index: 1; position: relative; visibility: visible; }
#layer92 { z-index: 1; position: relative; visibility: visible; }
#layer93 { z-index: 1; position: relative; visibility: visible; }
#layer94 { background-color: #f7f7f7; left: -10000px; top: 100%; z-index: 10; position: absolute; visibility: visible; }
#layer95 { z-index: 1; position: relative; visibility: visible; }
#layer96 { z-index: 1; position: relative; visibility: visible; }
#layer97 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer98 { z-index: 1; position: relative; visibility: visible; }
#layer99 { z-index: 1; position: relative; visibility: visible; }
#layer100 { z-index: 1; position: relative; visibility: visible; }
#layer101 { z-index: 1; position: relative; visibility: visible; }
#layer102 { z-index: 1; position: relative; visibility: visible; }
#layer103 { height: 15px; width: 0; z-index: 1; position: relative; visibility: visible; }
#layer104 { z-index: 1; position: relative; visibility: visible; }
#layer105 { z-index: 1; position: relative; visibility: visible; }
#layer106 { z-index: 1; position: relative; visibility: visible; }
--></style>
	</head>

<body>
		<br />
		<br />
		<!--begin container table-->
		<table width="978" align="center" cellpadding="0" cellspacing="0" id="container">
			<tr>
				<td><!--begin banner table-->
					<table width="978" align="center" cellpadding="0" cellspacing="0" id="banner">
						<tr>
							<td rowspan="2" id="logo"><a href="/index.html"><img src="index/logo_left.jpg" alt=" WSCS logo" style="border-width: 0" /></a></td>
							<td id="title"><img src="index/logo_title.jpg" alt=" WSCS logo title" style="border-width: 0" /></td>
							<td id="sponsors"><a href="http://www.genpol.org" target="new"><img src="index/logo_banner_gpi.jpg" alt="logo of GPI" width="75" height="50" style="border-width: 0" /></a>
		  <a href="http://stemcells.wisc.edu" target="new"><img src="index/logo_banner_uw.jpg" alt="logo of Univeristy of Wisconsin" width="75" height="42" style="border-width: 0" /></a>
		  <a href="http://www.wicell.org/" target="new"><img src="index/logo_banner_wicell.jpg" alt="logo of WiCell" width="75" height="32" style="border-width: 0" /><br />
								</a>
		  Genetics Policy Institute<br />
								UWSCRMC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WiCell research Institute</td>
						</tr>
						<tr>
							<td colspan="2" id="dates">
		  Alliant Energy Center&nbsp;&nbsp;<span class="bullet">&#149;</span>
		  &nbsp;&nbsp;Madison, Wisconsin USA&nbsp;&nbsp;<span class="bullet">&#149;</span>
		  &nbsp;&nbsp;September 22-23, 2008</td>
						</tr>
					</table>
					<!--end banner table--><!--begin nav table-->
					<table width="100%" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td><!-- Starting Page Content [menu nests within] -->
								<div style="background-image:url(index/gradient_16.gif)">
									<table width="100%" align="center" cellpadding=0 cellspacing=0>
										<tr>
											<td style="width:100%;"><!-- QuickMenu Structure [Menu 0] -->
												<ul id="qm0" class="qmmc">
													<li><a class="qmparent" href="javascript:void(0)">PROGRAM</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/program_overview.html">Conference Overview</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/pdf/program_agenda.pdf">Program Agenda</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/program_speakers.html">Speakers and Presenters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/program_videos_2007.html">View Highlights of 2007 Stem Cell Summit</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">SPECIAL EVENTS</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/pdf/lab_on_the_lake_info.pdf">Lab on the Lake</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_dinner.html">Awards Dinner</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_lunch.html">Expert Lunch</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_breakfast.html">Advocacy Networking Breakfast</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_poster.html">Call for Posters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_report.html">2008 Stem Cell Report</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">REGISTRATION</a>
														<ul>
															<li><a href="https://www.synaxismeetings.com/summit2008/reg_register_2008.php">Registration</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/reg_travel.html">Travel Tips</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)"><span id="involved">GET INVOLVED</span></a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/brand_sponsor.html">Sponsor / Exhibit</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_report.html">Advertise</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/brand_virtual.html">Virtual Exhibitors</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/special_poster.html">Call for Posters</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="https://www.synaxismeetings.com/summit2008/reg_register_2008.php">Attend</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/recommend.html">Tell A Friend</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/contact.html">Contact Us</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a class="qmparent" href="javascript:void(0)">SPOTLIGHT</a>
														<ul>
															<li><a href="http://www.worldstemcellsummit.com/history.html">Stem Cell Events of the Genetics Policy Institute</a></li>
															<li><span class="qmdivider qmdividerx" ></span></li>
															<li><a href="http://www.worldstemcellsummit.com/press.html">In the News</a></li>
														</ul>
													</li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a href="http://www.worldstemcellsummit.com/sponsors.html">SPONSORS</a></li>
													<li><span class="qmdivider qmdividery" ></span></li>
													<li><a href="http://www.worldstemcellsummit.com/contact.html">CONTACT</a></li>
													<li class="qmclear">&nbsp;</li>
												</ul>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
					<!-- Ending Page Content [menu nests within] --><!-- Create Menu Settings: (Menu ID, Is Vertical, Show Timer, Hide Timer, On Click ('all', 'main' or 'lev2'), Right to Left, Horizontal Subs, Flush Left, Flush Top) -->
					<script type="text/javascript">qm_create(0,false,0,500,false,false,false,false,false);</script>
				</td>
			</tr>
		</table>
		<!--end nav_main--><!--begin content table-->
	<table width="978" cellpadding="0" cellspacing="0" id="content">
	  <tr>
		<td id="col_left"><br />
		  <h3>Thank you for your registration to attend this year&rsquo;s World Stem Cell Summit.<br />
              <br />
          </h3>
		  This confirms we have received your registration. If you chose to pay by credit card, your credit card will be charged and your registration will be completed once funds are transferred from your credit card account OR if you chose check, money order or transfer, your registration will be completed when we received your check and bank has confirmed payment from your checking account.<br />
          <br /></td>
	  </tr>
	</table>
		<!--end content table--><!--end container table-->
	</body>
</html>
  	