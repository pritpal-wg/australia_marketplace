

/**********************************************************************************************

                               Content Swap Items and Content

**********************************************************************************************/

/*  

        Note:  Each <li... tag below creates a new swap item, you may add as many as you wish.  Terminate each line with 
               a backslash '\'.

     Warning:  Double check that there are no extra white spaces after your terminating '\' backslashes, the swap will fail to
               load if there are extra spaces present.

*/


document.write("\
\
<ul id='cswap0' style='position:relative;display:none;z-index:0;'>\
\
	<li><img src='spon/logo_promega.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_toucan.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_forward.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_quarles.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_darby.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_bio.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_fa.gif' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_sscn.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_scr.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_diabetes.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_fish.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_nyscf.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_neostem.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_epibio.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cdi.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_nano' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_angio.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_meso.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_hadassah.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_scot.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_maryland.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_jackson.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_pfizer.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_cirm.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_ht.jpg' style= 'padding: 10px 0 0 0'></li>\
\
	<li><img src='spon/logo_blind.jpg' style= 'padding: 10px 0 0 0'></li>\
</ul>\
\
</div>");





/**********************************************************************************************

                               Customizable Options and Styles

**********************************************************************************************/

node7 = true

function cswapdata0()
{


    /*---------------------------------------------
    Content Dimensions
    ---------------------------------------------*/

	this.container_width = 205
	this.container_height = 300



    /*---------------------------------------------
    Message Timing
    ---------------------------------------------*/

	this.initial_swap_delay = 1			//measured in seconds
	this.swap_delay = 3				//measured in seconds

	

    /*---------------------------------------------
    Container Styles and Padding
    ---------------------------------------------*/

	this.container_padding = "0,0,0,0"
	this.container_styles = "";



    /*---------------------------------------------
    Container Styles and Padding
    ---------------------------------------------*/
	
	this.item_styles = "color:#000000; text-decoration:none; font-family:Arial; font-size:13px; border-style:none;border-width:0px;";
	this.item_link_styles = "color:#0033cc; text-decoration:none; font-family:Arial; font-size:13px; border-style:none;border-width:0px;";
	this.item_link_hover_styles = "color:#0033cc; text-decoration:underline; font-family:Arial; font-size:13px; color:#ff0000;border-style:none;border-width:0px;";



    /*---------------------------------------------
    Animated Transitions (IE 5.5 & Up only)
    ---------------------------------------------*/

	/*--this.item_transitions = "filter:progid:DXImageTransform.Microsoft.Fade(duration=2);";--*/


}

