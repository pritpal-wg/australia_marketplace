<?php
if (!isset($_POST) && empty($_POST)) {
    header('Location: payment_supplier.php');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>NPW Prospectus 2012</title>
        <link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />

        <script type="text/javascript" language="javascript" src="async.js"></script>

    </head>

    <body bgcolor="#69433a" topmargin="0" marginheight="0">
        <div align="center">
            <table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
                <tr>
                    <td width="25"><img src="../images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
                    <td width="400"></td>
                    <td width="200"></td>
                    <td width="0"><img src="../images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
                </tr>
                <tr>
                    <td rowspan="3" width="25" background="../images/shadow_left.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td colspan="2" width="600"><img src="../images/n&a_header.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
                    <td rowspan="3" width="0" background="../images/shadow_right.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td colspan="2" width="600">
                        <menumachine name="npw" id="m3fs0c71">
                            <csobj t="Component" csref="../menumachine/npw/menuspecs.menudata"><noscript>
                                    <p><a class="mm_no_js_link" href="../menumachine/npw/navigation.html">Site Navigation</a></p>
                                </noscript> </csobj>
                            <script type="text/javascript"><!--
                                var mmfolder=/*URL*/"../menumachine/",zidx=1000;
                                //--></script>
                            <script type="text/javascript" src="../menumachine/menumachine2.js"></script>
                            <script type="text/javascript" src="../menumachine/npw/menuspecs.js"></script>
                            <script type="text/javascript"><!--//settings
                                pkg.hl("m3fs0c70",1);
                                //settings--></script>
                        </menumachine>
                    </td>
                </tr>
                <tr height="570">
                    <td colspan="2" valign="top" width="600" height="570">
                        <table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
                                        <tr>
                                            <td valign="top" width="1"><img src="../images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                            <td valign="top" class="headline"><span class="dsR7">Supplier Payment Preview</span></td>
                                            <td valign="top" class="headline2" width="2"><img src="../images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">
                                                <form method="post" name="form1" id="form1" action="authorize_net.php">
                                                    <table width="752" align="left" cellpadding="4">
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top" nowrap="nowrap">
                                                                <div align="left" class="style1">
                                                                    <br />
                                                                    <span class="textboldred"> Primary Contact Information:</span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Attendee First Name:</strong></div>
                                                            </td>
                                                            <td class="text" width="470" valign="top"><?php echo $_POST['Name'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Attendee Last Name:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><?php echo $_POST['LastName'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Contact Email:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"><?php echo $_POST['Email'] ?></td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td colspan="2" align="left" valign="top" nowrap="nowrap" class="text"><br />
                                                                <span class="textboldred">Registration Selection:</span></td>
                                                        </tr>

                                                        <?php if ($_POST['Share'] == '' && $_POST['Separate'] == '' && $_POST['CodeAmount'] == '') { ?>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Primary Delegate only:</strong></div>
                                                                    <input type="hidden" name="package" value="Primary Delegate only" />
                                                                </td>
                                                                <td class="text" valign="top">$<?php echo $_POST['Amount']; ?></td>
                                                            </tr>
                                                        <?php } else if ($_POST['Share'] != '' && $_POST['Separate'] == '' && $_POST['CodeAmount'] == '') { ?>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Primary delegate and additional delegate (share room):</strong></div>
                                                                    <input type="hidden" name="package" value="Primary delegate and additional delegate (share room)" />
                                                                </td>
                                                                <td class="text" valign="top">$<?php echo $_POST['Amount']; ?></td>
                                                            </tr>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Additional delegate name:</strong></div>
                                                                    <input type="hidden" name="delegate" value="<?php echo $_POST['Share']; ?>" />
                                                                </td>
                                                                <td class="text" valign="top"><?php echo $_POST['Share']; ?></td>
                                                            </tr>
                                                        <?php } else if ($_POST['Share'] == '' && $_POST['Separate'] != '' && $_POST['CodeAmount'] == '') { ?>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Primary delegate and additional delegate (separate room):<br /></strong></div>
                                                                    <input type="hidden" name="package" value="Primary delegate and additional delegate (separate room)" />
                                                                </td>
                                                                <td class="text" valign="top">$<?php echo $_POST['Amount']; ?></td>
                                                            </tr>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Additional delegate name:</strong></div>
                                                                    <input type="hidden" name="delegate" value="<?php echo $_POST['Separate']; ?>" />
                                                                </td>
                                                                <td class="text" valign="top"><?php echo $_POST['Separate']; ?></td>
                                                            </tr>
                                                        <?php } else if ($_POST['Share'] == '' && $_POST['Separate'] == '' && $_POST['CodeAmount'] != '') { ?>
                                                            <tr valign="baseline">
                                                                <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                    <div align="right">
                                                                        <strong>Preapproved Special Registration Fee <br />for Primary Delegate:<br />
                                                                        (NOTE: $50 processing fee will be added to the amount.)
                                                                        </strong></div>
                                                                    <input type="hidden" name="package" value="Preapproved Special Registration Fee for Primary Delegate" />
                                                                </td>
                                                                <td class="text" valign="top">$<?php echo $_POST['CodeAmount']+50; ?></td>
                                                                <input type="hidden" name="CodeAmount" value="<?php echo $_POST['CodeAmount']+50; ?>" />
                                                            </tr>
                                                        <?php } ?>
                                                        <tr valign="baseline">
                                                            <td class="text" colspan="2" align="left" valign="top"><span class="textboldred"><br />
                                                                </span>
                                                                <div align="left">
                                                                    <span class="textboldred"><span class="style1"><strong>Total and Payment Information:</strong></span></span></div>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>TOTAL AMOUNT:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                $<?php
                                                                if ($_POST['Share'] == '' && $_POST['Separate'] == '' && $_POST['CodeAmount'] != '')
                                                                {
                                                                    echo $_POST['Amount']+50;
                                                                }
                                                                else
                                                                {
                                                                    echo $_POST['Amount'];
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Accept total amount to charge:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top"> <?php echo $_POST['AcceptAmount']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Payment Type:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['Card']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Card Holder's Name<br />
								(as shown on credit card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCName']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Credit Card Number:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCNumber']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Month:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCMonth']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>Expiration Year:</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CCYear']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">
                                                                <div align="right">
                                                                    <strong>CVC <br />
							(Security code on back of card):</strong></div>
                                                            </td>
                                                            <td class="text" valign="top">
                                                                <?php echo $_POST['CVC']; ?>
                                                            </td>
                                                        </tr>
                                                        <tr valign="baseline">
                                                            <td class="text" align="right" valign="top" nowrap="nowrap">&nbsp;</td>
                                                            <td class="text" valign="top"><div align="left">

                                                                    <input type="hidden" name="Name" value="<?php echo $_POST['Name']; ?>" />
                                                                    <input type="hidden" name="LastName" value="<?php echo $_POST['LastName']; ?>" />
                                                                    <input type="hidden" name="Email" value="<?php echo $_POST['Email'];?>" />
                                                                    <input type="hidden" name="RegistrationFee" value="<?php echo $_POST['RegistrationFee'];?>" />
                                                                    <input type="hidden" name="Share" value="<?php echo $_POST['Share'];?>" />
                                                                    <input type="hidden" name="Separate" value="<?php echo $_POST['Separate'];?>" />
                                                                    <input type="hidden" name="CodeAmount" value="<?php echo $_POST['CodeAmount'];?>" />
                                                                    <?php if ($_POST['Share'] == '' && $_POST['Separate'] == '' && $_POST['CodeAmount'] != '') {?>
                                                                        <input type="hidden" name="Amount" value="<?php echo $_POST['Amount']+50;?>" />
                                                                    <?php } else { ?>
                                                                        <input type="hidden" name="Amount" value="<?php echo $_POST['Amount'];?>" />
                                                                    <?php } ?>
                                                                    <input type="hidden" name="AcceptAmount" value="<?php echo $_POST['AcceptAmount'];?>" />
                                                                    <input type="hidden" name="Card" value="<?php echo $_POST['Card'];?>" />
                                                                    <input type="hidden" name="CCName" value="<?php echo $_POST['CCName'];?>" />
                                                                    <input type="hidden" name="CCNumber" value="<?php echo $_POST['CCNumber'];?>" />
                                                                    <input type="hidden" name="CCMonth" value="<?php echo $_POST['CCMonth'];?>" />
                                                                    <input type="hidden" name="CCYear" value="<?php echo $_POST['CCYear'];?>" />
                                                                    <input type="hidden" name="CVC" value="<?php echo $_POST['CVC'];?>" />
                                                                    <input name="redirect" type="hidden" id="supplier_thankyou.html" value="supplier_thankyou.html" />

                                                                    <br />
                                                                    <input type="button" name="Back" onclick="javascript:history.back();" Value="Back" />
                                                                    <input type="submit" value="Make Payment" name="makepayment"/><br />
                                                                    <br /><div id="thawteseal" style="text-align:center;" title="Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications.">
                                                                        <div><script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=www.synaxismeetings.com&amp;size=M&amp;lang=en"></script></div>
                                                                        <div><a href="http://www.thawte.com/ssl-certificates/" target="_blank" style="color:#000000; text-decoration:none; font:bold 10px arial,sans-serif; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></div>
                                                                    </div>

                                                                </div></td>
                                                        </tr>
                                                    </table>
                                                    <div align="left"></div>
                                                    <div align="left" onfocus="FDK_AddRadioValidation('form1','document.form1.PaymentType',true,'\'This is a required field,please make a Payment Type selection.\'')">
                                                        <input type="hidden" name="MM_insert" value="form1" /></div>
                                                </form>
                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td valign="top" class="text">

                                            </td>
                                            <td valign="top" class="headline2" width="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="1"></td>
                                            <td class="text" valign="top"><br />
                                            </td>
                                            <td valign="top" class="text" width="2"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="25"><img src="../images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                    <td colspan="2" width="600" background="../images/center_bottom_shadow.jpg"></td>
                    <td width="0"><img src="../images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
                </tr>
                <tr height="25">
                    <td width="25" height="25" background="../images/top_shadow_left.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                    <td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                                        <table width="800" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bottom_text">
                                                    <div align="center">
														Copyright 2012. NAW Australian States and Territories. All Rights Reserved.</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div></td>
                            </tr>
                        </table></td>
                    <td width="0" height="25" background="../images/top_shadow_right.jpg"><img src="../images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
                </tr>
                <tr>
                    <td width="25"><img src="../images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                    <td colspan="2" width="600" background="../images/bottom_shadow_blue.jpg"></td>
                    <td width="0"><img src="../images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
                </tr>
            </table>
        </div>
        <p></p>
    </body>

</html>