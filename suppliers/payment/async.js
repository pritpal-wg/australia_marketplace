// JavaScript Document
function GetXmlHttpObject() {
    var xmlHttp = null;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        // Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}

function get_code_amount(form) {
    var code = form.clientID.value;
    document.getElementById('result').innerHTML = "";
    if (code == "undefined" || code == "" || code == null) {
        form.CodeAmount.value = "";
        form.clientID.focus();
        return;
    } else if (isNaN(code) || parseInt(code) <= 0) {
        form.clientID.value = "";
        form.CodeAmount.value = "";
        form.clientID.focus();
        return
    } else {
        xmlHttp = GetXmlHttpObject();
        if (xmlHttp == null) {
            alert("Your browser does not support AJAX!");
            return;
        }
        var url = "get_amount.php?code=" + code;
        url = url + "&sid=" + Math.random();
        form.loading.style.display = "block";
        xmlHttp.onreadystatechange = function stateResult() {
            if (xmlHttp.readyState == 4) {
                var result = xmlHttp.responseText;
                form.loading.style.display = "none";
                if (isNaN(result)) {
                    document.getElementById('result').innerHTML = result;
                    form.CodeAmount.value = "";
                    form.Amount.value = "0";
                } else {
                    form.CodeAmount.value = result;
                    form.Amount.value = result;
                    document.getElementById('result').innerHTML = "";
                }
                calcsum(form, 150);
            }
        }
        xmlHttp.open("GET", url, true);
        xmlHttp.send(null);
    }
}
function calcsum(form, perticketamt) {
    var guestickettotal = 0;
    var regfee = form.Amount.value;
    var grosstotal = 0;
//    var tickets = form.Additional2.value;
//    if (!isNaN(tickets) && tickets > 0) {
//        guestickettotal = parseInt(tickets) * parseInt(perticketamt);
//    }
    if (!isNaN(regfee) && parseInt(regfee) > 0) {
        grosstotal = guestickettotal + parseInt(regfee);
    } else {
        grosstotal = guestickettotal;
    }
//    form.total.value = guestickettotal;
//    form.Grosstotal.value = grosstotal;
}
function update_regamt(form, val) {
    form.Amount.value = val;
//    form.loading.style.display = "none";
//    form.clientID.value = "";
//    form.CodeAmount.value = "";
//    document.getElementById('result').innerHTML = "";
}
function check_amt(form) {
    var amt = form.Amount.value;
    if (amt != "undefined" || amt != "") {
        form.Amount.value = 0;
    }
//    form.clientID.focus();
}
function checknum(form, val, maxval) {
    if (isNaN(val) || val <= 0) {
        form.Additional2.value = "";
    } else if (parseInt(val) > maxval) {
        form.Additional2.value = "";
    }
}
