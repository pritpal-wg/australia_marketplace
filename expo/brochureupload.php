<?php error_reporting(0);

 foreach($_POST as $key => $value){
    	if (is_array($value)) {
    		$_values[$key] = join("%,% ",$value);
    	}else $_values[$key] = $value;
      $_values[$key]=stripslashes($_values[$key]);
      }

 if (!isset($_POST["_referer"])) {
 	@$_referer = $_SERVER["HTTP_REFERER"];
 }else $_referer = $_POST["_referer"];


 $_ErrorList = array();

 function mark_if_error($_field_name, $_old_style = ""){
 global $_ErrorList;
 	$flag=false;
 	foreach($_ErrorList as $_error_item_name){
 		if ($_error_item_name==$_field_name) {
 			$flag=true;
 		}
	}
	echo $flag ? "style=\"background-color: #FFCCBA; border: solid 1px #D63301;\"" : $_old_style;
 }

 function IsThereErrors($form, $_isdisplay) {
  global $_POST, $_FILES, $_values, $_ErrorList;
  $flag = false;
  if ($form > -1) {
	  if ($_isdisplay) {
	  	echo "<div style=\"border: 1px solid; margin: 10px auto; padding:15px 10px 15px 50px; background-repeat: no-repeat; background-position: 10px center; color: #D63301; background-color: #FFCCBA; max-width:600px; background-image:url('".$_SERVER["PHP_SELF"]."?image=warning');\">";
	  }

  $flag = false;

	  $req[0][] =  array("Name", "Name is required.");
	  $req[0][] =  array("Company", "Company is required.");
	  $req[0][] =  array("email", "Email is required.");
	  foreach($req[$form] as $field){
	    if (!isset($_values[$field[0]]) or ($_values[$field[0]]=="")) {
	      $flag = true;
	      if ($_isdisplay) {
	      	echo $field[1]."<br>";
	      	$_ErrorList[] = $field[0];
	      }
	    }
	  }

	  $files_req[0][] =  array("fileGetterName", "Your PDF brochure is required.");
	  foreach($files_req[$form] as $field){
	    if (@$_FILES[$field[0]]["name"]=="") {
	      $flag = true;
	      if ($_isdisplay) {
	      	echo $field[1]."<br>";
	      	$_ErrorList[] = $field[0];
	      }
	    }
	  }

 	  $files[0][] = array("fileGetterName", "\$_FILES['fileGetterName']['size']<3151872", "", "", "\$_FILES['fileGetterName']['size']<3151872", "File size exceeds the limit.");
 	  foreach($files[$form] as $file){
	     $str = $file[1];
	     if (eval("if($str){return true;}")) {
	        $_values[$file[0]] = $_FILES[$file[0]]["name"];
	        $dirs = explode("/","attachments//");
	        $cur_dir =".";
	        foreach($dirs as $dir){
	        $cur_dir = $cur_dir."/".$dir;
	        if (!@opendir($cur_dir)) { mkdir($cur_dir, 0777);}}
	        $_values[$file[0]."_real-name"] = "attachments/".date("YmdHis")."_".$_FILES[$file[0]]["name"]."_secure";
	        copy($_FILES[$file[0]]["tmp_name"],$_values[$file[0]."_real-name"]);
	        @unlink($_FILES[$file[0]]["tmp_name"]);
	     }else{
	      $flag=true;
	      if ($_isdisplay) {
	      	//$ExtFltr = $file[2];
	      	//$FileSize = $file[4];
	        if (!eval("if($file[2]){return true;}")){echo $file[3];}
	        if (!eval("if($file[4]){return true;}")){echo $file[5];}
	      	$_ErrorList[] = $file[0];
	      }
	     }
	  }

	  if ($_isdisplay) {
	  	echo "</div>";
	  }
  }
  return $flag;
 }


 function display_page_brochureupload($_iserrors) {
   global $_values, $_referer;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head><script type="text/javascript">/* <![CDATA[ */ 
var fields = {
};
var req = {
	"fileGetterName" : ["fileGetterName", "Your PDF brochure is required."],
	"email" : ["email", "Email is required."],
	"Company" : ["Company", "Company is required."],
	"Name" : ["Name", "Name is required."]
};
var validate_form=true;function CheckForm(){HideAllErrors();if(!validate_form)return true;var LastErrorField=null;for(var i in fields){isError=ValidateField(fields[i][0],fields[i][1],fields[i][2],fields[i][3]);if(isError)LastErrorField=isError}for(var i in req){isError=isFilled(req[i][0],req[i][1]);if(isError)LastErrorField=isError}if(LastErrorField){LastErrorField.focus();return false}else return true}function ShowTooltip(type,field,message){var IE='\v'=='v';var container;if(!(container=document.getElementById('error_list'))){var container=(IE)?(document.createElement('<div name="error_list">')):(document.createElement('div'));container=document.createElement('div');container.setAttribute('id','error_list');document.body.appendChild(container)}var div_id=field+'_tooltip';if(!document.getElementById(div_id)){var elem=(IE)?(document.createElement('<div name="myName">')):(document.createElement('div'));var elem=document.createElement('div');elem.setAttribute('id',div_id);elem.className="fe_tooltip";elem.setAttribute('title','Click to close');elem.onmouseover=function(){MoveDivToTop(this)};elem.onclick=function(){HideTooltip(this.id)};var table=document.createElement('table');var tbody=document.createElement('tbody');table.setAttribute('border','0');table.setAttribute('cellSpacing','0');table.setAttribute('cellPadding','0');var row=document.createElement('tr');var cell1=document.createElement('td');cell1.className="leftTop";var cell2=document.createElement('td');cell2.className="top";var cell3=document.createElement('td');cell3.className="rightTop";row.appendChild(cell1);row.appendChild(cell2);row.appendChild(cell3);tbody.appendChild(row);var row=document.createElement('tr');var cell1=document.createElement('td');cell1.className="leftTop-2";var cell2=document.createElement('td');cell2.className="content-"+type;cell2.innerHTML=message;var cell3=document.createElement('td');cell3.className="right";row.appendChild(cell1);row.appendChild(cell2);row.appendChild(cell3);tbody.appendChild(row);var row=document.createElement('tr');var cell1=document.createElement('td');cell1.className="leftBottom";var cell2=document.createElement('td');cell2.className="bottom";var cell3=document.createElement('td');cell3.className="rightBottom";row.appendChild(cell1);row.appendChild(cell2);row.appendChild(cell3);tbody.appendChild(row);table.appendChild(tbody);elem.appendChild(table);parentField=document.getElementsByName(field);var f=0;while(parentField[f].type=='hidden')f++;with(elem.style){top=findPos(parentField[f])[0]-17+'px';left=findPos(parentField[f])[1]+parentField[f].offsetWidth+'px'}container.appendChild(elem)}}function ValidateField(name,rule,condition,message){fld=document.getElementsByName(name);var i=0;while(fld[i].type=='hidden')i++;if(!(((fld[i].value.match(rule)!=null)==condition)||(fld[i].value==""))){ShowTooltip('error',fld[i].name,message);return fld[i]}return null}function isFilled(name,message){fld=document.getElementsByName(name);var isFilled=false;var i=0;while(fld[i].type=='hidden')i++;var obj=fld[i];for(j=i;j<fld.length;j++){if((fld[j].type=='checkbox')||(fld[j].type=='radio')){if(fld[j].checked)isFilled=true}else{if(fld[j].value!="")isFilled=true}}if(isFilled){return null}else{ShowTooltip('error',name,message);return obj}}function FieldBlur(elemId){HideTooltip(elemId);fieldName=elemId.replace(/(\S{0,})_tooltip/,"$1");if(typeof fields[fieldName]!='undefined'){ValidateField(fields[fieldName][0],fields[fieldName][1],fields[fieldName][2],fields[fieldName][3])}if(typeof req[fieldName]!='undefined'){isFilled(req[fieldName][0],req[fieldName][1])}}function HideTooltip(elemId){/*document.body.focus();*/var elem=document.getElementById(elemId);var parent=document.getElementById('error_list');if((elem)&&(parent))parent.removeChild(elem)}function HideAllErrors(){error_container=document.getElementById('error_list');if(error_container!=null){while(error_container.childNodes.length>0){error_container.removeChild(error_container.firstChild)}}}function MoveDivToTop(div_to_top){div_container=document.getElementById('error_list');for(i=0;i<div_container.childNodes.length;i++)div_container.childNodes[i].style.zIndex="998";div_to_top.style.zIndex="999"}function findPos(obj){var curleft=curtop=0;do{curleft+=obj.offsetLeft;curtop+=obj.offsetTop}while(obj=obj.offsetParent);return[curtop,curleft]}
 /* ]]> */</script>
<style type="text/css"> .form_expert_style {display: none;}.fe_tooltip{position:absolute;}.fe_tooltip .content-error,.fe_tooltip .content-tooltip{padding: 1px 5px;cursor:default;font:13px arial,helvetica,verdana,sans-serif;background:white;}.fe_tooltip .content-error{color: #FF0000;}.fe_tooltip .content-tooltip{color:#999999;}.fe_tooltip .leftTop{font-size: 3px;height: 20px;width: 20px;background:url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite');}.fe_tooltip .leftTop-2{font-size: 3px;width: 20px;background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') no-repeat 0px -140px;}.fe_tooltip .leftBottom{font-size: 3px;height: 20px;width: 20px;background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') no-repeat 0px -120px;}.fe_tooltip .rightTop{font-size: 3px;width: 20px;background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') no-repeat 0px -40px;}.fe_tooltip .rightBottom{font-size: 3px;background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') no-repeat 0px -80px;}.fe_tooltip .right{font-size: 3px;width: 20px;background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-right') repeat-y;}.fe_tooltip .top{background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') repeat-x 0px -20px;}.fe_tooltip .bottom{background: url('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite') repeat-x 0px -100px;} </style>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Australia Expo Prospectus 2014</title>
		<link href="css/basic.css" rel="stylesheet" type="text/css" media="all" />
		<style type="text/css" media="screen"><!--ul
{
	margin-left: 0;
	padding-left: 0;
	list-style-type: none;
	margin-top: 0px;
}

li
{
padding-left: 0;
margin-left: 0;
background-image: url(images/blue_arrow_bullet.jpg);
background-repeat: no-repeat;
background-position: 0 0.4em;
padding-left: 1.1em;
margin: 0em 0;}--></style>
	</head>

	<body bgcolor="#69433a" topmargin="0" marginheight="0">
		<div align="center">
			<table width="650" border="0" cellspacing="0" cellpadding="0" bgcolor="white">
				<tr>
					<td width="25"><img src="images/top_shadow_left.jpg" alt="" width="25" height="10" border="0" /></td>
					<td width="400"></td>
					<td width="200"></td>
					<td width="0"><img src="images/top_shadow_right.jpg" alt="" width="25" height="10" border="0" /></td>
				</tr>
				<tr>
					<td rowspan="3" width="25" background="images/shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
					<td colspan="2" width="600"><img src="images/n&a_header.jpg" alt="" width="800" height="150" usemap="#beck_head_solid_wastec360d38f" border="0" /></td>
					<td rowspan="3" width="0" background="images/shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
				</tr>
				<tr>
					<td colspan="2" width="600">
						<menumachine name="npw" id="m3fs0c71">
							<csobj t="Component" csref="menumachine/npw/menuspecs.menudata"><noscript>
									<p><a class="mm_no_js_link" href="menumachine/npw/navigation.html">Site Navigation</a></p>
								</noscript> </csobj> 
							<script type="text/javascript"><!--
var mmfolder=/*URL*/"menumachine/",zidx=1000;
//--></script>
							<script type="text/javascript" src="menumachine/menumachine2.js"></script>
							<script type="text/javascript" src="menumachine/npw/menuspecs.js"></script>
							<script type="text/javascript"><!--//settings
pkg.hl("m3fs0c70",1);
//settings--></script>
						</menumachine>
					</td>
				</tr>
				<tr height="570">
					<td colspan="2" valign="top" width="600" height="570">
						<table width="798" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="796" border="0" cellspacing="0" cellpadding="4" align="left">
								  <tr>
											<td valign="top" width="1"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
											<td valign="top" class="headline"><span class="dsR7">BROCHURE UPLOAD FORM<br />
													<br />
												</span></td>
											<td valign="top" class="headline2" width="2"><img src="images/clear_pixel.gif" alt="clear" width="6" height="6" /></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td valign="top" class="text"></td>
											<td valign="top" class="headline2" width="2"></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td valign="top" class="text">
												<form id="profile" action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post" name="profile" enctype="multipart/form-data" onsubmit="return CheckForm(this);">
<?php IsThereErrors("0", $_iserrors); ?>
<input type="hidden" name="_referer" value="<?php echo $_referer ?>" />
<input type="hidden" name="_next_page" value="1" />
<p class="form_expert_style"><input name="URL" type="text" value="" /></p>

													<table width="100%" border="0" cellspacing="0" cellpadding="4">
														<tr>
															<td class="textbold" valign="top" width="20%">Delegate Name:</td>
															<td class="text" valign="top"><input type="text" name="Name" size="60"  onblur="FieldBlur(this.name+'_tooltip')" <?php mark_if_error("Name", "") ?> value="<?php echo isset($_values["Name"]) ? htmlspecialchars($_values["Name"]) : "" ?>"/></td>
														</tr>
														<tr>
															<td class="textbold" valign="top" width="20%">Company Name:</td>
															<td class="text" valign="top"><input type="text" name="Company" size="60"  onblur="FieldBlur(this.name+'_tooltip')" <?php mark_if_error("Company", "") ?> value="<?php echo isset($_values["Company"]) ? htmlspecialchars($_values["Company"]) : "" ?>"/></td>
														</tr>
														<tr>
															<td class="textbold" valign="top" width="20%">Email:</td>
															<td class="text" valign="top"><input type="text" name="email" size="60"  onblur="FieldBlur(this.name+'_tooltip')" <?php mark_if_error("email", "") ?> value="<?php echo isset($_values["email"]) ? htmlspecialchars($_values["email"]) : "" ?>"/></td>
														</tr>
														<tr>
															<td class="text" colspan="2" valign="top"><span class="textbold">Product Brochure PDF:</span><br />
																	To upload your Product Brochure PDF, please select the browse button below and locate the Product Brochure PDF on your computer hard drive and select it. The PDF will then appear in the form.<br />
																<br />
																<span class="textbold">Please note:</span> There is a 2mb limit on the size of the PDF. If you wish to send more than one file, you will need to combine the files into one by using a compression (combining) program such as &quot;Zip&quot; found on most computers.<br />
																<br />
																If you have trouble with this feature, please forward (attach) your items via email after you complete this form to:<br />
                                                                <a href="mailto:joseph@synaxismeetings.com">joseph@synaxismeetings.com</a><br />
																<br />
																<input type="file" name="fileGetterName" size="33"  onblur="FieldBlur(this.name+'_tooltip')" <?php mark_if_error("fileGetterName", "") ?>/><br />
																<br />
															</td>
														</tr>
														<tr>
															<td class="text" colspan="2" valign="top">
																<div align="center"><input type="submit" name="submitButtonName" value="Submit Brochure PDF"  onblur="FieldBlur(this.name+'_tooltip')" <?php mark_if_error("submitButtonName", "") ?>/></div>
															</td>
														</tr>
													</table>
												</form>
											</td>
											<td valign="top" class="headline2" width="2"></td>
										</tr>
										<tr>
											<td valign="top" width="1"></td>
											<td class="text" valign="top"><br />
											</td>
										  <td valign="top" class="text" width="2"></td>
										</tr>
									</table>
							  </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="25"><img src="images/left_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
					<td colspan="2" width="600" background="images/center_bottom_shadow.jpg"></td>
					<td width="0"><img src="images/right_corner_shadow.jpg" alt="" width="25" height="6" border="0" /></td>
				</tr>
				<tr height="25">
					<td width="25" height="25" background="images/top_shadow_left.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
					<td width="600" height="31" colspan="2"><table width="798" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="265" height="20" align="left" class="bottom_text"><div align="left">
                              <table width="800" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td class="bottom_text">
													<div align="center">
														Copyright 2007-2014. Australian States and Territories and Tourism Australia. All Rights Reserved.</div>
												</td>
                                </tr>
                              </table>
                          </div></td>
                        </tr>
                    </table></td>
				  <td width="0" height="25" background="images/top_shadow_right.jpg"><img src="images/clear_pixel.gif" alt="" width="6" height="6" border="0" /></td>
			  </tr>
				<tr>
					<td width="25"><img src="images/left_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
					<td colspan="2" width="600" background="images/bottom_shadow_blue.jpg"></td>
					<td width="0"><img src="images/right_bottom_corner.jpg" alt="" width="25" height="20" border="0" /></td>
				</tr>
			</table>
		</div>
	<p></p>
	<script type="text/javascript">/* <![CDATA[ */ function preloadImages(){var imgs=new Array();for (var i=0;i<arguments.length;i++){var imgsrc=arguments[i];imgs[i]=new Image();imgs[i].src=imgsrc;}}preloadImages('<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-sprite','<?php echo $_SERVER["PHP_SELF"];?>?image=tooltip-right'); /* ]]> */</script></body>

</html>

<?php }  
 function display_default() { ?> <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>Successful submission</title><link rel="shortcut icon" href="http://forms-expert.com/images/favicon.ico" /><style>html,body,form,fieldset{margin:0;padding:0}html,body{   height:100%}body{color:#000;background:#FFF;font-family:Tahoma,Arial,Helvetica,sans-serif;line-height:160%}body#bd{padding:0;color:#333;background-color:#FFF}body.fs4{font-size:12px}.componentheading{color:#4F4F4F;font-family:"Segoe UI","Trebuchet MS",Arial,Helvetica,sans-serif;font-weight:bold}small,.small{color:#666;font-size:92%}ul{list-style:none}ul li{padding-left:30px;background:url(../images/bullet-list.gif) no-repeat 18px 9px;line-height:180%}.componentheading{padding:0 0 15px 0;margin-bottom:0px;color:#4F4F4F;background:url(http://forms-expert.com/images/dot.gif) repeat-x bottom;font-size:250%;font-weight:bold}#ja-header{height:60px;position:relative;z-index:999;width:920px;margin:0 auto;clear:both}#ja-containerwrap,#ja-footer{width:920px;margin:0 auto;clear:both}#main-container{ min-height:100%; /*height:100%;*/   position:relative}#ja-footerwrap{clear:both;border-top:1px solid #CCC;margin-top:10px;background:url(../images/grad2.gif) repeat-x top; position:absolute; bottom:0; width:100%; height:60px}#ja-footer{padding:15px 0;position:relative}#ja-footer small{padding:4px 0 0 10px;float:left;display:block;color:#999;font-style:normal;line-height:normal}small.ja-copyright{position:absolute;right:10px}#ja-footer a{color:#666;text-decoration:none}#ja-footer a:hover,#ja-footer a:active,#ja-footer a:focus{color:#666;text-decoration:underline}#ja-footer ul{margin:4px 0 5px 10px;padding:0;float:left;background:url(http://forms-expert.com/images/vline.gif) no-repeat center right;line-height:normal}#ja-footer li{margin:0;padding:0;display:inline;background:none}#ja-footer li a{padding:0 10px;display:inline;background:url(http://forms-expert.com/images/vline.gif) no-repeat center left;font-size:92%;line-height:normal}.clearfix:after{clear:both;display:block;content:".";height:0;visibility:hidden}* html >body .clearfix{width:100%;display:block}* html .clearfix{height:1%}/* Firefox Scrollbar Hack - Do not remove *//*html{margin-bottom:1px;height:100%!important;height:auto}*/a{color:#F90}a:hover,a:active,a:focus{color:#F90}#ja-containerwrap{padding:0;padding-bottom:60px}</style></head><body id="bd" class="fs4"><div id="main-container"><br><br><br><br><br><div id="ja-containerwrap">	<div id="ja-container" class="clearfix"><div style="padding: 20px 30px 20px 30px;"><div class="ja-innerpad clearfix"><div class="componentheading">Your submission was successful. Thank you.</div><p align="right">This form was processed by <a href="http://forms-expert.com">Forms Expert</a>.<p align="right">&copy; 2009 Forms-Expert. </div></div></div></div><div id="ja-footerwrap"><div id="ja-footer" class="clearfix"><ul><li><a href="http://forms-expert.com">Visit Forms Expert</a></li><li><a href="http://forms-expert.com/form-processing-features/">Features</a></li><li><a href="http://forms-expert.com/download/">Download Beta</a></li><li><a href="http://forms-expert.com/support/">Support</a></li><li><a href="http://forms-expert.com/contactus/">Contact page</a></li></ul><small class="ja-copyright">&copy; 2009 <a href="http://forms-expert.com/">Forms-Expert</a></small></div></div></div></body></html> <?php }
 function redirect_to() { header("Location: http://www.synaxismeetings.com/naw/thankyoubro.php"); }
  function BuildBody($body, $html, $num){
	   global $zag, $un;
	   if ($html) {
       	  $zag[$num]       = "--".$un."\r\nContent-Type:text/html;\r\n";
       }
	    else {
           $zag[$num]       = "--".$un."\r\nContent-Type:text/plain;\r\n";
	     };
       $zag[$num]      .= "Content-Transfer-Encoding: 8bit\r\n\r\n$body\r\n\r\n";
  }
  function SendEmails (){
    global $_values, $zag, $un;
    $un        = strtoupper(uniqid(time()));
    $to[0]      .= "joseph@synaxismeetings.com";
    $from[0]      .= "".str_replace("%,%", ",", $_values['email'])."";
    $subject[0]      .= "NAW brochureupload was submitted on ".date("F j, Y")." ".date("H:i")."";
    $head[0]     .= "MIME-Version: 1.0\r\n";
    $head[0]      .= "From: ".str_replace("%,%", ",", $_values['email'])."\r\n";
    $head[0]     .= "X-Mailer: Forms Expert at www.forms-expert.com\r\n";
    $head[0]      .= "Reply-To: ".str_replace("%,%", ",", $_values['email'])."\r\n";
    $head[0]     .= "Content-Type:multipart/mixed;";
    $head[0]     .= "boundary=\"".$un."\"\r\n\r\n";
    $EmailBody = "<html><body>
Brochure Upload Form was filled with the following data:
<br><b>Name:</b> ".htmlspecialchars(str_replace("%,%", ",", $_values["Name"]))."
<br><b>Company:</b> ".htmlspecialchars(str_replace("%,%", ",", $_values["Company"]))."
<br><b>email:</b> ".htmlspecialchars(str_replace("%,%", ",", $_values["email"]))."
<br><b>fileGetterName:</b> ".htmlspecialchars(str_replace("%,%", ",", $_values["fileGetterName"]))."
</body></html>
";
    BuildBody($EmailBody, True, 0);
    $path = $_values['fileGetterName_real-name'];
    if (strlen($_values['fileGetterName'])>0) {AddAttach($path,$_values['fileGetterName'],0);}
    for ($i=0;$i<=0;$i++){
      mail($to[$i], $subject[$i], $zag[$i], $head[$i]);
    }
  }
  function AddAttach ($filename, $name, $num){
       global $zag, $un;
       $f		      = fopen($filename,"rb");
       $zag[$num]      .= "--".$un."\r\n";
       $zag[$num]      .= "Content-Type: application/octet-stream;";
       $zag[$num]      .= "name=\"".basename($name)."\"\r\n";
       $zag[$num]      .= "Content-Transfer-Encoding:base64\r\n";
       $zag[$num]      .= "Content-Disposition:attachment;";
       $zag[$num]      .= "filename=\"".basename($name)."\"\r\n\r\n";
       $zag[$num]      .= chunk_split(base64_encode(@fread($f,filesize($filename))))."\r\n\r\n";
       @fclose($f);
  }

 $actions = array ("display_page_brochureupload","redirect_to");


	if (in_array($_GET["image"], array("warning", "tooltip-sprite", "tooltip-right"))) {
		header("Content-type: image/png");
		switch ($_GET["image"]) {
			case "warning":
				echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanZZ3VFTXFofPvXd6oc0w0hl6ky4wgPQuIB0EURhmBhjKAMMMTWyIqEBEEREBRZCggAGjoUisiGIhKKhgD0gQUGIwiqioZEbWSnx5ee/l5ffHvd/aZ+9z99l7n7UuACRPHy4vBZYCIJkn4Ad6ONNXhUfQsf0ABniAAaYAMFnpqb5B7sFAJC83F3q6yAn8i94MAUj8vmXo6U+ng/9P0qxUvgAAyF/E5mxOOkvE+SJOyhSkiu0zIqbGJIoZRomZL0pQxHJijlvkpZ99FtlRzOxkHlvE4pxT2clsMfeIeHuGkCNixEfEBRlcTqaIb4tYM0mYzBXxW3FsMoeZDgCKJLYLOKx4EZuImMQPDnQR8XIAcKS4LzjmCxZwsgTiQ7mkpGbzuXHxArouS49uam3NoHtyMpM4AoGhP5OVyOSz6S4pyalMXjYAi2f+LBlxbemiIluaWltaGpoZmX5RqP+6+Dcl7u0ivQr43DOI1veH7a/8UuoAYMyKarPrD1vMfgA6tgIgd/8Pm+YhACRFfWu/8cV5aOJ5iRcIUm2MjTMzM424HJaRuKC/6386/A198T0j8Xa/l4fuyollCpMEdHHdWClJKUI+PT2VyeLQDf88xP848K/zWBrIieXwOTxRRKhoyri8OFG7eWyugJvCo3N5/6mJ/zDsT1qca5Eo9Z8ANcoISN2gAuTnPoCiEAESeVDc9d/75oMPBeKbF6Y6sTj3nwX9+65wifiRzo37HOcSGExnCfkZi2viawnQgAAkARXIAxWgAXSBITADVsAWOAI3sAL4gWAQDtYCFogHyYAPMkEu2AwKQBHYBfaCSlAD6kEjaAEnQAc4DS6Ay+A6uAnugAdgBIyD52AGvAHzEARhITJEgeQhVUgLMoDMIAZkD7lBPlAgFA5FQ3EQDxJCudAWqAgqhSqhWqgR+hY6BV2ArkID0D1oFJqCfoXewwhMgqmwMqwNG8MM2An2hoPhNXAcnAbnwPnwTrgCroOPwe3wBfg6fAcegZ/DswhAiAgNUUMMEQbigvghEUgswkc2IIVIOVKHtCBdSC9yCxlBppF3KAyKgqKjDFG2KE9UCIqFSkNtQBWjKlFHUe2oHtQt1ChqBvUJTUYroQ3QNmgv9Cp0HDoTXYAuRzeg29CX0HfQ4+g3GAyGhtHBWGE8MeGYBMw6TDHmAKYVcx4zgBnDzGKxWHmsAdYO64dlYgXYAux+7DHsOewgdhz7FkfEqeLMcO64CBwPl4crxzXhzuIGcRO4ebwUXgtvg/fDs/HZ+BJ8Pb4LfwM/jp8nSBN0CHaEYEICYTOhgtBCuER4SHhFJBLVidbEACKXuIlYQTxOvEIcJb4jyZD0SS6kSJKQtJN0hHSedI/0ikwma5MdyRFkAXknuZF8kfyY/FaCImEk4SXBltgoUSXRLjEo8UISL6kl6SS5VjJHslzypOQNyWkpvJS2lIsUU2qDVJXUKalhqVlpirSptJ90snSxdJP0VelJGayMtoybDFsmX+awzEWZMQpC0aC4UFiULZR6yiXKOBVD1aF6UROoRdRvqP3UGVkZ2WWyobJZslWyZ2RHaAhNm+ZFS6KV0E7QhmjvlygvcVrCWbJjScuSwSVzcopyjnIcuUK5Vrk7cu/l6fJu8onyu+U75B8poBT0FQIUMhUOKlxSmFakKtoqshQLFU8o3leClfSVApXWKR1W6lOaVVZR9lBOVd6vfFF5WoWm4qiSoFKmclZlSpWiaq/KVS1TPaf6jC5Ld6In0SvoPfQZNSU1TzWhWq1av9q8uo56iHqeeqv6Iw2CBkMjVqNMo1tjRlNV01czV7NZ874WXouhFa+1T6tXa05bRztMe5t2h/akjpyOl06OTrPOQ12yroNumm6d7m09jB5DL1HvgN5NfVjfQj9ev0r/hgFsYGnANThgMLAUvdR6KW9p3dJhQ5Khk2GGYbPhqBHNyMcoz6jD6IWxpnGE8W7jXuNPJhYmSSb1Jg9MZUxXmOaZdpn+aqZvxjKrMrttTjZ3N99o3mn+cpnBMs6yg8vuWlAsfC22WXRbfLS0suRbtlhOWWlaRVtVWw0zqAx/RjHjijXa2tl6o/Vp63c2ljYCmxM2v9ga2ibaNtlOLtdZzllev3zMTt2OaVdrN2JPt4+2P2Q/4qDmwHSoc3jiqOHIdmxwnHDSc0pwOub0wtnEme/c5jznYuOy3uW8K+Lq4Vro2u8m4xbiVun22F3dPc692X3Gw8Jjncd5T7Snt+duz2EvZS+WV6PXzAqrFetX9HiTvIO8K72f+Oj78H26fGHfFb57fB+u1FrJW9nhB/y8/Pb4PfLX8U/z/z4AE+AfUBXwNNA0MDewN4gSFBXUFPQm2Dm4JPhBiG6IMKQ7VDI0MrQxdC7MNaw0bGSV8ar1q66HK4RzwzsjsBGhEQ0Rs6vdVu9dPR5pEVkQObRGZ03WmqtrFdYmrT0TJRnFjDoZjY4Oi26K/sD0Y9YxZ2O8YqpjZlgurH2s52xHdhl7imPHKeVMxNrFlsZOxtnF7YmbineIL4+f5rpwK7kvEzwTahLmEv0SjyQuJIUltSbjkqOTT/FkeIm8nhSVlKyUgVSD1ILUkTSbtL1pM3xvfkM6lL4mvVNAFf1M9Ql1hVuFoxn2GVUZbzNDM09mSWfxsvqy9bN3ZE/kuOd8vQ61jrWuO1ctd3Pu6Hqn9bUboA0xG7o3amzM3zi+yWPT0c2EzYmbf8gzySvNe70lbEtXvnL+pvyxrR5bmwskCvgFw9tst9VsR23nbu/fYb5j/45PhezCa0UmReVFH4pZxde+Mv2q4quFnbE7+0ssSw7uwuzi7Rra7bD7aKl0aU7p2B7fPe1l9LLCstd7o/ZeLV9WXrOPsE+4b6TCp6Jzv+b+Xfs/VMZX3qlyrmqtVqreUT13gH1g8KDjwZYa5ZqimveHuIfu1nrUttdp15UfxhzOOPy0PrS+92vG140NCg1FDR+P8I6MHA082tNo1djYpNRU0gw3C5unjkUeu/mN6zedLYYtta201qLj4Ljw+LNvo78dOuF9ovsk42TLd1rfVbdR2grbofbs9pmO+I6RzvDOgVMrTnV32Xa1fW/0/ZHTaqerzsieKTlLOJt/duFczrnZ86nnpy/EXRjrjup+cHHVxds9AT39l7wvXbnsfvlir1PvuSt2V05ftbl66hrjWsd1y+vtfRZ9bT9Y/NDWb9nffsPqRudN65tdA8sHzg46DF645Xrr8m2v29fvrLwzMBQydHc4cnjkLvvu5L2key/vZ9yff7DpIfph4SOpR+WPlR7X/aj3Y+uI5ciZUdfRvidBTx6Mscae/5T+04fx/Kfkp+UTqhONk2aTp6fcp24+W/1s/Hnq8/npgp+lf65+ofviu18cf+mbWTUz/pL/cuHX4lfyr468Xva6e9Z/9vGb5Dfzc4Vv5d8efcd41/s+7P3EfOYH7IeKj3ofuz55f3q4kLyw8Bv3hPP7yeKvygAAAARnQU1BAACxjnz7UZMAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAB+JJREFUeNpi/P//P8NAAoAAYmIYYAAQQIzT4rlJ0wEMsL9/fxuIyoi3cfLyyr56+Hjtj6+/ZjKzsjwn1fLMBV8YAAKIhVRNf37/MNO2sV1rG1kpw8ojyPDyxjGd3XM7vd+/fuvGwsr6nlTzAAKIpCj48+u3oKSS0nS76FYZVn5VYGhwM4jrRjA4J1WYsLIyZJATBQABRLQD/v37x8DC+j/KzD/NiIWXjYHhywoGhu8rgfQ2BklNFwYta9fCn9+/aZDqAIAAItoBf3//ElHQNWuT07FiYPiwlYHh10tgkHxhYPhxnoHh9ykGPedYUX4RwZq/f/6Q5ACAACLKAaCsys7JXm/qEc3H8PMC0PLnoJQIihMg/Y+B4fNZBgFJXgYdG6/w///+2JDiAIAAYiIu7n/qalk6hInICjMwfD0HtBRk8Xco/gFU8ImB4dNRBn07DxYhCam2f3//chDrAIAAIugAUJDyCQkU6dq4iDH8AFr++wNQ8BswUXwFyn6F0H9/AtPDDQY2zg8MJq4+tn///okgtoADCCAmwonvr6e2hUOcgCjQwC+XgPHxA2zx1y8fGF4/f8Pw+/dnqEOAofH5KIOKvh6DnJpayZ/fv4kqYAACCK8DgEHJKiwhVaxnY8XE8Pk40HJgUDN/Zfj29SPD9EmXGKpLjzLs3HILLMbABAyV348YmJluMxi7+mmzsLI0MRARCgABxEQg+OOMXdyc2bleAA2/A7QEGPes3xnevv/I8PzJFwYhfhaGx4/+MPz8AQwVZqADmICh8OUEg6yaKIOakWn071+/tAg5ACCAcDrg989fwsCgLFfRVQYmsAMMDIxACxiB2Y7hEwM7xx8GHh5uYEb4x8DBxc7AzApMA4zAqGAEOuAfMHt+P8pg7OgszsXLU/APlFvwAIAAYsKV7VjZWJP07R1VWf6D8vljoOh3iAOAmI3zNwMXDwfQAf8ZuLj/MLBwAqOA8StUHhhKX08zCIp9ZdC1tk0EhqIXPgcABBATjqBXVtDWKVbUAOamj4eAIqCE9w2C/30BhsAvBl5+VqA6JiANlGP/CHT1VyQ1QP6H7QwGtsYsIlKSZf/+/sNZ5wAEEBO2IpeVg6vIxMFUnOHTYYb/v98AQ+QXw39gKgdhUGpnZ//OwMP3l4GZhZ1BSIQRKPYBKPcDruY/w2+G/99uMLAzXWEwcrCz//vnbyYuBwAEEBNmkfvXWMNIN15U9DPDn49nGP6Cqt9/vxj+/Yfiv78ZmDi+MggKfWVgYWNl4BdiBpaMP8COhKn5++83EP9h+PtmD4OyliCDnLpyzu9fv2WxOQAggJjQsh0Dn7DQBAMrDe6/b/cx/PvzD2jgH6A4BP8H0X+Aieo30AGC/xk4gQmQg/03kA/U+wcqD8PAcPj78wMD64+TDIb2Zmps7OxZ/4Ghiw4AAgjVAf/+R+uYGVjzsjxg+P7+CQPILqBnGIBuANN/f0NoUJLgFwBazvGbQVjwH7g0RpaHqQfp//riPIOs1E8GBW2NnL9//1qgOwAggJiQEp6gqJR0kbouH+O3ZyeBPgKJgRogQA9CDQdXAUD6FzC9SUsxMwREmzFwsb4CxQCKPEg9SB9I/9+ffxi+Pz/OYGipzMPJzV8ETGPMyA4ACCAmWLZjYGTJ0bVUNWL+coPh1+dP4MoOZuA/GP4FKfZB4oy/nzBwMh5h+PzuLwPjH4g4SB6mFu4goDk/3jxk4Gd/waBrqR0K5Ecjl5AAAcQE8f0/ZWkFmQw5qT8MX55eg7gcajkYAw3/8wNCMwDlPn1gZFi0mI2hve03w5YtHAw/gTnvP5o6uF4o/vrwLIO6Ji+DmJR4+Z+//yRhDgAIICZQK5OFlbNJz1xK6ueLqwy/vvwBBx+oqv/zE4p/QRwEYoPi9+0rZob799gYhPn/M9y5y8/w9g07JM6hoYOuF9RG+f7uM8O/91cZ9CwUtJiY2LLBrVsgAAggpj+//5krakkGC3C8Y/j89AUDMOGDDYHFI9gAoK9+QQ3+ASzsBPkFGPQMVRl+/PjLoK4lx8DNyQVOFyD5X1D1YH2w9ANiA8399OABg4TwFwYlbenSP7//W4McABBALEyMHI7SMqzs3x7cYfgNLGeYQHEPTCZMQFczsgIxKMmwQOohBiCbERhmzKzvGDycdBkcXOIYOP+cY/j36T3Db6C+/6BcBso5/yBR9f8vJGr+Qdl/f/1j+HrvNoOquhrbk9vcqUBVRwECiIWZjV36x6PXDN/+fmX4D7QAhJnYIJYxMUEiiRFU2AEx039YjvnHwPRxPwMX435gOQG1ENpnAKljAOL/oMgF8YHmgNT8h2bPL08+MrAAKywuPhYzkBaAAGL59vnVqSf3GBgUBYFFOjcjAzMnE7CEA1oGrAaYWBnBIQDyNZhmRDgI0q2BOIqJAWIZsPQB0/+hoQELgb8//0NyE9Alv7//Y7h/7gnD67cMm0FGAAQQi0VQ2frbpzZOPn37Rryk6H8+7l8sDGzfmBmYQaHAxAi2HGQRjIZ3qRiRa0/UnhM4KmA0KE39/sfw6/t/hl8f/zA8fwrMrFKqS0z9/dtAygECiBFUBnx4cZvh8/tnGoyMzLJAO7mAQc8G8TuSlaT0YRlRa3dgsfwXaM0vYFQB8Z8XvIJSVwQl1UBlKgNAADHiaTwyYRhFHviPhDEAQACx4HA7I5UsZ8BiDopDAAIMAP+QrU5p/QTlAAAAAElFTkSuQmCC");
				break;
			case "tooltip-sprite":
				echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAABQAAAG4CAYAAABFIMcsAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABwxJREFUeNrsnM9vVFUUx7/3zXTamWmFzkwi7ayICxdIdKE2tAUVJXHrRjckYALaFjAtRon8UCMqGjRtLFGCGiFh5YrEfwBJQEpBQYXEFasyGIamLfOj7byZ64L7mts37725b3orr53zkpuZRecz59xz7vlx750yzjl0PgY0Pw0IDNfzoXQqwbQB06mEoUXCdCrRBIBrUVnAwlqAEiwCgC0JaIO1AAjVDXSAxcV7NT9MpxJMjJALrBnAmgOHDr8GYK3bCNv8yhAjJIBNAtQMIAog+lxX1zMT2cnPXSX0gEUEqEXAHntz1+6nN3X3vK6ishssCiAGIL67r3/Dx0c//UzFKEwMwzZvMTF38bcGBp46dOSjL8NNTXEVoNOcLUj29sCejQePfHhcBWYHVsH69uzd+MHhI1+pwrLZzAJQVjUKIDaw752NBw4e+toPLJXqAKOcEsCc4pUf6k1S2oGGbmBYN7BFNzCmG/iYbuBanUDGNUcHCg4arJzNZhBoK2uPNuQ2GqzcmWxnADCRneS6gIZbdV/Pl8g5hdtekU4lfIPlnMIdhm+wIUq4iDSsgj0khlWQQiWHWznFkqgMoALAFO/L4u8q9unwkrBNjFZRAkfFaJakNSxJa0lp5ZSKkMYEMC/GnLTOLcnKytHGNM3c3UzmwujI8MmzZ07fBlAAMCvGHICSAHIA3M1AVeHLNM3ciZHh/uNfHLsOIC/ARSG1KbRxBVYtvXA43Lp3cOi77Tt2rhdWD9vm0f9aDofDrfsGh/okoCEbpq7gsK6jY4skXUgFBgBGsZiHm5QCJEtW08mNQiEHN6gDbMGYrh9IJh9HPj/jBTRU1aWcQkACEpCAjQ28f/9fxOOuPXjFqhRUKi8AMGKxVkSjcS8gdwC6wg03mGmaOalerKpslWsb67mbyVwQxZEpV111GcU0zdzoyPBJW8WlNJeGE+zEyHC/qBHnJSkr8HO44FJwyoWmGjCdSnR5lMRVlatXsWlJOCX+sCKpZwpQSTJKRbXxeVCjrVjkOrWan7CYK9nHKjar+lopobZYlNmkKdugqOXMdglNt+bRD0gGVnS3t7zeD7s1PpQCgrY7p7oCVs92H7mNBiunU4lNuv1wSjdwRjewoBs4qxto6gZWdAO57uBAazmAOaXqETtv8t2HZjzcTW4DsHYiOzlm/0yxmEehkPM8aLUXTXK5V/VY7YlRo0jiDrCy6GEcoSpzKFe3JgBT9DB1GcVe1ZoA5kdHhk+6SakqoVzQz589c/r2iZHhfieo69KTLM2w+C5Jixix7Tt2rt83ONS3rqNji9iz9V7LEjQkQa27OPIph7WTbKiez1uuIp8NlEWDtGjjvGa0saluSNKGsPh6DQPAVCXkNknlVm7R1rRSPJQ2weVXp+EvwDqA7V9SX8T2OPVh2lKA9SWUU4IITKcS8XQqkUinEul0KvFkOpV49tjRT3aZpVIOzueljiObzXAA3H7VLTL03vsbBve/O6J6I63q6Uy2r+lMtq/rTLY/8e3oN2+USqUHvI7n3r07nHPOWWeyfY1IkfHbdzK/RiLNnfUIZt2dkw+r+Y+nTu13yxWqT6gtFo1Yse3C+fMzjLE/nu/q2maEQhE/oEIhh1isDaG2WLQJ0pXLy5cuTpumeX1Td/fLfqAyMCRFYwMAuzJ2eWp+fv5Gd0/PVlWoDLQ7NwPAxq+MTc3Ozt7o6e19SQUqA10PAK+OX5kqFos3eno314TKQM9vvjY+PlUoFv7s7d38ohfUDmQulRYAsGvj49P5fP6v3s1bXnCD2lXmHqUcA8B+v3p1Znp6+ubWV7a96gU0pNKt4rDTOSc6rSKA4k8/fH/rt0sXf64ZvjygJWnrdBbA3PjY2HWleGgrMu3QhX3ZX86d+0c5wAood4GWAJRu3fx7ylfEdoDKYLNWs2n4qF7t26hLKom5LiBcugFtWU87UEtelitX6972koCyUbgOo9gdvax7Dp0v7jAGznlNx7a/517ASqXs2t76rkIfAisBLuc45zAMQxlo70+WDFSSkDHj0S096gL0ABmWcjNtuSVkgZdQfT3rSqMPA2zFVwrwDLCGYfiOhzUTvR+VFaMN8x0PawTYkG8J/78AS+GLgARcMnAiO4mJ7OTKVFlb1tOaRpnLa7BUXrY51K4yreVGAWrZxFC/tqpTukeqsrLqhk51A6MydLmNdgkb1A8pHhKQgAQkIAEJSEACEpCABCQgAQlIQAISkIAEJCABCUhAAhKQgAQkIPxfTg6+yiv8cjIBCUhAAhKQgAQk4AoDshWhMtWHBFx1fqh16QW712PkNsGSkDWmH5LbNEpwYKDahoCr0CjBdmxK9LT0CEhAAhKQgFqAwfxBtBJsdc0hXzFu0yC/AeeN6YfLauVgAnmg/XBZ4yH9PwcCLgfwvwEATKtT/9vapTYAAAAASUVORK5CYII=");
				break;
			case "tooltip-right":
				echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAABQAAAABCAYAAADeko4lAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAANUlEQVR42mL8DwRv3jxnEBGRZEAH0iJClkDqAxB/AuJvQPwDiP8A8T8g/v/0zbv/6HoAAgwAVUYR/63ruiwAAAAASUVORK5CYII=");
				break;
		}
	}else{
		if (isset($_POST["_next_page"])) {
			$_next_page = $_POST["_next_page"];
		}else $_next_page = 0;

		if (isset($_POST["back"])) {
			call_user_func($actions[$_next_page-2],false);
		}else
			if (IsThereErrors($_next_page-1, false)){
				call_user_func($actions[$_next_page-1],true);
			}else {
				call_user_func($actions[$_next_page+0],false);
				if ($_next_page == count($actions)-1) {
					SendEmails();
					session_destroy();
				}
			}
		}
?>
