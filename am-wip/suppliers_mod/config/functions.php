<?php

function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

function vd($e) {
    echo "<code>";
    var_dump($e);
    echo "</code>";
}

function save_supplier_application($return = FALSE) {
    $data = array();
    $data['delegate_fname'] = $_POST['Name'];
    $data['delegate_lname'] = $_POST['Name2'];
    $data['job_title'] = $_POST['Title'];
    $data['company_name'] = $_POST['Company'];
    $data['address1'] = $_POST['Address_1'];
    $data['address2'] = $_POST['Address_2'];
    $data['tel'] = $_POST['Telephone'];
    $data['fax'] = $_POST['Fax'];
    $data['email'] = $_POST['email'];
    $data['website'] = $_POST['Website'];
    $sales_points = array(
        $_POST['Point1'],
        $_POST['Point2'],
        $_POST['Point3'],
        $_POST['Point4'],
        $_POST['Point5'],
    );
    $data['sales_points'] = serialize($sales_points);
    if (isset($_POST['ATEC']) && $_POST['ATEC'] == 'Yes') {
        $data['is_atec'] = 1;
    }

    if (isset($_POST['ATDW']) && $_POST['ATDW'] == 'Yes') {
        $data['is_atdw'] = 1;
    }
    $data['description'] = $_POST['Description'];
    $product_suitable_for = array();
    if (isset($_POST['Individuals'])) {
        $product_suitable_for[] = 'Individuals';
    }
    if (isset($_POST['Adventure'])) {
        $product_suitable_for[] = 'Adventure';
    }
    if (isset($_POST['Incentive'])) {
        $product_suitable_for[] = 'Incentive';
    }
    if (isset($_POST['Groups'])) {
        $product_suitable_for[] = 'Groups';
    }
    if (isset($_POST['Luxury'])) {
        $product_suitable_for[] = 'Luxury';
    }
    if (isset($_POST['Gay'])) {
        $product_suitable_for[] = 'Gay';
    }
    if (isset($_POST['Youth'])) {
        $product_suitable_for[] = 'Youth';
    }
    if (isset($_POST['Eco'])) {
        $product_suitable_for[] = 'Eco';
    }
    if (isset($_POST['Other'])) {
        $product_suitable_for[] = 'Other';
    }
    $data['product_suitable_for'] = serialize($product_suitable_for);
    $data['a_retail_roadshow'] = $_POST['Roadshow'];
    if ($return === TRUE)
        return $data;
    global $database;
    return $database->insert('supplier_application', $data);
}

function save_hotel_request($return = FALSE) {
    $data = array();
    $data['delegate_fname'] = $_POST['Name'];
    $data['delegate_lname'] = $_POST['Name2'];
    $data['company_name'] = $_POST['Company'];
    $data['email'] = $_POST['email'];
    if (isset($_POST['2nd_Delegate_Name']) && $_POST['2nd_Delegate_Name']) {
        $data['s_delegate'] = 1;
        $data['s_delegate_fname'] = $_POST['2nd_Delegate_Name'];
        $data['s_delegate_lname'] = $_POST['2nd_Delegate_Name2'];
        $data['s_email'] = $_POST['2nd_Delegate_Email'];
    }
    $data['room_type'] = $_POST['Room_Type'];
    $data['arrival_date'] = $_POST['Arrival_Date'];
    $data['departure_date'] = $_POST['Depart_Date'];
    $data['sharing_with'] = $_POST['Sharing_With'];
    if (isset($_POST['2nd_Delegate_Seprate_Room']) && $_POST['2nd_Delegate_Seprate_Room'] == 'on') {
        $data['s_separate_room'] = 1;
        $data['s_room_type'] = $_POST['2nd_Delegate_Room_Type'];
        $data['s_arrival_date'] = $_POST['2nd_Delegate_Arrival_Date'];
        $data['s_departure_date'] = $_POST['2nd_Delegate_Depart_Date'];
        $data['s_sharing_with'] = $_POST['2nd_Delegate_Sharing_With'];
    }
    $data['aai_date'] = $_POST['Airline_Date_Arrive'];
    $data['aai_carrier'] = $_POST['Carrier_Arrive'];
    $data['aai_from'] = $_POST['From_Arrive'];
    $data['aai_to'] = $_POST['To_Arrive'];
    $data['aai_dep_time'] = $_POST['Departure_Time_Arrive'];
    $data['aai_arr_time'] = $_POST['Arrival_Time_Arrive'];
    $data['dai_date'] = $_POST['Airline_Date_Depart'];
    $data['dai_carrier'] = $_POST['Carrier_Depart'];
    $data['dai_from'] = $_POST['From_Depart'];
    $data['dai_to'] = $_POST['To_Depart'];
    $data['dai_dep_time'] = $_POST['Departure_Time_Depart'];
    $data['dai_arr_time'] = $_POST['Arrival_Time_Depart'];
    if (isset($_POST['2nd_Delegate_Requires_Transfer']) && $_POST['2nd_Delegate_Requires_Transfer'] == 'on') {
        $data['s_separate_fly'] = 1;
        $data['s_aai_date'] = $_POST['2nd_Delegate_Airline_Date_Arrive'];
        $data['s_aai_carrier'] = $_POST['2nd_Delegate_Carrier_Arrive'];
        $data['s_aai_from'] = $_POST['2nd_Delegate_From_Arrive'];
        $data['s_aai_to'] = $_POST['2nd_Delegate_To_Arrive'];
        $data['s_aai_dep_time'] = $_POST['2nd_Delegate_Departure_Time_Arrive'];
        $data['s_aai_arr_time'] = $_POST['2nd_Delegate_Arrival_Time_Arrive'];
        $data['s_dai_date'] = $_POST['2nd_Delegate_Airline_Date_Depart'];
        $data['s_dai_carrier'] = $_POST['2nd_Delegate_Carrier_Depart'];
        $data['s_dai_from'] = $_POST['2nd_Delegate_From_Depart'];
        $data['s_dai_to'] = $_POST['2nd_Delegate_To_Depart'];
        $data['s_dai_dep_time'] = $_POST['2nd_Delegate_Departure_Time_Depart'];
        $data['s_dai_arr_time'] = $_POST['2nd_Delegate_Arrival_Time_Depart'];
    }
    $data['emergency_name'] = $_POST['Emergency_Contact_Name'];
    $data['emergency_rel'] = $_POST['Relationship'];
    $data['emergency_evening_phone'] = $_POST['Evening_Phone'];
    $data['emergency_cell_phone'] = $_POST['Cell_Phone'];
    if (isset($_POST['2nd_Delegate_Emergency_Contact_Name']) && $_POST['2nd_Delegate_Emergency_Contact_Name']) {
        $data['s_emergency_name'] = $_POST['2nd_Delegate_Emergency_Contact_Name'];
        $data['s_emergency_rel'] = $_POST['2nd_Delegate_Relationship'];
        $data['s_emergency_evening_phone'] = $_POST['2nd_Delegate_Evening_Phone'];
        $data['s_emergency_cell_phone'] = $_POST['2nd_Delegate_Cell_Phone'];
    }
    if ($return === TRUE)
        return $data;
    global $database;
    return $database->insert('hotel_request', $data);
}

function save_supplier_payment($return = FALSE) {
    $data = array();
    $data['aten_fname'] = $_POST['Name'];
    $data['aten_lname'] = $_POST['LastName'];
    $data['email'] = $_POST['Email'];
    $data['registration_fee'] = $_POST['RegistrationFee'];
    $data['share'] = $_POST['Share'] ? $_POST['Share'] : $_POST['Separate'];
    $data['code_amount'] = $_POST['CodeAmount'];
    $data['amount'] = $_POST['Amount'];
    $data['accept_amount'] = $_POST['AcceptAmount'];
    $data['card'] = $_POST['Card'];
    $data['cc_name'] = $_POST['CCName'];
    $data['cc_number'] = $_POST['CCNumber'];
    $data['cc_month'] = $_POST['CCMonth'];
    $data['cc_year'] = $_POST['CCYear'];
    $data['cvc'] = $_POST['CVC'];
    if ($return === TRUE)
        return $data;
    global $database;
    return $database->insert('supplier_payment', $data);
}

?>