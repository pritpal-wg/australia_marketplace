<?php

require_once __DIR__ . '/db_config.php';
require_once __DIR__ . '/medoo.min.php';

$attr = array(
    'database_type' => 'mysql',
    'database_name' => DB_NAME,
    'server' => DB_HOST,
    'username' => DB_USER,
    'password' => DB_PASS,
    'charset' => 'utf8',
);

$database = new medoo($attr);
$db = &$database;
?>