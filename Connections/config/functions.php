<?php

function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

function vd($e) {
    echo "<code>";
    var_dump($e);
    echo "</code>";
}

function save_supplier_application($return = FALSE) {
    $data = array();
    $data['delegate_fname'] = $_POST['Name'];
    $data['delegate_lname'] = $_POST['Name2'];
    $data['job_title'] = $_POST['Title'];
    $data['company_name'] = $_POST['Company'];
    $data['address1'] = $_POST['Address_1'];
    $data['address2'] = $_POST['Address_2'];
    $data['tel'] = $_POST['Telephone'];
    $data['fax'] = $_POST['Fax'];
    $data['email'] = $_POST['email'];
    $data['website'] = $_POST['Website'];
    if (isset($_POST['2nd_Delegate_Name']) && $_POST['2nd_Delegate_Name']) {
        $data['s_delegate'] = 1;
        $data['s_delegate_fname'] = $_POST['2nd_Delegate_Name'];
        $data['s_delegate_lname'] = $_POST['2nd_Delegate_Name2'];
        $data['s_job_title'] = $_POST['2nd_Delegate_Job_Title'];
        $data['s_company_name'] = $_POST['2nd_Delegate_Company_Name'];
        $data['s_address1'] = $_POST['2nd_Delegate_Address'];
        $data['s_address2'] = $_POST['2nd_Delegate_Second_Address'];
        $data['s_tel'] = $_POST['2nd_Delegate_Tel'];
        $data['s_website'] = $_POST['2nd_Delegate_Website'];
        $data['s_email'] = $_POST['2nd_Delegate_Email'];
    }
//    $data['room_type'] = $_POST['Room_Type'];
//    $data['arrival_date'] = $_POST['Arrival_Date'];
//    $data['departure_date'] = $_POST['Depart_Date'];
//    $data['sharing_with'] = $_POST['Sharing_With'];
    if (isset($_POST['2nd_Delegate_Seprate_Room']) && $_POST['2nd_Delegate_Seprate_Room'] == 'on') {
        $data['s_separate_room'] = 1;
        $data['s_room_type'] = $_POST['2nd_Delegate_Room_Type'];
        $data['s_arrival_date'] = $_POST['2nd_Delegate_Arrival_Date'];
        $data['s_departure_date'] = $_POST['2nd_Delegate_Depart_Date'];
        $data['s_sharing_with'] = $_POST['2nd_Delegate_Sharing_With'];
    }
    $data['emergency_name'] = $_POST['Emergency_Contact_Name'];
    $data['emergency_rel'] = $_POST['Relationship'];
    $data['emergency_evening_phone'] = $_POST['Evening_Phone'];
    $data['emergency_cell_phone'] = $_POST['Cell_Phone'];
    if (isset($_POST['2nd_Delegate_Emergency_Contact_Name']) && $_POST['2nd_Delegate_Emergency_Contact_Name']) {
        $data['s_emergency_name'] = $_POST['2nd_Delegate_Emergency_Contact_Name'];
        $data['s_emergency_rel'] = $_POST['2nd_Delegate_Relationship'];
        $data['s_emergency_evening_phone'] = $_POST['2nd_Delegate_Evening_Phone'];
        $data['s_emergency_cell_phone'] = $_POST['2nd_Delegate_Cell_Phone'];
    }
    $sales_points = array(
        $_POST['Point1'],
        $_POST['Point2'],
        $_POST['Point3'],
        $_POST['Point4'],
        $_POST['Point5'],
    );
    $data['point1'] = $_POST['Point1'];
    $data['point2'] = $_POST['Point2'];
    $data['point3'] = $_POST['Point3'];
    $data['point4'] = $_POST['Point4'];
    $data['point5'] = $_POST['Point5'];
//    $data['sales_points'] = serialize($sales_points);
    if (isset($_POST['ATEC']) && $_POST['ATEC'] == 'Yes') {
        $data['is_atec'] = 1;
    }

    if (isset($_POST['ATDW']) && $_POST['ATDW'] == 'Yes') {
        $data['is_atdw'] = 1;
    }
    $data['description'] = $_POST['Description'];
    $data['individuals'] = '';
    $data['adventure'] = '';
    $data['incentive'] = '';
    $data['groups'] = '';
    $data['luxury'] = '';
    $data['gay'] = '';
    $data['youth'] = '';
    $data['eco'] = '';
    $data['other'] = '';
    $product_suitable_for = array();
    if (isset($_POST['Individuals'])) {
        $product_suitable_for[] = 'Individuals';
        $data['individuals'] = $_POST['Individuals'];
    }
    if (isset($_POST['Adventure'])) {
        $product_suitable_for[] = 'Adventure';
        $data['adventure'] = $_POST['Adventure'];
    }
    if (isset($_POST['Incentive'])) {
        $product_suitable_for[] = 'Incentive';
        $data['incentive'] = $_POST['Incentive'];
    }
    if (isset($_POST['Groups'])) {
        $product_suitable_for[] = 'Groups';
        $data['groups'] = $_POST['Groups'];
    }
    if (isset($_POST['Luxury'])) {
        $product_suitable_for[] = 'Luxury';
        $data['luxury'] = $_POST['Luxury'];
    }
    if (isset($_POST['Gay'])) {
        $product_suitable_for[] = 'Gay';
        $data['gay'] = $_POST['Gay'];
    }
    if (isset($_POST['Youth'])) {
        $product_suitable_for[] = 'Youth';
        $data['youth'] = $_POST['Youth'];
    }
    if (isset($_POST['Eco'])) {
        $product_suitable_for[] = 'Eco';
        $data['eco'] = $_POST['Eco'];
    }
    if (isset($_POST['Other'])) {
        $product_suitable_for[] = 'Other';
        $data['other'] = $_POST['Other'];
    }
//    $data['product_suitable_for'] = serialize($product_suitable_for);
    $data['a_retail_roadshow'] = $_POST['Roadshow'];
    if ($return === TRUE)
        return $data;
    global $database;
    send_supplier_application();
    return $database->insert('supplier_application', $data);
}

function send_supplier_application() {
    $all_data = save_supplier_application(TRUE);
    ob_start();
    ?>
    <div>
        <h3 style="color:#005481;font-size:15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:700;line-height:18px;text-align:left;padding-top:.5em">Supplier Profile:</h3>
        Thank you for your application!
        <br />
        <br />
        <strong>***IMPORTANT***</strong><br />
        <br>
        <span class="text">If you have not made your hotel reservations here are the links.<br />
        </span> <br />
        If you are attending the Australia Marketplace ONLY please <a href="http://www.langhamhotels.com/en/pasadena-amp240216" target="_blank">CLICK HERE</a> to reserve your hotel room.<br />
        <br />
        If you are attending BOTH the Australia Marketplace AND the Australia Tourism Summit <a href="http://www.langhamhotels.com/en/pasadena-asm220216" target="_blank">CLICK HERE</a> to reserve your hotel room for both events.<br />
        <br />
        NOTE: A reminder that three nights (February 24, 25 and 26) are included in your Australia Marketplace registration. You are responsible for payment of your own hotel accommodation for Australia Tourism Summit.<br />
        <br />
        For your convenience, you may <a href="https://www.eventbrite.com/e/australia-tourism-summit-2016-tickets-18893457841" target="_blank">CLICK HERE</a> to be taken to the registration page for the Australia Tourism Summit.<br />
        <br /> 
        You provided the following information:<hr />
        <?php
        global $emailArray;
        $emailNameVals = "";
        for ($n = 0; $n < count($emailArray); $n++) {
            if ($emailArray[$n]['value'] != "") {
                $thisFieldName = MPAdjustFields(stripslashes($emailArray[$n]['key']));
                $thisFieldValue = stripslashes($emailArray[$n]['value']);
                $emailNameVals .= $thisFieldName . ": " . $thisFieldValue . "<br />";
            }
        }
        echo $emailNameVals;
        ?>
        <hr />
        Thank you,<br />
        <b>Australia Marketplace 2016</b>
    </div>
    <?php
    $html = ob_get_clean();
    $to = $all_data['email'];
    $subject = "Submission of Supplier Request for Australia Marketplace 2016";
    $headers = "From: joseph@synaxismeetings.com\r\n";
    $headers .= "Reply-To: joseph@synaxismeetings.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    mail($to, $subject, $html, $headers);
}

function save_hotel_request($return = FALSE) {
    $data = array();
    $data['delegate_fname'] = $_POST['Name'];
    $data['delegate_lname'] = $_POST['Name2'];
    $data['company_name'] = $_POST['Company'];
    $data['email'] = $_POST['email'];
    if (isset($_POST['2nd_Delegate_Name']) && $_POST['2nd_Delegate_Name']) {
        $data['s_delegate'] = 1;
        $data['s_delegate_fname'] = $_POST['2nd_Delegate_Name'];
        $data['s_delegate_lname'] = $_POST['2nd_Delegate_Name2'];
        $data['s_email'] = $_POST['2nd_Delegate_Email'];
    }
    $data['room_type'] = $_POST['Room_Type'];
    $data['arrival_date'] = $_POST['Arrival_Date'];
    $data['departure_date'] = $_POST['Depart_Date'];
    $data['sharing_with'] = $_POST['Sharing_With'];
    if (isset($_POST['2nd_Delegate_Seprate_Room']) && $_POST['2nd_Delegate_Seprate_Room'] == 'on') {
        $data['s_separate_room'] = 1;
        $data['s_room_type'] = $_POST['2nd_Delegate_Room_Type'];
        $data['s_arrival_date'] = $_POST['2nd_Delegate_Arrival_Date'];
        $data['s_departure_date'] = $_POST['2nd_Delegate_Depart_Date'];
        $data['s_sharing_with'] = $_POST['2nd_Delegate_Sharing_With'];
    }
//    $data['aai_date'] = $_POST['Airline_Date_Arrive'];
//    $data['aai_carrier'] = $_POST['Carrier_Arrive'];
//    $data['aai_from'] = $_POST['From_Arrive'];
//    $data['aai_to'] = $_POST['To_Arrive'];
//    $data['aai_dep_time'] = $_POST['Departure_Time_Arrive'];
//    $data['aai_arr_time'] = $_POST['Arrival_Time_Arrive'];
//    $data['dai_date'] = $_POST['Airline_Date_Depart'];
//    $data['dai_carrier'] = $_POST['Carrier_Depart'];
//    $data['dai_from'] = $_POST['From_Depart'];
//    $data['dai_to'] = $_POST['To_Depart'];
//    $data['dai_dep_time'] = $_POST['Departure_Time_Depart'];
//    $data['dai_arr_time'] = $_POST['Arrival_Time_Depart'];
//    if (isset($_POST['2nd_Delegate_Requires_Transfer']) && $_POST['2nd_Delegate_Requires_Transfer'] == 'on') {
//        $data['s_separate_fly'] = 1;
//        $data['s_aai_date'] = $_POST['2nd_Delegate_Airline_Date_Arrive'];
//        $data['s_aai_carrier'] = $_POST['2nd_Delegate_Carrier_Arrive'];
//        $data['s_aai_from'] = $_POST['2nd_Delegate_From_Arrive'];
//        $data['s_aai_to'] = $_POST['2nd_Delegate_To_Arrive'];
//        $data['s_aai_dep_time'] = $_POST['2nd_Delegate_Departure_Time_Arrive'];
//        $data['s_aai_arr_time'] = $_POST['2nd_Delegate_Arrival_Time_Arrive'];
//        $data['s_dai_date'] = $_POST['2nd_Delegate_Airline_Date_Depart'];
//        $data['s_dai_carrier'] = $_POST['2nd_Delegate_Carrier_Depart'];
//        $data['s_dai_from'] = $_POST['2nd_Delegate_From_Depart'];
//        $data['s_dai_to'] = $_POST['2nd_Delegate_To_Depart'];
//        $data['s_dai_dep_time'] = $_POST['2nd_Delegate_Departure_Time_Depart'];
//        $data['s_dai_arr_time'] = $_POST['2nd_Delegate_Arrival_Time_Depart'];
//    }
    $data['emergency_name'] = $_POST['Emergency_Contact_Name'];
    $data['emergency_rel'] = $_POST['Relationship'];
    $data['emergency_evening_phone'] = $_POST['Evening_Phone'];
    $data['emergency_cell_phone'] = $_POST['Cell_Phone'];
    if (isset($_POST['2nd_Delegate_Emergency_Contact_Name']) && $_POST['2nd_Delegate_Emergency_Contact_Name']) {
        $data['s_emergency_name'] = $_POST['2nd_Delegate_Emergency_Contact_Name'];
        $data['s_emergency_rel'] = $_POST['2nd_Delegate_Relationship'];
        $data['s_emergency_evening_phone'] = $_POST['2nd_Delegate_Evening_Phone'];
        $data['s_emergency_cell_phone'] = $_POST['2nd_Delegate_Cell_Phone'];
    }
    if ($return === TRUE)
        return $data;
    global $database;
    send_hotel_request();
    return $database->insert('hotel_request', $data);
}

function send_hotel_request() {
    $all_data = save_hotel_request(TRUE);
    ob_start();
    ?>
    <div>
        <h3 style="color:#005481;font-size:15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:700;line-height:18px;text-align:left;padding-top:.5em">Supplier Hotel and Travel:</h3>
        Thank you for your hotel request and travel information!
        <br />
        <br />
        If you have any further questions, you may contact <a href="mailto:joseph@synaxismeetings.com">joseph@synaxismeetings.com</a>
        <br />
        <br />
        You provided the following information:<hr />
        <?php
        global $emailArray;
        $emailNameVals = "";
        for ($n = 0; $n < count($emailArray); $n++) {
            if ($emailArray[$n]['value'] != "") {
                $thisFieldName = MPAdjustFields(stripslashes($emailArray[$n]['key']));
                $thisFieldValue = stripslashes($emailArray[$n]['value']);
                $emailNameVals .= $thisFieldName . ": " . $thisFieldValue . "<br />";
            }
        }
        echo $emailNameVals;
        ?>
        <hr />
        Thank you,<br />
        <b>Australia Marketplace 2016</b>
    </div>
    <?php
    $html = ob_get_clean();
    $to = $all_data['email'];
    $subject = "Submission of Supplier Hotel Request for Australia Marketplace 2016";
    $headers = "From: joseph@synaxismeetings.com\r\n";
    $headers .= "Reply-To: joseph@synaxismeetings.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    mail($to, $subject, $html, $headers);
}

function save_supplier_payment($return = FALSE) {
    $data = array();
    $data['aten_fname'] = $_POST['Name'];
    $data['aten_lname'] = $_POST['LastName'];
    $data['email'] = $_POST['Email'];
    $data['registration_fee'] = $_POST['RegistrationFee'];
    $data['share'] = $_POST['Share'] ? $_POST['Share'] : $_POST['Separate'];
    $data['code_amount'] = $_POST['CodeAmount'];
    $data['amount'] = $_POST['Amount'];
    $data['accept_amount'] = $_POST['AcceptAmount'];
    $data['card'] = $_POST['Card'];
    $data['cc_name'] = $_POST['CCName'];
    $data['cc_number'] = $_POST['CCNumber'];
    $data['cc_month'] = $_POST['CCMonth'];
    $data['cc_year'] = $_POST['CCYear'];
    $data['cvc'] = $_POST['CVC'];
    if ($return === TRUE)
        return $data;
    global $database;
    return $database->insert('supplier_payment', $data);
}

function cleanData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"'))
        $str = '"' . str_replace('"', '""', $str) . '"';
}

function save_buyer_application($return = FALSE) {
    $data = array();
    $data['name'] = $_POST['Name'];
    $data['name2'] = $_POST['Name2'];
    $data['title'] = $_POST['Title'];
    $data['company'] = $_POST['Company'];
    $data['product_contact'] = $_POST['Product_Contact'];
    $data['address1'] = $_POST['Address_1'];
    $data['address2'] = $_POST['Address_2'];
    $data['tel'] = $_POST['Telephone'];
    $data['fax'] = $_POST['Fax'];
    $data['email'] = $_POST['email'];
    $data['website'] = $_POST['Website'];
    $data['inbound_operator'] = $_POST['Inbound_Operator'];
    $data['company_profile'] = $_POST['Company_Profile'];
    $categories = array();
    $data['individuals'] = '';
    $data['adventure'] = '';
    $data['incentive'] = '';
    $data['groups'] = '';
    $data['luxury'] = '';
    $data['gay'] = '';
    $data['youth'] = '';
    $data['eco'] = '';
    $data['other'] = '';
    if (isset($_POST['Individuals'])) {
        $categories[] = 'Individuals';
        $data['individuals'] = $_POST['Individuals'];
    }
    if (isset($_POST['Adventure'])) {
        $categories[] = 'Adventure';
        $data['adventure'] = $_POST['Adventure'];
    }
    if (isset($_POST['Incentive'])) {
        $categories[] = 'Incentive / Conventions';
        $data['incentive'] = $_POST['Incentive'];
    }
    if (isset($_POST['Groups'])) {
        $categories[] = 'Groups';
        $data['groups'] = $_POST['Groups'];
    }
    if (isset($_POST['Luxury'])) {
        $categories[] = 'Luxury / Romance';
        $data['luxury'] = $_POST['Luxury'];
    }
    if (isset($_POST['Gay'])) {
        $categories[] = 'Gay / Lesbian';
        $data['gay'] = $_POST['Gay'];
    }
    if (isset($_POST['Youth'])) {
        $categories[] = 'Youth';
        $data['youth'] = $_POST['Youth'];
    }
    if (isset($_POST['Eco'])) {
        $categories[] = 'Eco Tourism';
        $data['eco'] = $_POST['Eco'];
    }
    if (isset($_POST['Other'])) {
        $categories[] = 'Other';
        $data['other'] = $_POST['Other'];
    }
    $data['categories'] = serialize($categories);
    unset($data['categories']);
    //$data['room_type'] = $_POST['Room_Type'];
    //$data['arrival_date'] = $_POST['Arrival_Date'];
    //$data['depart_date'] = $_POST['Depart_Date'];
    //$data['sharing_with'] = $_POST['Sharing_With'];
    $data['subsidy'] = $_POST['Subsidy'];
    $data['emergency_name'] = $_POST['Emergency_Contact_Name'];
    $data['emergency_rel'] = $_POST['Relationship'];
    $data['emergency_evening_phone'] = $_POST['Evening_Phone'];
    $data['emergeny_cell_phone'] = $_POST['Cell_Phone'];
    if ($return === TRUE)
        return $data;
    global $database;
    send_buyer_application();
    return $database->insert('buyer_application', $data);
}

function send_buyer_application() {
    $all_data = save_buyer_application(TRUE);
    ob_start();
    ?>
    <div>
        <h3 style="color:#005481;font-size:15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:700;line-height:18px;text-align:left;padding-top:.5em">Wholesaler Profile:</h3>
        Thank you for registering for the 2016 Australia Marketplace!<br />
        <br />
        The Australia Marketplace committee will review all applications shortly. Synaxis will then inform you of your approval status. Once approved, please make payment through our credit card payment page for your participation fee. Your payment authorization will be the indication of your acceptance to the Australia Marketplace.<br />
        <br />
        <strong>Please do not make any payment until official notice of acceptance is received.</strong><br />
        <br />
        <strong>***IMPORTANT***</strong><br />
        <br />
        If you are attending the Australia Marketplace ONLY please <a href="http://www.langhamhotels.com/en/pasadena-amp240216" target="_blank">CLICK HERE</a> to reserve your hotel room.<br />
        <br />
        If you are attending BOTH the Australia Marketplace AND the Australia Tourism Summit <a href="http://www.langhamhotels.com/en/pasadena-asm220216" target="_blank">CLICK HERE</a> to reserve your hotel room for both events.<br />
        <br />
        NOTE: A reminder that three nights (February 24, 25 and 26) are included in your Australia Marketplace registration. You are responsible for payment of your own hotel accommodation for Australia Tourism Summit.<br />
        <br />
        For your convenience, you may <a href="https://www.eventbrite.com/e/australia-tourism-summit-2016-tickets-18893457841" target="_blank">CLICK HERE</a> to be taken to the registration page for the Australia Tourism Summit.<br />
        <br />
        Further communication will follow.<br />
        <br />
        You provided the following information:<hr />
        <?php
        global $emailArray;
        $emailNameVals = "";
        for ($n = 0; $n < count($emailArray); $n++) {
            if ($emailArray[$n]['value'] != "") {
                $thisFieldName = MPAdjustFields(stripslashes($emailArray[$n]['key']));
                $thisFieldValue = stripslashes($emailArray[$n]['value']);
                $emailNameVals .= $thisFieldName . ": " . $thisFieldValue . "<br />";
            }
        }
        echo $emailNameVals;
        ?>
        <hr />
        Thank you,<br />
        <b>Australia Marketplace 2016</b>
    </div>
    <?php
    $html = ob_get_clean();
    $to = $all_data['email'];
    $subject = "Submission of Wholesaler Profile for Australia Marketplace 2016";
    $headers = "From: joseph@synaxismeetings.com\r\n";
    $headers .= "Reply-To: joseph@synaxismeetings.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    mail($to, $subject, $html, $headers);
}

function save_aussie_application($return = FALSE) {
    $data = array();
    $data['name'] = $_POST['Name'];
    $data['name2'] = $_POST['Name2'];
    $data['title'] = $_POST['Title'];
    $data['company'] = $_POST['Company'];
    $data['product_contact'] = $_POST['Product_Contact'];
    $data['address1'] = $_POST['Address_1'];
    $data['address2'] = $_POST['Address_2'];
    $data['tel'] = $_POST['Telephone'];
    $data['fax'] = $_POST['Fax'];
    $data['email'] = $_POST['email'];
    $data['website'] = $_POST['Website'];
    $data['inbound_operator'] = $_POST['Inbound_Operator'];
    $data['company_profile'] = $_POST['Company_Profile'];
    $categories = array();
    $data['individuals'] = '';
    $data['adventure'] = '';
    $data['incentive'] = '';
    $data['groups'] = '';
    $data['luxury'] = '';
    $data['gay'] = '';
    $data['youth'] = '';
    $data['eco'] = '';
    $data['other'] = '';
    if (isset($_POST['Individuals'])) {
        $categories[] = 'Individuals';
        $data['individuals'] = $_POST['Individuals'];
    }
    if (isset($_POST['Adventure'])) {
        $categories[] = 'Adventure';
        $data['adventure'] = $_POST['Adventure'];
    }
    if (isset($_POST['Incentive'])) {
        $categories[] = 'Incentive / Conventions';
        $data['incentive'] = $_POST['Incentive'];
    }
    if (isset($_POST['Groups'])) {
        $categories[] = 'Groups';
        $data['groups'] = $_POST['Groups'];
    }
    if (isset($_POST['Luxury'])) {
        $categories[] = 'Luxury / Romance';
        $data['luxury'] = $_POST['Luxury'];
    }
    if (isset($_POST['Gay'])) {
        $categories[] = 'Gay / Lesbian';
        $data['gay'] = $_POST['Gay'];
    }
    if (isset($_POST['Youth'])) {
        $categories[] = 'Youth';
        $data['youth'] = $_POST['Youth'];
    }
    if (isset($_POST['Eco'])) {
        $categories[] = 'Eco Tourism';
        $data['eco'] = $_POST['Eco'];
    }
    if (isset($_POST['Other'])) {
        $categories[] = 'Other';
        $data['other'] = $_POST['Other'];
    }
    $data['categories'] = serialize($categories);
    unset($data['categories']);
    $data['room_type'] = $_POST['Room_Type'];
    $data['arrival_date'] = $_POST['Arrival_Date'];
    $data['depart_date'] = $_POST['Depart_Date'];
    $data['sharing_with'] = $_POST['Sharing_With'];
    $data['subsidy'] = $_POST['Subsidy'];
    $data['emergency_name'] = $_POST['Emergency_Contact_Name'];
    $data['emergency_rel'] = $_POST['Relationship'];
    $data['emergency_evening_phone'] = $_POST['Evening_Phone'];
    $data['emergeny_cell_phone'] = $_POST['Cell_Phone'];
    if ($return === TRUE)
        return $data;
    global $database;
    send_aussie_application();
    return $database->insert('aussie_application', $data);
}

function send_aussie_application() {
    $all_data = save_aussie_application(TRUE);
    ob_start();
    ?>
    <div>
        <h3 style="color:#005481;font-size:15px;font-family:Verdana,Arial,Helvetica,sans-serif;font-weight:700;line-height:18px;text-align:left;padding-top:.5em">Premier Aussie Specialists Profile:</h3>
        Thank you for registering for the 2016 Australia Marketplace!<br />
        <br />
        The Australia Marketplace committee will review all applications shortly. Synaxis will then inform you of your approval status. Once approved, please make payment through our credit card payment page for your participation fee. Your payment authorization will be the indication of your acceptance to the Australia Marketplace.<u></u><br />
        <br />
        <strong>Please do not make any payment until official notice of acceptance is received.</strong><br />
        <br />
        <strong>***IMPORTANT***</strong><br />
        <br />
        NOTE: A reminder that three nights (February 24, 25 and 26) are included in your Australia Marketplace registration. <strong>Rooms are based on twin share.</strong> Single rooms are available at your own cost, based on hotel room availability. <br />
        <br />
        If you are planning to attend Australia Tourism Summit, you are responsible for payment of your own hotel accommodation. Hotel can be booked for the Australia Tourism Summit on the Summit registration page at the link below.<br />
        <br />
        For your convenience, you may <a href="https://www.eventbrite.com/e/australia-tourism-summit-2016-tickets-18893457841" target="_blank">CLICK HERE</a> to be taken to the registration page for the Australia Tourism Summit.<br />
        <br />
        Further communication will follow.<br />
        <br />
        You provided the following information:<hr />
        <?php
        global $emailArray;
        $emailNameVals = "";
        for ($n = 0; $n < count($emailArray); $n++) {
            if ($emailArray[$n]['value'] != "") {
                $thisFieldName = MPAdjustFields(stripslashes($emailArray[$n]['key']));
                $thisFieldValue = stripslashes($emailArray[$n]['value']);
                $emailNameVals .= $thisFieldName . ": " . $thisFieldValue . "<br />";
            }
        }
        echo $emailNameVals;
        ?>
        <hr />
        Thank you,<br />
        <b>Australia Marketplace 2016</b>
    </div>
    <?php
    $html = ob_get_clean();
    $to = $all_data['email'];
    $subject = "Submission of Wholesaler Profile for Australia Marketplace 2016";
    $headers = "From: joseph@synaxismeetings.com\r\n";
    $headers .= "Reply-To: joseph@synaxismeetings.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    mail($to, $subject, $html, $headers);
}
?>